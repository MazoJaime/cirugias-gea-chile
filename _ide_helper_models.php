<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\Procedimiento
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Procedimiento newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Procedimiento newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Procedimiento query()
 * @mixin \Eloquent
 * @property int $id
 * @property string $codigo
 * @property string $nombre
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Procedimiento whereCodigo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Procedimiento whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Procedimiento whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Procedimiento whereNombre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Procedimiento whereUpdatedAt($value)
 */
	class Procedimiento extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Prevision
 *
 * @property int $id
 * @property string $nombre
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Prevision newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Prevision newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Prevision query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Prevision whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Prevision whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Prevision whereNombre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Prevision whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class Prevision extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Diagnostico
 *
 * @property int $id
 * @property string $nombre
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Diagnostico newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Diagnostico newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Diagnostico query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Diagnostico whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Diagnostico whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Diagnostico whereNombre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Diagnostico whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class Diagnostico extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\BitacoraAsistencia
 *
 * @property int $id
 * @property int $user_id
 * @property int $asistencia_id
 * @property string $observacion
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Asistencia $asistencia
 * @property-read \App\Models\User $creador
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BitacoraAsistencia newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BitacoraAsistencia newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BitacoraAsistencia query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BitacoraAsistencia whereAsistenciaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BitacoraAsistencia whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BitacoraAsistencia whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BitacoraAsistencia whereObservacion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BitacoraAsistencia whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BitacoraAsistencia whereUserId($value)
 * @mixin \Eloquent
 */
	class BitacoraAsistencia extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Region
 *
 * @property int $id
 * @property string $nombre
 * @property int $staff
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Region newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Region newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Region query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Region whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Region whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Region whereNombre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Region whereStaff($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Region whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class Region extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Permission[] $permissions
 * @property-read int|null $permissions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\NotificationChannels\WebPush\PushSubscription[] $pushSubscriptions
 * @property-read int|null $push_subscriptions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Role[] $roles
 * @property-read int|null $roles_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User permission($permissions)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User role($roles, $guard = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read mixed $all_permissions
 */
	class User extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Asistencia
 *
 * @property int $asistencia
 * @property int $expediente
 * @property string $cuenta
 * @property string $nombre_afiliado
 * @property string $rut_afiliado
 * @property string $apertura_expediente
 * @property string $apertura_asistencia
 * @property int $costo_asistencia obtenido de costos de asistencia en temp
 * @property int $prevision_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Asistencia newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Asistencia newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Asistencia query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Asistencia whereAperturaAsistencia($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Asistencia whereAperturaExpediente($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Asistencia whereAsistencia($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Asistencia whereCostoAsistencia($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Asistencia whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Asistencia whereCuenta($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Asistencia whereExpediente($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Asistencia whereNombreAfiliado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Asistencia wherePrevisionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Asistencia whereRutAfiliado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Asistencia whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $user_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\BitacoraAsistencia[] $bitacora
 * @property-read int|null $bitacora_count
 * @property-read \App\Models\User $creador
 * @property-read \App\Models\Prevision|null $prevision
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Asistencia whereUserId($value)
 */
	class Asistencia extends \Eloquent {}
}

