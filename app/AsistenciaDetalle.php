<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\AsistenciaDetalle
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AsistenciaDetalle newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AsistenciaDetalle newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AsistenciaDetalle query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AsistenciaDetalle whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AsistenciaDetalle whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AsistenciaDetalle whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class AsistenciaDetalle extends Model
{
    //
}
