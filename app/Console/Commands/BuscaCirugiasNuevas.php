<?php

namespace App\Console\Commands;

use App\Models\Asistencia;
use App\Models\BitacoraAsistencia;
use Illuminate\Console\Command;
use GuzzleHttp\Client;

class BuscaCirugiasNuevas extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cirugia:nuevas';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Se conecta al servidor del siga y busca las cirugias del día, si no esta cargada la crea en sistema';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $client = new Client([
            'base_uri' => env('SIGA_URL')
        ]);

        $response = $client->get('/servicios/swciru_servicios.php?type=all');

        if ($response->getStatusCode() == 200) {
            $asistencias = json_decode($response->getBody());
            foreach ($asistencias as $asistencia) {
                $bitacora = true;
                //Logica para agregar nuevo registro de una cirugia
                $registro = Asistencia::firstOrNew([
                    'asistencia' => $asistencia->NUMASISTENCIA,
                    'expediente' => $asistencia->CVEEXPED,
                    'cuenta' => $asistencia->CUENTA,
                    'nombre_afiliado' => $asistencia->NOMAFILIADO,
                    'rut_afiliado' => $asistencia->CVEAFILIADO,
                    'apertura_expediente' => $asistencia->APERTURA_EXP,
                    'apertura_asistencia' => $asistencia->APERTURA_ASIS,
                ]);

                if ($registro->exists) {
                    $bitacora = false;
                    if ($registro->costo_asistencia != $asistencia->COSTO_ASISTENCIA) {
                        $registro->costo_asistencia = $asistencia->COSTO_ASISTENCIA;

                    }
                } else {
                    $registro->user_id = 1;
                    $registro->costo_asistencia = $asistencia->COSTO_ASISTENCIA;
                    $registro->prevision_id = $asistencia->PREVISION ? : 1;

                }

                $registro->save();
                if ($bitacora) {
                    BitacoraAsistencia::create([
                        'user_id' => 1,
                        'asistencia_id' => $asistencia->NUMASISTENCIA,
                        'observacion' => 'Se crea registro de forma automatica desde SIGA'
                    ]);
                }
            }

        }
    }
}
