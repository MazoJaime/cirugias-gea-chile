<?php

namespace App\Console\Commands;

use App\Models\PrecioUf;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Console\Command;


class ValorUf extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'uf:valor';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Obtiene el valor de las UF diariamente en el rango de 1 mes ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $date = Carbon::now()->format('Y-m');
        $extramonth = Carbon::now()->addMonth()->format('Y-m');
        $inicio = explode('-', $date);
        $fin = explode('-', $extramonth);

        $key = env('SBIF_KEY');
        $url = "https://api.sbif.cl/api-sbifv3/recursos_api/uf/periodo/{$inicio[0]}/{$inicio[1]}/{$fin[0]}/{$fin[1]}?apikey={$key}&formato=json";

        $client = new Client();
        $response = $client->get($url);
        if ($response->getStatusCode() == 200) {
            $valores = json_decode($response->getBody(), true);
            foreach ($valores['UFs'] as $UF) {

                $sub1 = str_replace('.', '', $UF['Valor']);
                $valor = str_replace(',', '.', $sub1);
                PrecioUf::firstOrCreate([
                    'precio' => $valor,
                    'fecha' => $UF['Fecha']
                ]);
            }
        }else {

            return false;
        }

        return true;
    }

}
