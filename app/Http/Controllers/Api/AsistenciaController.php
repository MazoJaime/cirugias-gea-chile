<?php


namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Asistencia;
use App\Models\BitacoraAsistencia;
use App\Models\Bono;
use App\Models\CostoAsistencia;
use App\Models\ObservacionAsistencia;
use App\Models\Prevision;
use App\Models\Region;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class AsistenciaController extends Controller
{

    public function data()
    {

        $data['regiones'] = Region::all();
        $data['previsiones'] = Prevision::all();
        return $data;
    }


    public function show(Asistencia $asistencia)
    {
        $data = $this->data();
        $data['asistencia'] = $asistencia->load(['prevision', 'bitacora', 'creador', 'bitacora.creador', 'observaciones', 'observaciones.creador', 'costos']);
        return $data;
    }

    public function store(Request $request)
    {
        $request->validate([
            'asistencia' => 'unique:asistencias|required',
            'expediente' => 'unique:asistencias|required',
            'cuenta' => 'required|string',
            'nombre_afiliado' => 'required|string',
            'rut_afiliado' => 'required|string',
            'apertura_expediente' => 'required|date',
            'apertura_asistencia' => 'required|date',
            'prevision_id' => 'required|exists:previsiones,id',
            'costo_asistencia' => 'required|numeric'
        ]);
        $data = $request->all();
        $user = Auth::user();
        $data['user_id'] = $user->id;
        Asistencia::create($data);
        $asistencia = Asistencia::findOrFail($data['asistencia']);
        BitacoraAsistencia::create([
            'user_id' => $user->id,
            'asistencia_id' => $asistencia->asistencia,
            'observacion' => "Usuario {$user->name} crea registro de cirugia."
        ]);
        return \Response::json($asistencia, 201);
    }

    public function updateprevision(Request $request)
    {
        $asistencia = Asistencia::find($request->asistencia);
        $previsionAnterior = $asistencia->prevision->nombre;
        $prevision = Prevision::find($request->prevision);
        $asistencia->prevision_id = $prevision->id;
        $asistencia->save();
        BitacoraAsistencia::create([
            'user_id' => \Auth::user()->id,
            'asistencia_id' => $asistencia->asistencia,
            'observacion' => "Se actualizo prevision de {$previsionAnterior} a {$prevision->nombre}"
        ]);
        return ['prevision' => $prevision->id];
    }

    public function costos(Request $request)
    {

        $asistencia = Asistencia::find($request->post('asistencia'));

        $costos = CostoAsistencia::create(['asistencia' => $asistencia->asistencia,
            'costo' => $request->costo,
            'tipo_cuenta' => $request->tipo_cuenta,
            'insumos' => $request->insumos ?? false,
            'costo_insumos' => $request->costo_insumos,
            'complementos' => $request->complementos ?? false,
            'costo_complementos' => $request->costo_complementos ?? 0,
            'honorarios' => $request->honorarios ?? 0,
            'programa' => $request->programa ?? 0,
        ]);
        BitacoraAsistencia::create([
            'user_id' => \Auth::user()->id,
            'asistencia_id' => $asistencia->asistencia,
            'observacion' => "se crearon los costos con valores " . json_encode($costos)
        ]);
        return ['status' => 'success'];
    }

    public function updatecosto(Request $request)
    {

        $asistencia = Asistencia::find($request->post('asistencia'));

        $costo = CostoAsistencia::where('asistencia', $asistencia->asistencia)->first();

        $costo->update(['costo' => $request->costo,
            'tipo_cuenta' => $request->tipo_cuenta,
            'insumos' => $request->insumos ?? false,
            'costo_insumos' => $request->costo_insumos,
            'complementos' => $request->complementos ?? false,
            'costo_complementos' => $request->costo_complementos ?? 0,
            'honorarios' => $request->honorarios ?? 0,
            'programa' => $request->programa ?? 0,
        ]);
        $costo->save();
        BitacoraAsistencia::create([
            'user_id' => \Auth::user()->id,
            'asistencia_id' => $asistencia->asistencia,
            'observacion' => "Se actualizaron los costos los costos con valores " . json_encode($costo)
        ]);
        return ['status' => 'success'];
    }

    public function observacion(Request $request)
    {

        $asistencia = Asistencia::find($request->post('asistencia'));

        $observacion = ObservacionAsistencia::create([
            'asistencia_id' => $asistencia->asistencia,
            'user_id' => $request->user()->id,
            'texto' => $request->texto
        ]);

        return ['status' => 'success', 'observacion' => $observacion->load('creador')];

    }

    public function agendar(Request $request)
    {
        $user = Auth::user();
        AgendaNotificacion::create([
            'user' => $user->id,
            'title' => 'Agenda De cirugia',
            'body' => 'Se agendo la revision de la cirguia con asistencia:' . $request->asistencia,
            'action_url' => url('/asistencias/' . $request->asistencia),
            'created' => Carbon::now()->toIso8601String(),
            'fecha_agenda' => $request->fecha_agenda
        ]);
        return \Response::json(['mensaje' => "Se agendo exitosamente para el  {$request->fecha_agenda}"], 201);
    }

    public function bonosData(Request $request)
    {

        $bonos = Bono::where('categoria', $request->bono)->get();

        return $bonos;
    }
}
