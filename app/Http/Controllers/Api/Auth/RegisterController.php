<?php

namespace App\Http\Controllers\Api\Auth;

use App\Models\User;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class RegisterController extends Controller
{
    public function index(Request $request)
    {
        $response = ['status' => 'error', 'message' => 'Datos incompletos'];

        $rut = $request->post('rut', null);

        if ($rut != null) {
            try {
                User::where('rut', $rut)->firstOrFail();

                $response = ['status' => 'error', 'message' => 'El rut ya tiene una cuenta asociada'];
                //Si falla es porque no existe cuenta con el rut por tanto la cuenta podra ser creada.
            } catch (ModelNotFoundException $exception) {

                ## $response = ['status' => 'success', 'message' => 'Es posible crear una cuenta para este usuario'];
                $response = $this->getDataFromSiga($rut);


            } catch (\Exception $exception) {
                Log::error($exception->getTraceAsString(), ['Error No controlado al intentar crear usuario']);
                $response = ['status' => 'error', 'message' => 'Un error inesperado ha impedido la creacion de este usuario'];
            }
        }
        //por defecto retorna datos incompletos
        return $response;
    }

    protected function getDataFromSiga(String $rut)
    {
        $response = ['status' => 'error', 'message' => 'Es factible crear el usuario'];
        $client = new Client();
        $responsehttp = $client->request('GET', env('SIGA_URL') . "servicios/user.php?userrut_={$rut}&token=" . env('SIGA_KEY'));

        if ($responsehttp->getStatusCode() === 200) {

            ##Hasta este punto conectamos con el siga independientemente de si existe o no el usuario
            $body = json_decode($responsehttp->getBody());

            if ($body->status === 'success') {

                $response['user'] = $body->usu_alix;
                $response['name'] = $body->nombre_usu;
                $response['rut'] = $body->cedula;
                $response['status'] = 'success';

            } elseif ($body->status === 'error') {

                if ($body->message == 'User not found or not active') {
                    $response['message'] = 'Usuario no existe o inactivo';

                } elseif ($body->message == 'Not authorized') {

                    Log::error('Se modifico algo en el siga que impide acceder al servicio de valiudacion de usuarios');
                    $response['message'] = 'Un error impide la creacion de usuarios, favor informar a sistemas';

                }else{
                    $response['message'] = 'Un error inesperado afecta el servicio, por favor intente nuevamente';
                }

            }

        } else {
            Log::error('Sin respuesta desde servidor del siga al intentar crear un usuario');
            $response['message'] = 'Sin respuesta del siga, si el problema persiste informe a sistemas';
        }

        return $response;
    }
}
