<?php

namespace App\Http\Controllers\Api\Mantenedores;

use App\Models\Bono;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;

class BonosController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function index(Request $request)
    {

        $colselect = ['*'];

        $length = $request->input('length');
        if ($request->input('showdata')) {
            return Bono::paginate($length, $colselect);
        }
        $search_input = $request->input('search');
        $query = Bono::select($colselect);
        if ($search_input) {
            $query->where(function ($query) use ($search_input) {
                $query->where('codigo', 'like', '%' . $search_input . '%');
            });
        }
        return $query->paginate($length);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {

        $request->validate([
            'codigo' => 'numeric|unique:bonos|required',
            'descripcion' => 'required|min:10|max:150',
            'valor' => 'required|numeric',
            'categoria' => ['required', Rule::in([1, 2])]
        ]);
        $bono = Bono::create($request->only(['codigo', 'descripcion', 'valor', 'categoria']));
        return \Response::json(['bono' => $bono], 201);


    }


    /**
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        return Bono::findOrFail($id);
    }

    /**
     * @param Request $request
     * @param Bono $bono
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Bono $bono)
    {
        $request->validate([
            'descripcion' => 'required|min:10|max:150',
            'valor' => 'required|numeric',
            'categoria' => ['required', Rule::in([1, 2])]
        ]);
        $bono->update($request->only(['descripcion', 'valor', 'categoria']));
        $bono->save();
        return \Response::json(['bono' => $bono ,'status' => 'success'], 201);
    }
}
