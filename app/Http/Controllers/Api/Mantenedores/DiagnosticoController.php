<?php

namespace App\Http\Controllers\Api\Mantenedores;

use App\Models\Diagnostico;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class DiagnosticoController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function index(Request $request)
    {

        $colselect = ['nombre', 'created_at', 'updated_at', 'id'];

        $length = $request->input('length');
        if ($request->input('showdata')) {
            return Diagnostico::paginate($length, $colselect);
        }
        $search_input = $request->input('search');
        $query = Diagnostico::select($colselect);
        if ($search_input) {
            $query->where(function ($query) use ($search_input) {
                $query->where('nombre', 'like', '%' . $search_input . '%');
            });
        }
        return $query->paginate($length);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'nombre' => 'required|unique:diagnosticos|max:100'
        ]);
        $diagnotico = Diagnostico::create($request->only(['nombre']));
        return \Response::json($diagnotico, 201);

    }

    /**
     * @param $id
     * @return Diagnostico|Diagnostico[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
     */
    public function show($id)
    {
        return Diagnostico::findOrFail($id);
    }

    /**
     * @param Request $request
     * @param Diagnostico $diagnostico
     * @return Diagnostico
     */
    public function update(Request $request, Diagnostico $diagnostico)
    {
        $request->validate(['nombre' => 'unique:diagnosticos|required|max:100']);
        $diagnostico->update($request->only(['nombre']));
        $diagnostico->save();
        return $diagnostico;

    }
}
