<?php

namespace App\Http\Controllers\Api\Mantenedores;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Prevision;

class PrevisionController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:previsiones-list', ['only' => ['index', 'show']]);
        $this->middleware('permission:previsiones-edit', ['only' => ['update']]);
        $this->middleware('permission:previsiones-create', ['only' => ['store']]);

    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function index(Request $request)
    {

        $colselect = ['nombre', 'created_at', 'updated_at', 'id'];
        $length = $request->input('length');
        \Log::debug($request->input('showdata'));
        \Log::debug(gettype($request->input('showdata')));
        if ($request->input('showdata') == true) {
            return Prevision::paginate($length, $colselect);
        }
        $search_input = $request->input('search');
        $query = Prevision::select($colselect);
        if ($search_input) {
            $query->where(function ($query) use ($search_input) {
                $query->where('nombre', 'like', '%' . $search_input . '%');
            });
        }

        return $query->paginate($length);

    }

    public function store(Request $request)
    {
        $data = $this->validate($request, [
            'nombre' => 'required|unique:previsiones|max:50'
        ], [
            'unique' => 'El :attribute ya esta en uso.',
            'required' => 'El :attribute es obligatorio',
            'max' => 'El :attribute no puede tener un largo mayor a :max'
        ]);
        $prevision = Prevision::create($data);

        return \Response::json(['prevision' => $prevision], 201);
    }

    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'nombre' => 'required|unique:previsiones|max:50'
        ], [
            'unique' => 'El :attribute ya esta en uso.',
            'required' => 'El :attribute es obligatorio',
            'max' => 'El :attribute no puede tener un largo mayor a :max',
        ]);
        try {
            $prevision = Prevision::findOrFail($id);
            $prevision->update(['nombre' => $request->nombre]);
            $prevision->save();
        } catch (\Exception $e) {

        }
        return \Response::json(['prevision' => $prevision], 200);
    }

    public function show($id)
    {
        try {
            $prevision = Prevision::findOrFail($id);
        } catch (ModelNotFoundException $exception) {
            return \Response::json(['error' => 'No es posible recuperar informacion de una prevision que no existe'], 422);
        }
        return \Response::json(['prevision' => $prevision], 200);
    }
}
