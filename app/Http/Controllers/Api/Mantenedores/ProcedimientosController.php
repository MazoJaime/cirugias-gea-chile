<?php

namespace App\Http\Controllers\Api\Mantenedores;

use App\Models\Procedimiento;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProcedimientosController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function index(Request $request)
    {

        $colselect = ['nombre', 'codigo', 'created_at', 'updated_at', 'id'];

        $length = $request->input('length');
        if ($request->input('showdata')) {
            return Procedimiento::paginate($length, $colselect);
        }
        $search_input = $request->input('search');
        $query = Procedimiento::select($colselect);
        if ($search_input) {
            $query->where(function ($query) use ($search_input) {
                $query->where('nombre', 'like', '%' . $search_input . '%');
            });
        }
        return $query->paginate($length);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'nombre' => 'required|unique:procedimientos|max:150',
            'codigo' => 'unique:procedimientos|max:12|required'
        ]);
        $proc = Procedimiento::create($request->only(['nombre', 'codigo']));
        return \Response::json($proc, 201);

    }

    /**
     * @param $id
     * @return Procedimiento|Procedimiento[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
     */
    public function show($id)
    {
        return Procedimiento::findOrFail($id);
    }

    /**
     * @param Request $request
     * @param Procedimiento $procedimiento
     * @return Procedimiento
     */
    public function update(Request $request, Procedimiento $procedimiento)
    {
        $request->validate(['nombre' => "unique:procedimientos,nombre,$procedimiento->id|required|max:150",
            'codigo' => "unique:procedimientos,codigo,$procedimiento->id|required|max:12"
        ]);
        $procedimiento->update($request->only(['nombre', 'codigo']));
        $procedimiento->save();
        return $procedimiento;

    }
}
