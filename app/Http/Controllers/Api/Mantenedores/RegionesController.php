<?php

namespace App\Http\Controllers\Api\Mantenedores;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Region;


class RegionesController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function index(Request $request)
    {

        $colselect = ['nombre', 'staff', 'created_at', 'updated_at', 'id'];

        $length = $request->input('length');
        if ($request->input('showdata')) {
            return Region::paginate($length, $colselect);
        }
        $search_input = $request->input('search');
        $query = Region::select($colselect);
        if ($search_input) {
            $query->where(function ($query) use ($search_input) {
                $query->where('nombre', 'like', '%' . $search_input . '%');
            });
        }
        return $query->paginate($length);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'nombre' => 'required|unique:regiones|max:150',
            'staff' => 'boolean|required'
        ]);
        $reg = Region::create($request->only(['nombre', 'staff']));
        return \Response::json($reg, 201);

    }

    /**
     * @param $id
     * @return Region|Region[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
     */
    public function show($id)
    {
        return Region::findOrFail($id);
    }

    /**
     * @param Request $request
     * @param $id
     * @return Region|Region[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null
     */
    public function update(Request $request, $id)
    {

        $request->validate(['nombre' => "unique:regiones,nombre,$id|required|max:150",
            'staff' => "required|boolean"
        ]);
        $region = Region::find($id);
        $region->update($request->only(['nombre', 'staff']));
        $region->save();
        return $region;

    }
}
