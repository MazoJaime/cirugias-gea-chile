<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Asistencia;
use Illuminate\Http\Request;

class SearchAsistenciasController extends Controller
{

    public function search(Request $request)
    {
        $numero = "%" . $request->numasis . "%";
        $asistencias = Asistencia::where('asistencia', 'like', $numero)
            ->orWhere('expediente', 'like', $numero)
            ->orWhere('rut_afiliado', 'like', $numero)
            ->limit(5)
            ->get(['asistencia', 'expediente', 'nombre_afiliado']);

        return $asistencias;
    }

    public function list(Request $request)
    {
        $colselect = ['asistencia', 'expediente', 'nombre_afiliado', 'apertura_expediente', 'rut_afiliado'];
        $length = $request->input('length');
        if ($request->input('showdata')) {
            return Asistencia::orderBy('asistencia')
                ->paginate($length, $colselect);
        }
        $search_input = $request->input('search');
        $query = Asistencia::select($colselect);
        if ($search_input) {
            $query->where(function ($query) use ($search_input) {
                $query->where('asistencia', 'like', '%' . $search_input . '%')
                    ->orWhere('expediente', 'like', '%' . $search_input . '%')
                    ->orWhere('nombre_afiliado', 'like', '%' . $search_input . '%')
                    ->orWhere('apertura_expediente', 'like', '%' . $search_input . '%')
                    ->orWhere('rut_afiliado', 'like', '%' . $search_input . '%');
            });
        }
        return $query->paginate($length);

    }

}
