<?php

namespace App\Http\Controllers\Api;

use App\Models\PrecioUf;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UfController extends Controller
{
    /**
     * @param $day
     * @param $month
     * @param $year
     * @return PrecioUf|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function show($year, $month, $day)
    {

        $date = $year . '-' . $month . '-' . $day;
        try {
            $uf = PrecioUf::whereFecha($date)->firstOrFail();
        } catch (ModelNotFoundException $e) {

            $key = env('SBIF_KEY');
            $url = "https://api.sbif.cl/api-sbifv3/recursos_api/uf/{$year}/{$month}/dias/{$day}?apikey={$key}&formato=json";
            $client = new Client();
            $response = $client->get($url);
            if ($response->getStatusCode() == 200) {
                $valores = json_decode($response->getBody(), true);
                for ($i = 0; $i < count($valores['UFs']); $i++) {

                    $sub1 = str_replace('.', '', $valores['UFs'][$i]['Valor']);
                    $valor = str_replace(',', '.', $sub1);
                    $uf = PrecioUf::create([
                        'precio' => $valor,
                        'fecha' => $valores['UFs'][$i]['Fecha']
                    ]);
                }
            }

        }
        return $uf;
    }
}
