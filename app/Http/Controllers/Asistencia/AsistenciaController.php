<?php

namespace App\Http\Controllers\Asistencia;

use App\Http\Controllers\Controller;
use App\Models\Asistencia;
use App\Models\Region;

class AsistenciaController extends Controller
{

    public function __construct()
    {

    }

    public function index(){

        return view('asistencias.index');
    }

}
