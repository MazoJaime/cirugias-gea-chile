<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'user';
    }

    /**
     *
     * Get the data from form and validate credentials against SIGA server then grant access if allowed
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Symfony\Component\HttpFoundation\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {
        $this->validateLogin($request);
        $client = new Client();

        try {

            $response = $client->request('GET', env('SIGA_URL') . 'servicios/user.php?loc=' . base64_encode($request->user) . '&timebase=' . base64_encode($request->password));

            if ($response->getStatusCode() === 200) {

                $body = json_decode($response->getBody());

                if ($body->status === 'success') {

                    $user = User::where('user', $request->user)->where('active', '=', true)->firstOrFail();
                    Auth::login($user);
                    return $this->sendLoginResponse($request);

                } elseif ($body->status === 'error' && $body->message === 'User not found or not active') {

                    // En caso de error envia siempre mensaje de usuario o contraseña incorrectos
                    $this->incrementLoginAttempts($request);
                    return $this->sendFailedLoginResponse($request);

                } else {

                    Log::error('No se pudo hacer login y no entra en casos anteriores, es posible que hayan modificado algo del siga e.e');
                    $this->incrementLoginAttempts($request);
                    return $this->sendFailedLoginResponse($request);

                }

            } else {

                $body = $response->getBody();
                Log::critical('Error en servidor Siga: ' . $body);
                return back()->with('error', 'Ha ocurrido un problema al ingresar con su usuario del siga, para mas informacion con sistemas');

            }

        } catch (ModelNotFoundException $exception) {
            //entra, si credenciales del siga estan ok pero no tiene usuario en esta plataforma o esta desactivado
            Log::critical($exception->getTraceAsString());
            return back()->with('error', 'Es posible  que el usuario no este autorizado a ingresar a esta plataforma, comuniquese con el administrador');

        }


    }

}
