<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MantenedoresController extends Controller
{


    /**
     * MantenedoresController constructor.
     */
    public function __construct()
    {
        $this->middleware('permission:mantenedores-list');
    }

    /**
     * Punto de entrada a mantederores de vue
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(){

        return view('mantenedores.index');
    }
}
