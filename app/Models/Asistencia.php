<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Asistencia
 *
 * @property int $asistencia
 * @property int $expediente
 * @property string $cuenta
 * @property string $nombre_afiliado
 * @property string $rut_afiliado
 * @property string $apertura_expediente
 * @property string $apertura_asistencia
 * @property int $costo_asistencia obtenido de costos de asistencia en temp
 * @property int $prevision_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Asistencia newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Asistencia newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Asistencia query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Asistencia whereAperturaAsistencia($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Asistencia whereAperturaExpediente($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Asistencia whereAsistencia($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Asistencia whereCostoAsistencia($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Asistencia whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Asistencia whereCuenta($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Asistencia whereExpediente($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Asistencia whereNombreAfiliado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Asistencia wherePrevisionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Asistencia whereRutAfiliado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Asistencia whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $user_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\BitacoraAsistencia[] $bitacora
 * @property-read int|null $bitacora_count
 * @property-read \App\Models\User $creador
 * @property-read \App\Models\Prevision|null $prevision
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Asistencia whereUserId($value)
 * @property-read \App\Models\CostoAsistencia $costos
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ObservacionAsistencia[] $observaciones
 * @property-read int|null $observaciones_count
 */
class Asistencia extends Model
{
    //
    protected $primaryKey = 'asistencia';
    protected $fillable = [
        'asistencia',
        'expediente',
        'cuenta',
        'nombre_afiliado',
        'rut_afiliado',
        'apertura_expediente',
        'apertura_asistencia',
        'prevision_id',
        'user_id',
        'costo_asistencia'
    ];

    public function bitacora()
    {
        return $this->hasMany(BitacoraAsistencia::class, 'asistencia_id', 'asistencia')->orderBy('id','desc');
    }

    public function prevision()
    {
        return $this->belongsTo(Prevision::class, 'prevision_id');
    }

    public function creador()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
    public function observaciones(){
        return $this->hasMany(ObservacionAsistencia::class, 'asistencia_id', 'asistencia')->orderBy('id','desc');
    }
    public function costos(){
        return $this->hasOne(CostoAsistencia::class, 'asistencia','asistencia');
    }
}
