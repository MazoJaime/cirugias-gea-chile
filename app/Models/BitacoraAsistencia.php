<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\BitacoraAsistencia
 *
 * @property int $id
 * @property int $user_id
 * @property int $asistencia_id
 * @property string $observacion
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Asistencia $asistencia
 * @property-read \App\Models\User $creador
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BitacoraAsistencia newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BitacoraAsistencia newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BitacoraAsistencia query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BitacoraAsistencia whereAsistenciaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BitacoraAsistencia whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BitacoraAsistencia whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BitacoraAsistencia whereObservacion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BitacoraAsistencia whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BitacoraAsistencia whereUserId($value)
 * @mixin \Eloquent
 */
class BitacoraAsistencia extends Model
{
    //
    protected $fillable = [
        'user_id',
        'asistencia_id',
        'observacion',
    ];

    public function creador()
    {
        return $this->belongsTo(User::class, 'user_id','id');
    }

    public function asistencia()
    {
        return $this->belongsTo(Asistencia::class, 'asistencia_id', 'asistencia');
    }
}
