<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Bono
 *
 * @property int $id
 * @property int $codigo
 * @property string $descripcion
 * @property int $valor
 * @property string $categoria 1 =>pad, 2=>cta abierta
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Bono newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Bono newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Bono query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Bono whereCategoria($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Bono whereCodigo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Bono whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Bono whereDescripcion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Bono whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Bono whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Bono whereValor($value)
 * @mixin \Eloquent
 */
class Bono extends Model
{

    protected $fillable = ['codigo', 'descripcion', 'valor', 'categoria'];
}
