<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ClasificacionCobertura
 *
 * @property int $id
 * @property string $clasificacion
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClasificacionCobertura newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClasificacionCobertura newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClasificacionCobertura query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClasificacionCobertura whereClasificacion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClasificacionCobertura whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClasificacionCobertura whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClasificacionCobertura whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ClasificacionCobertura extends Model
{
    //
}
