<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ClasificacionCuenta
 *
 * @property int $id
 * @property int $clasificacion_id
 * @property string $codigo_cuenta
 * @property int $cobertura
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClasificacionCuenta newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClasificacionCuenta newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClasificacionCuenta query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClasificacionCuenta whereClasificacionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClasificacionCuenta whereCobertura($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClasificacionCuenta whereCodigoCuenta($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClasificacionCuenta whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClasificacionCuenta whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClasificacionCuenta whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ClasificacionCuenta extends Model
{
    //
}
