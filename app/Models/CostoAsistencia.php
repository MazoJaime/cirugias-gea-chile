<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CostoAsistencia
 *
 * @property int $id
 * @property int $asistencia
 * @property int $costo
 * @property int|null $insumos indica si persona requiere o no insumos medicos
 * @property int $costo_insumos
 * @property int|null $complementos
 * @property int $costo_complementos
 * @property int $honorarios
 * @property int $programa
 * @property int $bono_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CostoAsistencia newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CostoAsistencia newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CostoAsistencia query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CostoAsistencia whereAsistencia($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CostoAsistencia whereBonoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CostoAsistencia whereComplementos($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CostoAsistencia whereCosto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CostoAsistencia whereCostoComplementos($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CostoAsistencia whereCostoInsumos($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CostoAsistencia whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CostoAsistencia whereHonorarios($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CostoAsistencia whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CostoAsistencia whereInsumos($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CostoAsistencia wherePrograma($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CostoAsistencia whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CostoAsistencia extends Model
{

    protected $fillable= [
        'asistencia',
        'costo',
        'insumos',
        'costo_insumos',
        'complementos',
        'costo_complementos',
        'honorarios',
        'programa',
        'tipo_cuenta'
    ];
}
