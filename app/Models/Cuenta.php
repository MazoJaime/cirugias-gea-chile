<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Cuenta
 *
 * @property string $codigo
 * @property string $nombre
 * @property int $cobertura_defecto
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ClasificacionCuenta[] $coberturas
 * @property-read int|null $coberturas_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cuenta newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cuenta newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cuenta query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cuenta whereCoberturaDefecto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cuenta whereCodigo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cuenta whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cuenta whereNombre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cuenta whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Cuenta extends Model
{

    protected $fillable = ['codigo', 'nombre', 'cobertura_defecto'];

    public function coberturas(){
        return $this->hasMany(ClasificacionCuenta::class,'codigo_cuenta','codigo');
    }
}
