<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Diagnostico
 *
 * @property int $id
 * @property string $nombre
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Diagnostico newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Diagnostico newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Diagnostico query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Diagnostico whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Diagnostico whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Diagnostico whereNombre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Diagnostico whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Diagnostico extends Model
{
    protected $fillable = ['nombre'];
}
