<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\Doctor
 *
 * @property int $id
 * @property string $nombre
 * @property int $region_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Doctor newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Doctor newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Doctor query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Doctor whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Doctor whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Doctor whereNombre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Doctor whereRegionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Doctor whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Doctor extends Model
{
    protected $table='doctores';
    protected $fillable = ['nombre','region_id'];
}
