<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Especialidad
 *
 * @property int $id
 * @property string $nombre
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Especialidad newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Especialidad newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Especialidad query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Especialidad whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Especialidad whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Especialidad whereNombre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Especialidad whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Especialidad extends Model
{
    protected $table = 'especialidades';

    protected $fillable = ['nombre'];
}
