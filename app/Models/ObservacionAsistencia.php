<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ObservacionAsistencia
 *
 * @property int $id
 * @property int $asistencia_id
 * @property int $user_id
 * @property string $texto
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User $creador
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ObservacionAsistencia newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ObservacionAsistencia newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ObservacionAsistencia query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ObservacionAsistencia whereAsistenciaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ObservacionAsistencia whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ObservacionAsistencia whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ObservacionAsistencia whereTexto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ObservacionAsistencia whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ObservacionAsistencia whereUserId($value)
 * @mixin \Eloquent
 */
class ObservacionAsistencia extends Model
{
    protected $table = 'observacion_asistencias';
    protected $fillable=[
        'asistencia_id',
        'user_id',
        'texto'
    ];

    public function creador(){
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
}
