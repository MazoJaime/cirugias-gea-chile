<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PrecioUf
 *
 * @property int $id
 * @property float $precio
 * @property string $fecha
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PrecioUf newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PrecioUf newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PrecioUf query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PrecioUf whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PrecioUf whereFecha($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PrecioUf whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PrecioUf wherePrecio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PrecioUf whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class PrecioUf extends Model
{
    protected $table = 'precio_ufs';
    protected $fillable = ['precio', 'fecha'];

}
