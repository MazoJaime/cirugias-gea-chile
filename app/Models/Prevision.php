<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;



/**
 * App\Models\Prevision
 *
 * @property int $id
 * @property string $nombre
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Prevision newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Prevision newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Prevision query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Prevision whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Prevision whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Prevision whereNombre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Prevision whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Prevision extends Model
{


    protected $table = 'previsiones';
    //
    protected $fillable= ['nombre'];
}
