<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Procedimiento
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Procedimiento newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Procedimiento newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Procedimiento query()
 * @mixin \Eloquent
 * @property int $id
 * @property string $codigo
 * @property string $nombre
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Procedimiento whereCodigo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Procedimiento whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Procedimiento whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Procedimiento whereNombre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Procedimiento whereUpdatedAt($value)
 */
class Procedimiento extends Model
{
    protected $fillable = ['nombre', 'codigo'];
}
