<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Models\Diagnostico;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

$factory->define(Diagnostico::class, function (Faker $faker) {
    return [
        'nombre' => $faker->name,
    ];
});
