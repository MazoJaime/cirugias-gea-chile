<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAsistenciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asistencias', function (Blueprint $table) {
            $table->unsignedInteger('asistencia')->primary();
            $table->unsignedInteger('expediente')->index();
            $table->string('cuenta',30);
            $table->string('nombre_afiliado',100);
            $table->string('rut_afiliado',15);
            $table->date('apertura_expediente');
            $table->date('apertura_asistencia');
            $table->unsignedInteger('costo_asistencia')->default(0)->comment('obtenido de costos de asistencia en temp');
            $table->unsignedBigInteger('prevision_id')->nullable();
            $table->unsignedBigInteger('user_id');
            $table->foreign('prevision_id')->references('id')->on('previsiones');
            $table->foreign('user_id')->references('id')->on('users');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asistencias');
    }
}
