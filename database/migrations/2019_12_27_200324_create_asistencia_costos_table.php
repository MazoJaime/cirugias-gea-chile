<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAsistenciaCostosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('costo_asistencias', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('asistencia');
            $table->integer('costo')->default(0);
            $table->boolean('insumos')->nullable()->comment('indica si persona requiere o no insumos medicos');
            $table->integer('costo_insumos')->default(0);
            $table->boolean('complementos')->nullable();
            $table->integer('costo_complementos')->default(0);
            $table->integer('honorarios')->default(0);
            $table->integer('programa')->default(0);
            $table->unsignedBigInteger('bono_id');
            $table->foreign('bono_id')->references('id')->on('bonos');
            $table->timestamps();
            $table->foreign('asistencia')->references('asistencia')->on('asistencias');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('costo_asistencias');
    }
}
