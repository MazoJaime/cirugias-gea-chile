<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateObservacionAsistenciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('observacion_asistencias', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('asistencia_id');
            $table->unsignedBigInteger('user_id');
            $table->text('texto');
            $table->foreign('asistencia_id')->references('asistencia')->on('asistencias');
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('observacion_asistencias');
    }
}
