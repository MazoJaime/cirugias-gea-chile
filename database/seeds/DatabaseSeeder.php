<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Artisan;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PrevisionesTableSeeder::class);

        if ((new Role())->where('name', 'admin')->doesntExist()) {
            $roleadmin = Role::create([
                'id' => 1,
                'name' => 'admin'
            ]);
        }
        if ((new Role())->where('name', 'usuario')->doesntExist()) {
            $roleuser = Role::create([
                'id' => 2,
                'name' => 'usuario'
            ]);
        }
        $this->call(UsersTableSeeder::class);
        $this->call(PermisionsTableSeeder::class);

        $permissions = Permission::pluck('id', 'id')->all();
        $admin = \App\Models\User::where('user', 'mazojr')->first();
        $permissionsuser = Permission::where('name', 'like', 'cirugia%')->get();
        $user = \App\Models\User::where('user', 'lopezariv')->first();

        $roleadmin->syncPermissions($permissions);
        $admin->assignRole($roleadmin->id);
        $roleuser->syncPermissions($permissionsuser);
        $user->assignRole($roleuser->id);


        $this->call(ProcedimientosTableSeeder::class);
        $this->call(RegionesTableSeeder::class);
        $this->call(DoctoresTableSeeder::class);
        $this->call(EspecialidadesTableSeeder::class);
        Artisan::call('cirugia:nuevas');
    }
}
/**
 * 
 */
