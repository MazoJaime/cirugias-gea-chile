<?php

use Illuminate\Database\Seeder;

class DoctoresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $doctores = [['Toledo Juan Pablo', 13], ['PALOMINOS', 13], ['HARRY GUZMAN', 13], ['Escudero', 13], ['Cristobal Diaz', 13]];

        foreach ($doctores as $doctor) {
            \App\Models\Doctor::firstOrCreate(['nombre'=> $doctor[0], 'region_id' => $doctor[1]]);
        }
    }
}
