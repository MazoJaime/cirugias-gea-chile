<?php

use Illuminate\Database\Seeder;

class EspecialidadesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $especialidades = [
            'rodilla',
            'hombro',
            'tobillo',
            'mano',
            'tendon aquiles',
            'pies y manos',
            'rodillas y tobillo'
        ];

        foreach ($especialidades as $especialidad){
            \App\Models\Especialidad::firstOrCreate(['nombre' => $especialidad]);
        }
    }
}
