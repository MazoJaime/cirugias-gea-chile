<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermisionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            'mantenedores-list',
            'user-list',
            'user-create',
            'user-edit',
            'user-delete',
            'role-list',
            'role-create',
            'role-edit',
            'role-delete',
            'cirugia-list',
            'cirugia-create',
            'cirugia-edit',
            'bonos-list',
            'bonos-create',
            'bonos-edit',
            'diagnosticos-list',
            'diagnosticos-create',
            'diagnosticos-edit',
            'previsiones-list',
            'previsiones-edit',
            'previsiones-create',
            'procedimientos-list',
            'procedimientos-create',
            'procedimientos-edit',
            'regiones-list',
            'regiones-create',
            'regiones-edit'
        ];


        foreach ($permissions as $permission) {
            Permission::create(['name' => $permission]);
        }
    }
}
