<?php

use Illuminate\Database\Seeder;

class PrevisionesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $atenmedic_previsiones = array(
            array('id' => '1','nombre' => 'Sin Prevision'),
            array('id' => '2','nombre' => 'Consalud'),
            array('id' => '3','nombre' => 'Banmedica'),
            array('id' => '4','nombre' => 'Vida tres'),
            array('id' => '5','nombre' => 'Cruz blanca'),
            array('id' => '6','nombre' => 'Colmena'),
            array('id' => '8','nombre' => 'Mas vida'),
            array('id' => '9','nombre' => 'Optima'),
            array('id' => '10','nombre' => 'Dipreca'),
            array('id' => '11','nombre' => 'Capredena'),
            array('id' => '12','nombre' => 'Fundación'),
            array('id' => '13','nombre' => 'Fusat'),
            array('id' => '14','nombre' => 'Chuquicamata'),
            array('id' => '15','nombre' => 'Fonasa A'),
            array('id' => '16','nombre' => 'Fonasa B'),
            array('id' => '17','nombre' => 'Fonasa C'),
            array('id' => '18','nombre' => 'Fonasa D')
        );
        foreach ($atenmedic_previsiones as $prevision){
            \App\Models\Prevision::firstOrCreate([
                'nombre' => $prevision['nombre'],
                'id' => $prevision['id']
            ]);
        }
        //
    }
}
