<?php

use Illuminate\Database\Seeder;
use App\Models\Procedimiento;

class ProcedimientosTableSeeder extends Seeder
{
    public $filename;

    public function __construct()
    {
        $this->filename = database_path() . '/seeds/csv/procedimientos.csv';
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $header = ['codigo','nombre'];
        $items = array();

        if (($handle = fopen($this->filename, 'r')) !== FALSE) {
            while (($row = fgetcsv($handle, 1000, ';')) !== FALSE) {

                if (!$header) {
                    $header = $row;
                } else {
                    $items[] = array_combine($header, $row);
                }
            }
            fclose($handle);


            foreach ($items as $item) {
                Procedimiento::firstOrCreate([
                    'codigo' => $item['codigo'],
                    'nombre' => $item['nombre'],
                ]);
            }
        }

    }
}
