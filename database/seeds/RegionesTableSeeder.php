<?php

use Illuminate\Database\Seeder;

class RegionesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $regiones = [
            ['I Región de Tarapacá', 1],
            ['II Región de Antofagasta', 1],
            ['III Región de Atacama', 1],
            ['IV Región de Coquimbo', 1],
            ['V Región de Valparaíso', 1],
            ['VI Región del Libertador General Bernardo O’Higgins', 1],
            ['VII Región del Maule', 1],
            ['VIII Región del Biobío', 1],
            ['IX Región de La Araucanía', 1],
            ['X Región de Los Lagos', 1],
            ['XI Región Aysén del General Carlos Ibáñez del Campo', 1],
            ['XII Región de Magallanes y Antártica Chilena', 1],
            ['XIII Región Metropolitana de Santiago', 1],
            ['XIV Región de Los Ríos', 1],
            ['XV Región de Arica y Parinacota', 1],
            ['XVI Región de Ñuble', 1]
        ];

        foreach ($regiones as $region) {
            \App\Models\Region::firstOrCreate(['nombre' => $region[0], 'staff' => $region[1]]);
        }
    }
}
