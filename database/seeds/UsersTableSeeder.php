<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Jaime Mazo',
            'user' => 'mazojr',
            'rut' => '24544936-4',
            'active' => true,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('users')->insert([
            'name' => 'Admin',
            'user' => 'lopezariv',
            'rut' => '15565977-7',
            'active' => false,
            'created_at' => now(),
            'updated_at' => now()
        ]);

    }
}

