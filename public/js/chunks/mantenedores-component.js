(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["mantenedores-component"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/mantenedores/MantenedoresMain.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/mantenedores/MantenedoresMain.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/mantenedores/bonos/CrearBonos.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/mantenedores/bonos/CrearBonos.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      codigo: 0,
      descripcion: null,
      valor: null,
      categoria: null
    };
  },
  methods: {
    crearBono: function crearBono() {
      var _this = this;

      var formData = {
        codigo: Number(this.codigo),
        descripcion: this.descripcion,
        valor: Number(this.valor),
        categoria: Number(this.categoria)
      };
      axios.post('/api/mantenedores/bonos', formData).then(function (response) {
        if (response.data.bono) {
          _this.$swal({
            type: 'success',
            text: 'Bono  ' + response.data.bono.descripcion + ' Registrado correctamente'
          });

          _this.codigo = null;
          _this.descripcion = null;
          _this.valor = null;
          _this.categoria = null;
        }
      })["catch"](console.error);
    }
  },
  computed: {
    full: function full() {
      return !(this.codigo !== 0 && this.valor != null && this.descripcion !== null && this.categoria !== null);
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/mantenedores/bonos/EditarBonos.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/mantenedores/bonos/EditarBonos.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      codigo: 0,
      descripcion: null,
      valor: null,
      categoria: null
    };
  },
  created: function created() {
    var _this = this;

    axios.get('/api/mantenedores/bonos/' + this.$route.params.id).then(function (response) {
      _this.codigo = response.data.codigo;
      _this.descripcion = response.data.descripcion;
      _this.valor = response.data.valor;
      _this.categoria = response.data.categoria;
    });
  },
  methods: {
    updateBono: function updateBono() {
      var _this2 = this;

      var formData = {
        descripcion: this.descripcion,
        valor: Number(this.valor),
        categoria: Number(this.categoria)
      };
      axios.put('/api/mantenedores/bonos/' + this.$route.params.id, formData).then(function (response) {
        if (response.data.status === 'success') {
          _this2.$swal({
            type: 'success',
            text: 'Bono actualizado correctamente'
          });
        }
      })["catch"](console.error);
    }
  },
  computed: {
    full: function full() {
      return !(this.codigo !== 0 && this.valor != null && this.descripcion !== null && this.categoria !== null);
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/mantenedores/bonos/ListaBonos.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/mantenedores/bonos/ListaBonos.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    var sortOrders = {};
    var columns = [{
      label: 'Id',
      name: 'id'
    }, {
      label: 'Codigo',
      name: 'codigo'
    }, {
      label: 'Descripcion',
      name: 'descripcion'
    }, {
      label: 'Fecha Creacion',
      name: 'created_at'
    }, {
      label: 'Fecha Actualizacion',
      name: 'updated_at'
    }];
    columns.forEach(function (column) {
      sortOrders[column.name] = -1;
    });
    return {
      bonos: [],
      columns: columns,
      buscar: '',
      sortKey: '',
      sortOrders: sortOrders,
      length: 10,
      bono: Object,
      tableShow: {
        showdata: true
      },
      pagination: {
        value: 1,
        total: 0,
        from: 0,
        to: 0,
        perPage: 0
      }
    };
  },
  computed: {
    numberCols: function numberCols() {
      return this.columns.length + 1;
    }
  },
  methods: {
    search: function search() {
      var _this = this;

      var val = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;
      var url = '/api/mantenedores/bonos?page=' + val + '&length=' + this.length + '&search=' + this.buscar;

      if (this.buscar == "") {
        url = url + '&showdata=' + this.tableShow.showdata;
      }

      axios.get(url).then(function (response) {
        _this.bonos = response.data.data;
        _this.pagination.total = response.data.total;
        _this.pagination.from = response.data.from;
        _this.pagination.to = response.data.to;
        _this.pagination.value = response.data.current_page;
        _this.pagination.perPage = parseInt(response.data.per_page);
      })["catch"](function (errors) {
        console.log(errors);
      });
    },
    openmodal: function openmodal(val) {
      this.bono = val;
    },
    sortBy: function sortBy(key) {
      this.sortKey = key;
      this.sortOrders[key] = this.sortOrders[key] * -1;
      this.ordenarDatos();
    },
    getIndex: function getIndex(array, key, value) {
      return array.findIndex(function (i) {
        return i[key] == value;
      });
    },
    ordenarDatos: function ordenarDatos() {
      var _this2 = this;

      var bonos = this.bonos;
      var sortKey = this.sortKey;
      var order = this.sortOrders[sortKey] || 1;

      if (sortKey) {
        bonos = bonos.slice().sort(function (a, b) {
          var index = _this2.getIndex(_this2.columns, 'name', sortKey);

          a = String(a[sortKey]).toLowerCase();
          b = String(b[sortKey]).toLowerCase();

          if (_this2.columns[index].type && _this2.columns[index].type === 'date') {
            return (a === b ? 0 : new Date(a).getTime() > new Date(b).getTime() ? 1 : -1) * order;
          } else if (_this2.columns[index].type && _this2.columns[index].type === 'number') {
            return (+a === +b ? 0 : +a > +b ? 1 : -1) * order;
          } else {
            return (a === b ? 0 : a > b ? 1 : -1) * order;
          }
        });
      }

      this.bonos = bonos;
    }
  },
  created: function created() {
    this.search();
  },
  watch: {
    length: function length() {
      this.search();
    },
    buscar: _.debounce(function () {
      this.search();
    }, 500)
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/mantenedores/diagnosticos/CrearDiagnostico.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/mantenedores/diagnosticos/CrearDiagnostico.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      nombre: '',
      errors: []
    };
  },
  methods: {
    crearDiagnostico: function crearDiagnostico() {
      var _this = this;

      var url = '/api/mantenedores/diagnosticos';
      axios.post(url, {
        nombre: this.nombre
      }).then(function (response) {
        if (response.data.nombre) {
          _this.$swal({
            type: 'success',
            text: 'Diagnostico "' + response.data.nombre + '" se creo correctamente'
          });
        }
      })["catch"](function (error) {
        _this.errors = error.response.data.errors;
        var message = '';

        for (var _i = 0, _Object$keys = Object.keys(_this.errors); _i < _Object$keys.length; _i++) {
          var field = _Object$keys[_i];
          console.log(_this.errors[field][0]);
          message = _this.errors[field][0];
          /*  message += ' ' +(this.errors[field][0], 'error');*/
        }

        _this.$swal({
          type: 'error',
          text: 'El Diagnostico no ha podido ser creada debido a que :' + message
        });

        _this.errors = [];
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/mantenedores/diagnosticos/EditarDiagnostico.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/mantenedores/diagnosticos/EditarDiagnostico.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      nombre: '',
      id: 0,
      errors: []
    };
  },
  created: function created() {
    this.id = this.$route.params.id;
    this.getDiagnostico();
  },
  methods: {
    updateDiagnostico: function updateDiagnostico() {
      var _this = this;

      var url = '/api/mantenedores/diagnosticos/' + this.id;
      axios.put(url, {
        nombre: this.nombre
      }).then(function (response) {
        if (response.data) {
          _this.$swal({
            type: 'success',
            text: 'Prevision ' + response.data.nombre + ' se creo correctamente'
          });
        }
      })["catch"](function (error) {
        if (error.response.status === 403) {
          _this.$swal({
            type: 'error',
            text: error.response.data.message
          }).then(function (value) {
            window.location.href = '/';
          });
        } else {
          _this.errors = error.response.data.errors;
          var message = '';

          for (var _i = 0, _Object$keys = Object.keys(_this.errors); _i < _Object$keys.length; _i++) {
            var field = _Object$keys[_i];
            message = _this.errors[field][0];
            /*  message += ' ' +(this.errors[field][0], 'error');*/
          }

          _this.$swal({
            type: 'error',
            text: 'La prevision no ha podido ser modificada ' + message
          });

          _this.errors = [];
        }
      });
    },
    getDiagnostico: function getDiagnostico() {
      var _this2 = this;

      var url = '/api/mantenedores/diagnosticos/' + this.id;
      axios.get(url, {
        nombre: this.nombre
      }).then(function (response) {
        if (response.data) {
          _this2.nombre = response.data.nombre;
        }
      })["catch"](function (error) {
        var message = error.response.data.error;

        _this2.$swal({
          type: 'error',
          text: message
        });

        _this2.errors = [];
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/mantenedores/diagnosticos/ListaDiagnosticos.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/mantenedores/diagnosticos/ListaDiagnosticos.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    var sortOrders = {};
    var columns = [{
      label: 'Id',
      name: 'id'
    }, {
      label: 'Diagnostico',
      name: 'nombre'
    }, {
      label: 'Fecha Creacion',
      name: 'created_at'
    }, {
      label: 'Fecha Actualizacion',
      name: 'updated_at'
    }];
    columns.forEach(function (column) {
      sortOrders[column.name] = -1;
    });
    return {
      diagnosticos: [],
      columns: columns,
      sortKey: '',
      sortOrders: sortOrders,
      length: 10,
      diagnostico: Object,
      tableShow: {
        showdata: true
      },
      pagination: {
        value: 1,
        total: 0,
        from: 0,
        to: 0,
        perPage: 0
      }
    };
  },
  computed: {
    numberCols: function numberCols() {
      return this.columns.length + 1;
    }
  },
  methods: {
    search: function search(val) {
      var _this = this;

      var url = '/api/mantenedores/diagnosticos?page=' + val + '&length=' + this.length;
      console.log(val);
      axios.get(url, {
        params: this.tableShow
      }).then(function (response) {
        _this.diagnosticos = response.data.data;
        _this.pagination.total = response.data.total;
        _this.pagination.from = response.data.from;
        _this.pagination.to = response.data.to;
        _this.pagination.value = response.data.current_page;
        _this.pagination.perPage = parseInt(response.data.per_page);
      })["catch"](function (errors) {
        console.log(errors);
      });
    },
    openmodal: function openmodal(val) {
      this.diagnostico = val;
    }
  },
  created: function created() {
    this.search(1);
  },
  watch: {
    length: function length() {
      this.search(1);
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/mantenedores/previsiones/CrearPrevisiones.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/mantenedores/previsiones/CrearPrevisiones.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      nombre: '',
      errors: []
    };
  },
  methods: {
    crearPrevision: function crearPrevision() {
      var _this = this;

      var url = '/api/mantenedores/previsiones';
      axios.post(url, {
        nombre: this.nombre
      }).then(function (response) {
        if (response.data.prevision) {
          _this.$swal({
            type: 'success',
            text: 'Prevision ' + response.data.prevision.nombre + ' se creo correctamente'
          });
        }
      })["catch"](function (error) {
        _this.errors = error.response.data.errors;
        var message = '';

        for (var _i = 0, _Object$keys = Object.keys(_this.errors); _i < _Object$keys.length; _i++) {
          var field = _Object$keys[_i];
          console.log(_this.errors[field][0]);
          message = _this.errors[field][0];
          /*  message += ' ' +(this.errors[field][0], 'error');*/
        }

        _this.$swal({
          type: 'error',
          text: 'La prevision no ha podido ser creada debido a que :' + message
        });

        _this.errors = [];
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/mantenedores/previsiones/EditarPrevisiones.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/mantenedores/previsiones/EditarPrevisiones.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      nombre: '',
      id: 0,
      errors: []
    };
  },
  created: function created() {
    this.id = this.$route.params.id;
    this.getPrevision();
  },
  methods: {
    updatePrevision: function updatePrevision() {
      var _this = this;

      var url = '/api/mantenedores/previsiones/' + this.id;
      axios.put(url, {
        nombre: this.nombre
      }).then(function (response) {
        if (response.data.prevision) {
          _this.$swal({
            type: 'success',
            text: 'Prevision ' + response.data.prevision.nombre + ' se creo correctamente'
          });
        }
      })["catch"](function (error) {
        if (error.response.status === 403) {
          _this.$swal({
            type: 'error',
            text: error.response.data.message
          }).then(function (value) {
            window.location.href = '/';
          });
        } else {
          _this.errors = error.response.data.errors;
          var message = '';

          for (var _i = 0, _Object$keys = Object.keys(_this.errors); _i < _Object$keys.length; _i++) {
            var field = _Object$keys[_i];
            message = _this.errors[field][0];
            /*  message += ' ' +(this.errors[field][0], 'error');*/
          }

          _this.$swal({
            type: 'error',
            text: 'La prevision no ha podido ser modificada ' + message
          });

          _this.errors = [];
        }
      });
    },
    getPrevision: function getPrevision() {
      var _this2 = this;

      var url = '/api/mantenedores/previsiones/' + this.id;
      axios.get(url, {
        nombre: this.nombre
      }).then(function (response) {
        if (response.data.prevision) {
          _this2.nombre = response.data.prevision.nombre;
        }
      })["catch"](function (error) {
        var message = error.response.data.error;

        _this2.$swal({
          type: 'error',
          text: message
        });

        _this2.errors = [];
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/mantenedores/previsiones/ListaPrevisiones.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/mantenedores/previsiones/ListaPrevisiones.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    var sortOrders = {};
    var columns = [{
      label: 'Id',
      name: 'id'
    }, {
      label: 'Prevision',
      name: 'nombre'
    }, {
      label: 'Fecha Creacion',
      name: 'created_at'
    }, {
      label: 'Fecha Actualizacion',
      name: 'updated_at'
    }];
    columns.forEach(function (column) {
      sortOrders[column.name] = -1;
    });
    return {
      previsions: [],
      columns: columns,
      buscar: '',
      sortKey: '',
      sortOrders: sortOrders,
      length: 10,
      prevision: Object,
      tableShow: {
        showdata: true
      },
      pagination: {
        value: 1,
        total: 0,
        from: 0,
        to: 0,
        perPage: 0
      }
    };
  },
  computed: {
    numberCols: function numberCols() {
      return this.columns.length + 1;
    }
  },
  methods: {
    search: function search() {
      var _this = this;

      var val = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;
      var url = '/api/mantenedores/previsiones?page=' + val + '&length=' + this.length + '&search=' + this.buscar;

      if (this.buscar == "") {
        url = url + '&showdata=' + this.tableShow.showdata;
      }

      axios.get(url).then(function (response) {
        _this.previsions = response.data.data;
        _this.pagination.total = response.data.total;
        _this.pagination.from = response.data.from;
        _this.pagination.to = response.data.to;
        _this.pagination.value = response.data.current_page;
        _this.pagination.perPage = parseInt(response.data.per_page);
      })["catch"](function (errors) {
        console.log(errors);
      });
    },
    openmodal: function openmodal(val) {
      this.prevision = val;
    },
    sortBy: function sortBy(key) {
      this.sortKey = key;
      this.sortOrders[key] = this.sortOrders[key] * -1;
      this.ordenarDatos();
    },
    getIndex: function getIndex(array, key, value) {
      return array.findIndex(function (i) {
        return i[key] == value;
      });
    },
    ordenarDatos: function ordenarDatos() {
      var _this2 = this;

      var previsiones = this.previsions;
      var sortKey = this.sortKey;
      var order = this.sortOrders[sortKey] || 1;

      if (sortKey) {
        previsiones = previsiones.slice().sort(function (a, b) {
          var index = _this2.getIndex(_this2.columns, 'name', sortKey);

          a = String(a[sortKey]).toLowerCase();
          b = String(b[sortKey]).toLowerCase();

          if (_this2.columns[index].type && _this2.columns[index].type === 'date') {
            return (a === b ? 0 : new Date(a).getTime() > new Date(b).getTime() ? 1 : -1) * order;
          } else if (_this2.columns[index].type && _this2.columns[index].type === 'number') {
            return (+a === +b ? 0 : +a > +b ? 1 : -1) * order;
          } else {
            return (a === b ? 0 : a > b ? 1 : -1) * order;
          }
        });
      }

      this.previsions = previsiones;
    }
  },
  created: function created() {
    this.search();
  },
  watch: {
    length: function length() {
      this.search();
    },
    buscar: _.debounce(function () {
      this.search();
    }, 500)
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/mantenedores/procedimientos/CrearProcedimiento.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/mantenedores/procedimientos/CrearProcedimiento.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      nombre: '',
      codigo: '',
      errors: []
    };
  },
  methods: {
    crearDiagnostico: function crearDiagnostico() {
      var _this = this;

      var url = '/api/mantenedores/procedimientos';
      axios.post(url, {
        nombre: this.nombre,
        codigo: this.codigo
      }).then(function (response) {
        if (response.data.nombre && response.data.codigo) {
          _this.$swal({
            type: 'success',
            text: 'Procedimiento "' + response.data.nombre + '" con codigo ' + response.data.codigo + ' se creo correctamente'
          }).then(function (value) {
            _this.nombre = '';
            _this.codigo = '';
          });
        }
      })["catch"](function (error) {
        _this.errors = error.response.data.errors;
        var message = '';

        for (var _i = 0, _Object$keys = Object.keys(_this.errors); _i < _Object$keys.length; _i++) {
          var field = _Object$keys[_i];
          console.log(_this.errors[field][0]);
          message = _this.errors[field][0];
          /*  message += ' ' +(this.errors[field][0], 'error');*/
        }

        _this.$swal({
          type: 'error',
          text: 'El Procedimiento no ha podido ser creada debido a que :' + message
        });

        _this.errors = [];
      });
    }
  },
  computed: {
    disabled: function disabled() {
      return this.nombre.length < 3 && this.codigo.length < 3;
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/mantenedores/procedimientos/EditarProcedimientos.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/mantenedores/procedimientos/EditarProcedimientos.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      nombre: '',
      codigo: '',
      id: 0,
      errors: []
    };
  },
  created: function created() {
    this.id = this.$route.params.id;
    this.getProcedimiento();
  },
  methods: {
    updateProcedimiento: function updateProcedimiento() {
      var _this = this;

      var url = '/api/mantenedores/procedimientos/' + this.id;
      axios.put(url, {
        nombre: this.nombre,
        codigo: this.codigo
      }).then(function (response) {
        if (response.data.nombre) {
          _this.$swal({
            type: 'success',
            text: 'Procedimiento Actualizado Correctamente'
          });
        }
      })["catch"](function (error) {
        if (error.response.status === 403) {
          _this.$swal({
            type: 'error',
            text: error.response.data.message
          }).then(function (value) {
            window.location.href = '/';
          });
        } else {
          _this.errors = error.response.data.errors;
          var message = '';

          for (var _i = 0, _Object$keys = Object.keys(_this.errors); _i < _Object$keys.length; _i++) {
            var field = _Object$keys[_i];
            message = _this.errors[field][0];
            /*  message += ' ' +(this.errors[field][0], 'error');*/
          }

          _this.$swal({
            type: 'error',
            text: 'El procedimiento no ha podido ser modificado ' + message
          });

          _this.errors = [];
        }
      });
    },
    getProcedimiento: function getProcedimiento() {
      var _this2 = this;

      var url = '/api/mantenedores/procedimientos/' + this.id;
      axios.get(url, {
        nombre: this.nombre
      }).then(function (response) {
        if (response.data) {
          _this2.nombre = response.data.nombre;
          _this2.codigo = response.data.codigo;
        }
      })["catch"](function (error) {
        var message = error.response.data.error;

        _this2.$swal({
          type: 'error',
          text: message
        });

        _this2.errors = [];
      });
    }
  },
  computed: {
    disabled: function disabled() {
      return this.nombre.length < 3 && this.codigo.length < 3;
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/mantenedores/procedimientos/ListaProcedimientos.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/mantenedores/procedimientos/ListaProcedimientos.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    var sortOrders = {};
    var columns = [{
      label: 'Id',
      name: 'id'
    }, {
      label: 'Codigo',
      name: 'codigo'
    }, {
      label: 'Procedimiento',
      name: 'nombre'
    }, {
      label: 'Fecha Creacion',
      name: 'created_at'
    }, {
      label: 'Fecha Actualizacion',
      name: 'updated_at'
    }];
    columns.forEach(function (column) {
      sortOrders[column.name] = -1;
    });
    return {
      procedimientos: [],
      columns: columns,
      sortKey: '',
      sortOrders: sortOrders,
      length: 10,
      procedimiento: Object,
      tableShow: {
        showdata: true
      },
      pagination: {
        value: 1,
        total: 0,
        from: 0,
        to: 0,
        perPage: 0
      }
    };
  },
  computed: {
    numberCols: function numberCols() {
      return this.columns.length + 1;
    }
  },
  methods: {
    search: function search(val) {
      var _this = this;

      var url = '/api/mantenedores/procedimientos?page=' + val + '&length=' + this.length;
      axios.get(url, {
        params: this.tableShow
      }).then(function (response) {
        _this.procedimientos = response.data.data;
        _this.pagination.total = response.data.total;
        _this.pagination.from = response.data.from;
        _this.pagination.to = response.data.to;
        _this.pagination.value = response.data.current_page;
        _this.pagination.perPage = parseInt(response.data.per_page);
      })["catch"](function (errors) {
        console.log(errors);
      });
    },
    openmodal: function openmodal(val) {
      this.procedimiento = val;
    }
  },
  created: function created() {
    this.search(1);
  },
  watch: {
    length: function length() {
      this.search(1);
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/mantenedores/regiones/CrearRegion.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/mantenedores/regiones/CrearRegion.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      nombre: '',
      staff: true,
      errors: []
    };
  },
  methods: {
    crearDiagnostico: function crearDiagnostico() {
      var _this = this;

      var url = '/api/mantenedores/regiones';
      axios.post(url, {
        nombre: this.nombre,
        staff: this.staff
      }).then(function (response) {
        if (response.data.nombre && response.data.staff) {
          _this.$swal({
            type: 'success',
            text: 'region "' + response.data.nombre + '" se creo correctamente'
          }).then(function (value) {
            _this.nombre = '';
            _this.staff = '';
          });
        }
      })["catch"](function (error) {
        _this.errors = error.response.data.errors;
        var message = '';

        for (var _i = 0, _Object$keys = Object.keys(_this.errors); _i < _Object$keys.length; _i++) {
          var field = _Object$keys[_i];
          console.log(_this.errors[field][0]);
          message = _this.errors[field][0];
          /*  message += ' ' +(this.errors[field][0], 'error');*/
        }

        _this.$swal({
          type: 'error',
          text: 'El region no ha podido ser creada debido a que :' + message
        });

        _this.errors = [];
      });
    }
  },
  computed: {
    disabled: function disabled() {
      return this.nombre.length < 3;
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/mantenedores/regiones/EditarRegion.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/mantenedores/regiones/EditarRegion.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      nombre: '',
      staff: '',
      id: 0,
      errors: []
    };
  },
  created: function created() {
    this.id = this.$route.params.id;
    this.getregion();
  },
  methods: {
    updateregion: function updateregion() {
      var _this = this;

      var url = '/api/mantenedores/regiones/' + this.id;
      axios.put(url, {
        nombre: this.nombre,
        staff: this.staff
      }).then(function (response) {
        if (response.data.nombre) {
          _this.$swal({
            type: 'success',
            text: 'Region Actualizado Correctamente'
          });
        }
      })["catch"](function (error) {
        if (error.response.status === 403) {
          _this.$swal({
            type: 'error',
            text: error.response.data.message
          }).then(function (value) {
            window.location.href = '/';
          });
        } else {
          _this.errors = error.response.data.errors;
          var message = '';

          for (var _i = 0, _Object$keys = Object.keys(_this.errors); _i < _Object$keys.length; _i++) {
            var field = _Object$keys[_i];
            message = _this.errors[field][0];
            /*  message += ' ' +(this.errors[field][0], 'error');*/
          }

          _this.$swal({
            type: 'error',
            text: 'La region no ha podido ser modificado ' + message
          });

          _this.errors = [];
        }
      });
    },
    getregion: function getregion() {
      var _this2 = this;

      var url = '/api/mantenedores/regiones/' + this.id;
      axios.get(url).then(function (response) {
        if (response.data) {
          _this2.nombre = response.data.nombre;
          _this2.staff = response.data.staff;
        }
      })["catch"](function (error) {
        var message = error.response.data.error;

        _this2.$swal({
          type: 'error',
          text: message
        });

        _this2.errors = [];
      });
    }
  },
  computed: {
    disabled: function disabled() {
      return this.nombre.length < 3;
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/mantenedores/regiones/ListaRegiones.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/mantenedores/regiones/ListaRegiones.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    var sortOrders = {};
    var columns = [{
      label: 'Id',
      name: 'id'
    }, {
      label: 'Region',
      name: 'nombre'
    }, {
      label: 'Staff',
      name: 'staff'
    }, {
      label: 'Fecha Creacion',
      name: 'created_at'
    }, {
      label: 'Fecha Actualizacion',
      name: 'updated_at'
    }];
    columns.forEach(function (column) {
      sortOrders[column.name] = -1;
    });
    return {
      regions: [],
      columns: columns,
      buscar: '',
      sortKey: '',
      sortOrders: sortOrders,
      length: 10,
      region: Object,
      tableShow: {
        showdata: true
      },
      pagination: {
        value: 1,
        total: 0,
        from: 0,
        to: 0,
        perPage: 0
      }
    };
  },
  computed: {
    numberCols: function numberCols() {
      return this.columns.length + 1;
    }
  },
  methods: {
    search: function search() {
      var _this = this;

      var val = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;
      var url = '/api/mantenedores/regiones?page=' + val + '&length=' + this.length + '&search=' + this.buscar;

      if (this.buscar == "") {
        url = url + '&showdata=' + this.tableShow.showdata;
      }

      axios.get(url).then(function (response) {
        _this.regions = response.data.data;
        _this.pagination.total = response.data.total;
        _this.pagination.from = response.data.from;
        _this.pagination.to = response.data.to;
        _this.pagination.value = response.data.current_page;
        _this.pagination.perPage = parseInt(response.data.per_page);
      })["catch"](function (errors) {
        console.log(errors);
      });
    },
    openmodal: function openmodal(val) {
      this.region = val;
    },
    sortBy: function sortBy(key) {
      this.sortKey = key;
      this.sortOrders[key] = this.sortOrders[key] * -1;
      this.ordenarDatos();
    },
    getIndex: function getIndex(array, key, value) {
      return array.findIndex(function (i) {
        return i[key] == value;
      });
    },
    ordenarDatos: function ordenarDatos() {
      var _this2 = this;

      var regiones = this.regions;
      var sortKey = this.sortKey;
      var order = this.sortOrders[sortKey] || 1;

      if (sortKey) {
        regiones = regiones.slice().sort(function (a, b) {
          var index = _this2.getIndex(_this2.columns, 'name', sortKey);

          a = String(a[sortKey]).toLowerCase();
          b = String(b[sortKey]).toLowerCase();

          if (_this2.columns[index].type && _this2.columns[index].type === 'date') {
            return (a === b ? 0 : new Date(a).getTime() > new Date(b).getTime() ? 1 : -1) * order;
          } else if (_this2.columns[index].type && _this2.columns[index].type === 'number') {
            return (+a === +b ? 0 : +a > +b ? 1 : -1) * order;
          } else {
            return (a === b ? 0 : a > b ? 1 : -1) * order;
          }
        });
      }

      this.regions = regiones;
    }
  },
  created: function created() {
    this.search();
  },
  watch: {
    length: function length() {
      this.search();
    },
    buscar: _.debounce(function () {
      this.search();
    }, 500)
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/mantenedores/MantenedoresMain.vue?vue&type=template&id=40be0e66&scoped=true&":
/*!********************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/mantenedores/MantenedoresMain.vue?vue&type=template&id=40be0e66&scoped=true& ***!
  \********************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("router-view")
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/mantenedores/bonos/CrearBonos.vue?vue&type=template&id=2d35cd49&scoped=true&":
/*!********************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/mantenedores/bonos/CrearBonos.vue?vue&type=template&id=2d35cd49&scoped=true& ***!
  \********************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm.$can("bonos-create")
    ? _c("div", { staticClass: "card col-6 center" }, [
        _c("div", { staticClass: "card-header bg-white border-0" }, [
          _c("div", { staticClass: "row align-items-center" }, [
            _vm._m(0),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "col-4 text-right" },
              [
                _c(
                  "router-link",
                  {
                    staticClass: "btn btn-dark",
                    attrs: { to: { name: "bonos-list" } }
                  },
                  [_vm._v("Volver a listado de Bonos\n                ")]
                )
              ],
              1
            )
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "card-body" }, [
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-12" }, [
              _c("div", { staticClass: "form-group" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.codigo,
                      expression: "codigo"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: { type: "number", placeholder: "Codigo del bono" },
                  domProps: { value: _vm.codigo },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.codigo = $event.target.value
                    }
                  }
                })
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "form-group" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.descripcion,
                      expression: "descripcion"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: { type: "text", placeholder: "Descripcion del bono" },
                  domProps: { value: _vm.descripcion },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.descripcion = $event.target.value
                    }
                  }
                })
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "form-group" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.valor,
                      expression: "valor"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: {
                    type: "number",
                    placeholder: "Descripcion del bono"
                  },
                  domProps: { value: _vm.valor },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.valor = $event.target.value
                    }
                  }
                })
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "form-group" }, [
                _c(
                  "select",
                  {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.categoria,
                        expression: "categoria"
                      }
                    ],
                    staticClass: "form-control",
                    on: {
                      change: function($event) {
                        var $$selectedVal = Array.prototype.filter
                          .call($event.target.options, function(o) {
                            return o.selected
                          })
                          .map(function(o) {
                            var val = "_value" in o ? o._value : o.value
                            return val
                          })
                        _vm.categoria = $event.target.multiple
                          ? $$selectedVal
                          : $$selectedVal[0]
                      }
                    }
                  },
                  [
                    _c("option", { attrs: { value: "null" } }, [
                      _vm._v(" Escoja")
                    ]),
                    _vm._v(" "),
                    _c("option", { attrs: { value: "1" } }, [
                      _vm._v("Bono Pad")
                    ]),
                    _vm._v(" "),
                    _c("option", { attrs: { value: "2" } }, [
                      _vm._v("Cuenta Abierta")
                    ])
                  ]
                )
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "form-group" }, [
                _c(
                  "button",
                  {
                    staticClass: "btn btn-success",
                    attrs: { disabled: _vm.full },
                    on: { click: _vm.crearBono }
                  },
                  [_vm._v("Guardar\n                    ")]
                )
              ])
            ])
          ])
        ])
      ])
    : _c("div", [_vm._v(" No tienes permisos para acceder a esta area")])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-8" }, [
      _c("h3", { staticClass: "mb-0" }, [_vm._v("Registrar Nuevo Bono")])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/mantenedores/bonos/EditarBonos.vue?vue&type=template&id=29b60dcf&scoped=true&":
/*!*********************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/mantenedores/bonos/EditarBonos.vue?vue&type=template&id=29b60dcf&scoped=true& ***!
  \*********************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm.$can("bonos-edit")
    ? _c("div", { staticClass: "card col-6 center" }, [
        _c("div", { staticClass: "card-header bg-white border-0" }, [
          _c("div", { staticClass: "row align-items-center" }, [
            _vm._m(0),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "col-4 text-right" },
              [
                _c(
                  "router-link",
                  {
                    staticClass: "btn btn-dark",
                    attrs: { to: { name: "bonos-list" } }
                  },
                  [_vm._v("Volver a listado de Bonos\n                ")]
                )
              ],
              1
            )
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "card-body" }, [
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-12" }, [
              _c("div", { staticClass: "form-group" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.codigo,
                      expression: "codigo"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: {
                    type: "number",
                    placeholder: "Codigo del bono",
                    readonly: ""
                  },
                  domProps: { value: _vm.codigo },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.codigo = $event.target.value
                    }
                  }
                })
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "form-group" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.descripcion,
                      expression: "descripcion"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: { type: "text", placeholder: "Descripcion del bono" },
                  domProps: { value: _vm.descripcion },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.descripcion = $event.target.value
                    }
                  }
                })
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "form-group" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.valor,
                      expression: "valor"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: {
                    type: "number",
                    placeholder: "Descripcion del bono"
                  },
                  domProps: { value: _vm.valor },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.valor = $event.target.value
                    }
                  }
                })
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "form-group" }, [
                _c(
                  "select",
                  {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.categoria,
                        expression: "categoria"
                      }
                    ],
                    staticClass: "form-control",
                    on: {
                      change: function($event) {
                        var $$selectedVal = Array.prototype.filter
                          .call($event.target.options, function(o) {
                            return o.selected
                          })
                          .map(function(o) {
                            var val = "_value" in o ? o._value : o.value
                            return val
                          })
                        _vm.categoria = $event.target.multiple
                          ? $$selectedVal
                          : $$selectedVal[0]
                      }
                    }
                  },
                  [
                    _c("option", { attrs: { value: "null" } }, [
                      _vm._v(" Escoja")
                    ]),
                    _vm._v(" "),
                    _c("option", { attrs: { value: "1" } }, [
                      _vm._v("Bono Pad")
                    ]),
                    _vm._v(" "),
                    _c("option", { attrs: { value: "2" } }, [
                      _vm._v("Cuenta Abierta")
                    ])
                  ]
                )
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "form-group" }, [
                _c(
                  "button",
                  {
                    staticClass: "btn btn-success",
                    attrs: { disabled: _vm.full },
                    on: { click: _vm.updateBono }
                  },
                  [_vm._v("Guardar Cambios\n                    ")]
                )
              ])
            ])
          ])
        ])
      ])
    : _c("div", [_vm._v(" No tienes permisos para acceder a esta area")])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-8" }, [
      _c("h3", { staticClass: "mb-0" }, [_vm._v("Editar información de bono")])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/mantenedores/bonos/ListaBonos.vue?vue&type=template&id=09931466&scoped=true&":
/*!********************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/mantenedores/bonos/ListaBonos.vue?vue&type=template&id=09931466&scoped=true& ***!
  \********************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm.$can("bonos-list")
    ? _c(
        "div",
        { staticClass: "card" },
        [
          _vm._v(">\n    "),
          _c("div", { staticClass: "card-header" }, [
            _c("div", { staticClass: "row" }, [
              _c("div", { staticClass: "col" }, [
                _c(
                  "select",
                  {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.length,
                        expression: "length"
                      }
                    ],
                    staticClass: "form-control ",
                    staticStyle: { "max-width": "10rem" },
                    on: {
                      change: function($event) {
                        var $$selectedVal = Array.prototype.filter
                          .call($event.target.options, function(o) {
                            return o.selected
                          })
                          .map(function(o) {
                            var val = "_value" in o ? o._value : o.value
                            return val
                          })
                        _vm.length = $event.target.multiple
                          ? $$selectedVal
                          : $$selectedVal[0]
                      }
                    }
                  },
                  [
                    _c("option", { attrs: { value: "10" } }, [_vm._v("10")]),
                    _vm._v(" "),
                    _c("option", { attrs: { value: "25" } }, [_vm._v("25")]),
                    _vm._v(" "),
                    _c("option", { attrs: { value: "50" } }, [_vm._v("50")]),
                    _vm._v(" "),
                    _c("option", { attrs: { value: "100" } }, [_vm._v("100")])
                  ]
                )
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "col align-content-center" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.buscar,
                      expression: "buscar"
                    }
                  ],
                  staticClass: "form-control  ",
                  attrs: { type: "text", placeholder: "Buscar" },
                  domProps: { value: _vm.buscar },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.buscar = $event.target.value
                    }
                  }
                })
              ]),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "col" },
                [
                  _c(
                    "router-link",
                    {
                      staticClass: "btn btn-success float-right",
                      attrs: { to: { name: "bonos-crear" } }
                    },
                    [_vm._v("Nuevo Bono\n                ")]
                  )
                ],
                1
              )
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "card-body" }, [
            _c(
              "table",
              {
                staticClass:
                  "table table-condensed align-items-center table-bordered"
              },
              [
                _c("base-table-head", {
                  staticClass: "thead-light",
                  attrs: {
                    columns: _vm.columns,
                    sortOrders: _vm.sortOrders,
                    sortKey: _vm.sortKey
                  },
                  on: { sortBy: _vm.sortBy }
                }),
                _vm._v(" "),
                _vm.bonos
                  ? _c(
                      "tbody",
                      _vm._l(_vm.bonos, function(bono) {
                        return _c(
                          "tr",
                          { key: bono.id, staticClass: "text-center" },
                          [
                            _c("td", [_vm._v(_vm._s(bono.id))]),
                            _vm._v(" "),
                            _c("td", [_vm._v(_vm._s(bono.codigo))]),
                            _vm._v(" "),
                            _c("td", [_vm._v(_vm._s(bono.descripcion))]),
                            _vm._v(" "),
                            _c("td", [_vm._v(_vm._s(bono.created_at))]),
                            _vm._v(" "),
                            _c("td", [_vm._v(_vm._s(bono.updated_at))]),
                            _vm._v(" "),
                            _c("td", [
                              _c("div", { staticClass: "dropdown" }, [
                                _vm._m(0, true),
                                _vm._v(" "),
                                _c(
                                  "div",
                                  {
                                    staticClass:
                                      "dropdown-menu dropdown-menu-right dropdown-menu-arrow"
                                  },
                                  [
                                    _c(
                                      "a",
                                      {
                                        staticClass: "dropdown-item",
                                        attrs: {
                                          href: "#",
                                          "data-toggle": "modal",
                                          "data-target": "#modal-notification"
                                        },
                                        on: {
                                          click: function($event) {
                                            return _vm.openmodal(bono)
                                          }
                                        }
                                      },
                                      [
                                        _c("i", { staticClass: "fas fa-eye" }),
                                        _vm._v(
                                          "Ver\n                            "
                                        )
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "router-link",
                                      {
                                        staticClass: "dropdown-item",
                                        attrs: {
                                          to: {
                                            name: "bonos-editar",
                                            params: { id: bono.id }
                                          }
                                        }
                                      },
                                      [
                                        _c("i", { staticClass: "fas fa-pen" }),
                                        _vm._v(
                                          "Editar\n                            "
                                        )
                                      ]
                                    )
                                  ],
                                  1
                                )
                              ])
                            ])
                          ]
                        )
                      }),
                      0
                    )
                  : _c("tbody", [
                      _c("tr", [
                        _c(
                          "td",
                          {
                            staticClass: "text-center",
                            attrs: { colspan: _vm.numberCols }
                          },
                          [_vm._v("No se ha entoncontrado informacion")]
                        )
                      ])
                    ])
              ],
              1
            )
          ]),
          _vm._v(" "),
          _c(
            "base-pagination",
            _vm._b(
              {
                attrs: { align: "center" },
                on: { "pagination-change-page": _vm.search }
              },
              "base-pagination",
              _vm.pagination,
              false
            )
          ),
          _vm._v(" "),
          _c(
            "base-modal",
            [
              _c(
                "h5",
                {
                  staticClass: "modal-title",
                  attrs: { slot: "header", id: "modal-title-notification" },
                  slot: "header"
                },
                [_vm._v("\n            Informacion del bono\n        ")]
              ),
              _vm._v(" "),
              _c("p", { attrs: { slot: "body" }, slot: "body" }, [
                _vm._v(
                  "\n            Codigo del Bono: " + _vm._s(_vm.bono.codigo)
                ),
                _c("br"),
                _vm._v(
                  "\n            Descripcion del bono: " +
                    _vm._s(_vm.bono.descripcion)
                ),
                _c("br"),
                _vm._v(
                  "\n            Precio del bono: $" + _vm._s(_vm.bono.valor)
                ),
                _c("br"),
                _vm._v(
                  "\n            Tipo de Bono: " +
                    _vm._s(
                      _vm.bono.categoria == "1" ? "Bono Pad" : "Cuenta Abierta"
                    )
                ),
                _c("br"),
                _vm._v(
                  "\n            Fecha de creacion: " +
                    _vm._s(_vm.bono.created_at) +
                    "\n        "
                )
              ]),
              _vm._v(" "),
              _c("template", { slot: "footer" }, [
                _c(
                  "button",
                  {
                    staticClass: "btn ml-auto btn-default",
                    attrs: { type: "button", "data-dismiss": "modal" }
                  },
                  [_vm._v("Cerrar")]
                )
              ])
            ],
            2
          )
        ],
        1
      )
    : _c("div", [_vm._v(" No tienes permisos para acceder a esta area")])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "a",
      {
        staticClass: "btn btn-sm btn-icon-only text-light",
        attrs: {
          href: "#",
          role: "button",
          "data-toggle": "dropdown",
          "aria-haspopup": "true",
          "aria-expanded": "false"
        }
      },
      [_c("i", { staticClass: "fas fa-ellipsis-v" })]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/mantenedores/diagnosticos/CrearDiagnostico.vue?vue&type=template&id=6287a8b4&scoped=true&":
/*!*********************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/mantenedores/diagnosticos/CrearDiagnostico.vue?vue&type=template&id=6287a8b4&scoped=true& ***!
  \*********************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm.$can("diagnosticos-create")
    ? _c("div", { staticClass: "card col-6 center" }, [
        _c("div", { staticClass: "card-header bg-white border-0" }, [
          _c("div", { staticClass: "row align-items-center" }, [
            _vm._m(0),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "col-4 text-right" },
              [
                _c(
                  "router-link",
                  {
                    staticClass: "btn btn-dark",
                    attrs: { to: { name: "diagnostico-list" } }
                  },
                  [_vm._v("Volver a listado")]
                )
              ],
              1
            )
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "card-body" }, [
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-12" }, [
              _c("div", { staticClass: "form-group" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.nombre,
                      expression: "nombre"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: { type: "text", placeholder: "Nombre de Diagnostico" },
                  domProps: { value: _vm.nombre },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.nombre = $event.target.value
                    }
                  }
                })
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "form-group" }, [
                _c(
                  "button",
                  {
                    staticClass: "btn btn-success",
                    attrs: { disabled: this.nombre.length < 3 },
                    on: { click: _vm.crearDiagnostico }
                  },
                  [_vm._v("Guardar\n                    ")]
                )
              ])
            ])
          ])
        ])
      ])
    : _vm._e()
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-8" }, [
      _c("h3", { staticClass: "mb-0" }, [_vm._v("Registrar Nuevo Diagnostico")])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/mantenedores/diagnosticos/EditarDiagnostico.vue?vue&type=template&id=4537da6a&scoped=true&":
/*!**********************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/mantenedores/diagnosticos/EditarDiagnostico.vue?vue&type=template&id=4537da6a&scoped=true& ***!
  \**********************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm.$can("diagnosticos-edit")
    ? _c("div", { staticClass: "card col-6 center" }, [
        _vm._v(">\n    "),
        _c("div", { staticClass: "card-header bg-white border-0" }, [
          _c("div", { staticClass: "row align-items-center" }, [
            _vm._m(0),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "col-4 text-right" },
              [
                _c(
                  "router-link",
                  {
                    staticClass: "btn btn-dark",
                    attrs: { to: { name: "diagnostico-list" } }
                  },
                  [_vm._v("Volver a listado")]
                )
              ],
              1
            )
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "card-body" }, [
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-12" }, [
              _c("div", { staticClass: "form-group" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.nombre,
                      expression: "nombre"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: { type: "text", placeholder: "Nombre de Diagnostico" },
                  domProps: { value: _vm.nombre },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.nombre = $event.target.value
                    }
                  }
                })
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "form-group" }, [
                _c(
                  "button",
                  {
                    staticClass: "btn btn-success",
                    attrs: { disabled: this.nombre.length < 3 },
                    on: { click: _vm.updateDiagnostico }
                  },
                  [_vm._v("Guardar\n                    ")]
                )
              ])
            ])
          ])
        ])
      ])
    : _c("div", [_vm._v("No tiene permisos para acceder a esta area")])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-8" }, [
      _c("h3", { staticClass: "mb-0" }, [
        _vm._v("Actualizar Nombre de Diagnostico")
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/mantenedores/diagnosticos/ListaDiagnosticos.vue?vue&type=template&id=8cb2ec2a&scoped=true&":
/*!**********************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/mantenedores/diagnosticos/ListaDiagnosticos.vue?vue&type=template&id=8cb2ec2a&scoped=true& ***!
  \**********************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm.$can("diagnosticos-list")
    ? _c(
        "div",
        { staticClass: "card" },
        [
          _c("div", { staticClass: "card-header" }, [
            _c(
              "div",
              { staticClass: "row" },
              [
                _c("div", { staticClass: "col" }, [
                  _c(
                    "select",
                    {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.length,
                          expression: "length"
                        }
                      ],
                      staticClass: "form-control ",
                      staticStyle: { "max-width": "10rem" },
                      on: {
                        change: function($event) {
                          var $$selectedVal = Array.prototype.filter
                            .call($event.target.options, function(o) {
                              return o.selected
                            })
                            .map(function(o) {
                              var val = "_value" in o ? o._value : o.value
                              return val
                            })
                          _vm.length = $event.target.multiple
                            ? $$selectedVal
                            : $$selectedVal[0]
                        }
                      }
                    },
                    [
                      _c("option", { attrs: { value: "10" } }, [_vm._v("10")]),
                      _vm._v(" "),
                      _c("option", { attrs: { value: "25" } }, [_vm._v("25")]),
                      _vm._v(" "),
                      _c("option", { attrs: { value: "50" } }, [_vm._v("50")]),
                      _vm._v(" "),
                      _c("option", { attrs: { value: "100" } }, [_vm._v("100")])
                    ]
                  )
                ]),
                _vm._v(" "),
                _c(
                  "router-link",
                  {
                    staticClass: " rigth btn btn-success",
                    attrs: { to: { name: "diagnostico-crear" } }
                  },
                  [_vm._v("Nuevo Diagnostico\n            ")]
                )
              ],
              1
            )
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "card-body" }, [
            _c(
              "table",
              {
                staticClass:
                  "table table-condensed align-items-center table-bordered"
              },
              [
                _c("base-table-head", {
                  staticClass: "thead-light",
                  attrs: { columns: _vm.columns }
                }),
                _vm._v(" "),
                _vm.diagnosticos
                  ? _c(
                      "tbody",
                      _vm._l(_vm.diagnosticos, function(diagnostico) {
                        return _c(
                          "tr",
                          { key: diagnostico.id, staticClass: "text-center" },
                          [
                            _c("td", [_vm._v(_vm._s(diagnostico.id))]),
                            _vm._v(" "),
                            _c("td", [_vm._v(_vm._s(diagnostico.nombre))]),
                            _vm._v(" "),
                            _c("td", [_vm._v(_vm._s(diagnostico.created_at))]),
                            _vm._v(" "),
                            _c("td", [_vm._v(_vm._s(diagnostico.updated_at))]),
                            _vm._v(" "),
                            _c("td", [
                              _c("div", { staticClass: "dropdown" }, [
                                _vm._m(0, true),
                                _vm._v(" "),
                                _c(
                                  "div",
                                  {
                                    staticClass:
                                      "dropdown-menu dropdown-menu-right dropdown-menu-arrow"
                                  },
                                  [
                                    _c(
                                      "a",
                                      {
                                        staticClass: "dropdown-item",
                                        attrs: {
                                          href: "#",
                                          "data-toggle": "modal",
                                          "data-target": "#modal-notification"
                                        },
                                        on: {
                                          click: function($event) {
                                            return _vm.openmodal(diagnostico)
                                          }
                                        }
                                      },
                                      [
                                        _c("i", { staticClass: "fas fa-eye" }),
                                        _vm._v(
                                          "Ver\n                            "
                                        )
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "router-link",
                                      {
                                        staticClass: "dropdown-item",
                                        attrs: {
                                          to: {
                                            name: "diagnostico-editar",
                                            params: { id: diagnostico.id }
                                          }
                                        }
                                      },
                                      [
                                        _c("i", { staticClass: "fas fa-pen" }),
                                        _vm._v(
                                          "Editar\n                            "
                                        )
                                      ]
                                    )
                                  ],
                                  1
                                )
                              ])
                            ])
                          ]
                        )
                      }),
                      0
                    )
                  : _c("tbody", [
                      _c("tr", [
                        _c(
                          "td",
                          {
                            staticClass: "text-center",
                            attrs: { colspan: _vm.numberCols }
                          },
                          [_vm._v("No se ha entoncontrado informacion")]
                        )
                      ])
                    ])
              ],
              1
            )
          ]),
          _vm._v(" "),
          _c(
            "base-pagination",
            _vm._b(
              {
                attrs: { align: "center" },
                on: { "pagination-change-page": _vm.search }
              },
              "base-pagination",
              _vm.pagination,
              false
            )
          ),
          _vm._v(" "),
          _c(
            "base-modal",
            { attrs: { gradient: "teal" } },
            [
              _c(
                "h5",
                {
                  staticClass: "modal-title text-white",
                  attrs: { slot: "header", id: "modal-title-notification" },
                  slot: "header"
                },
                [_vm._v("\n            Informacion del diagnostico\n        ")]
              ),
              _vm._v(" "),
              _c(
                "p",
                {
                  staticClass: "text-white",
                  attrs: { slot: "body" },
                  slot: "body"
                },
                [
                  _vm._v(
                    "\n            Nombre de diagnostico: " +
                      _vm._s(_vm.diagnostico.nombre)
                  ),
                  _c("br"),
                  _vm._v(
                    "\n            Fecha de creacion: " +
                      _vm._s(_vm.diagnostico.created_at) +
                      "\n        "
                  )
                ]
              ),
              _vm._v(" "),
              _c("template", { slot: "footer" }, [
                _c(
                  "button",
                  {
                    staticClass: "btn btn-link ml-auto text-white",
                    attrs: { type: "button", "data-dismiss": "modal" }
                  },
                  [_vm._v("Cerrar")]
                )
              ])
            ],
            2
          )
        ],
        1
      )
    : _vm._e()
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "a",
      {
        staticClass: "btn btn-sm btn-icon-only text-light",
        attrs: {
          href: "#",
          role: "button",
          "data-toggle": "dropdown",
          "aria-haspopup": "true",
          "aria-expanded": "false"
        }
      },
      [_c("i", { staticClass: "fas fa-ellipsis-v" })]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/mantenedores/previsiones/CrearPrevisiones.vue?vue&type=template&id=f0a567ee&scoped=true&":
/*!********************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/mantenedores/previsiones/CrearPrevisiones.vue?vue&type=template&id=f0a567ee&scoped=true& ***!
  \********************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm.$can("previsiones-create")
    ? _c("div", { staticClass: "card col-6 center" }, [
        _c("div", { staticClass: "card-header bg-white border-0" }, [
          _c("div", { staticClass: "row align-items-center" }, [
            _vm._m(0),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "col-4 text-right" },
              [
                _c(
                  "router-link",
                  {
                    staticClass: "btn btn-dark",
                    attrs: { to: { name: "previsiones-list" } }
                  },
                  [_vm._v("Volver a listado")]
                )
              ],
              1
            )
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "card-body" }, [
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-12" }, [
              _c("div", { staticClass: "form-group" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.nombre,
                      expression: "nombre"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: { type: "text", placeholder: "Nombre de Prevision" },
                  domProps: { value: _vm.nombre },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.nombre = $event.target.value
                    }
                  }
                })
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "form-group" }, [
                _c(
                  "button",
                  {
                    staticClass: "btn btn-success",
                    attrs: { disabled: this.nombre.length < 3 },
                    on: { click: _vm.crearPrevision }
                  },
                  [_vm._v("Guardar\n                    ")]
                )
              ])
            ])
          ])
        ])
      ])
    : _c("div", [_vm._v(" No tienes permisos para acceder a esta area")])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-8" }, [
      _c("h3", { staticClass: "mb-0" }, [_vm._v("Registrar Nueva Prevision")])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/mantenedores/previsiones/EditarPrevisiones.vue?vue&type=template&id=6b1f3092&scoped=true&":
/*!*********************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/mantenedores/previsiones/EditarPrevisiones.vue?vue&type=template&id=6b1f3092&scoped=true& ***!
  \*********************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm.$can("previsiones-edit")
    ? _c("div", { staticClass: "card col-6 center" }, [
        _c("div", { staticClass: "card-header bg-white border-0" }, [
          _c("div", { staticClass: "row align-items-center" }, [
            _vm._m(0),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "col-4 text-right" },
              [
                _c(
                  "router-link",
                  {
                    staticClass: "btn btn-dark",
                    attrs: { to: { name: "previsiones-list" } }
                  },
                  [_vm._v("Volver a listado")]
                )
              ],
              1
            )
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "card-body" }, [
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-12" }, [
              _c("div", { staticClass: "form-group" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.nombre,
                      expression: "nombre"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: { type: "text", placeholder: "Nombre de Prevision" },
                  domProps: { value: _vm.nombre },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.nombre = $event.target.value
                    }
                  }
                })
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "form-group" }, [
                _c(
                  "button",
                  {
                    staticClass: "btn btn-success",
                    attrs: { disabled: this.nombre.length < 3 },
                    on: { click: _vm.updatePrevision }
                  },
                  [_vm._v("Guardar\n                    ")]
                )
              ])
            ])
          ])
        ])
      ])
    : _c("div", [_vm._v(" No tienes permisos para acceder a esta zona")])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-8" }, [
      _c("h3", { staticClass: "mb-0" }, [
        _vm._v("Actualizar Nombre de Prevision")
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/mantenedores/previsiones/ListaPrevisiones.vue?vue&type=template&id=3346818d&scoped=true&":
/*!********************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/mantenedores/previsiones/ListaPrevisiones.vue?vue&type=template&id=3346818d&scoped=true& ***!
  \********************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm.$can("previsiones-list")
    ? _c(
        "div",
        { staticClass: "card" },
        [
          _c("div", { staticClass: "card-header" }, [
            _c("div", { staticClass: "row" }, [
              _c("div", { staticClass: "col" }, [
                _c(
                  "select",
                  {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.length,
                        expression: "length"
                      }
                    ],
                    staticClass: "form-control ",
                    staticStyle: { "max-width": "10rem" },
                    on: {
                      change: function($event) {
                        var $$selectedVal = Array.prototype.filter
                          .call($event.target.options, function(o) {
                            return o.selected
                          })
                          .map(function(o) {
                            var val = "_value" in o ? o._value : o.value
                            return val
                          })
                        _vm.length = $event.target.multiple
                          ? $$selectedVal
                          : $$selectedVal[0]
                      }
                    }
                  },
                  [
                    _c("option", { attrs: { value: "10" } }, [_vm._v("10")]),
                    _vm._v(" "),
                    _c("option", { attrs: { value: "25" } }, [_vm._v("25")]),
                    _vm._v(" "),
                    _c("option", { attrs: { value: "50" } }, [_vm._v("50")]),
                    _vm._v(" "),
                    _c("option", { attrs: { value: "100" } }, [_vm._v("100")])
                  ]
                )
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "col align-content-center" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.buscar,
                      expression: "buscar"
                    }
                  ],
                  staticClass: "form-control  ",
                  attrs: { type: "text", placeholder: "Buscar" },
                  domProps: { value: _vm.buscar },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.buscar = $event.target.value
                    }
                  }
                })
              ]),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "col" },
                [
                  _c(
                    "router-link",
                    {
                      staticClass: "btn btn-success float-right",
                      attrs: { to: { name: "prevision-crear" } }
                    },
                    [_vm._v("Nueva Prevision\n                ")]
                  )
                ],
                1
              )
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "card-body" }, [
            _c(
              "table",
              {
                staticClass:
                  "table table-condensed align-items-center table-bordered"
              },
              [
                _c("base-table-head", {
                  staticClass: "thead-light",
                  attrs: {
                    columns: _vm.columns,
                    sortOrders: _vm.sortOrders,
                    sortKey: _vm.sortKey
                  },
                  on: { sortBy: _vm.sortBy }
                }),
                _vm._v(" "),
                _vm.previsions
                  ? _c(
                      "tbody",
                      _vm._l(_vm.previsions, function(prevision) {
                        return _c(
                          "tr",
                          { key: prevision.id, staticClass: "text-center" },
                          [
                            _c("td", [_vm._v(_vm._s(prevision.id))]),
                            _vm._v(" "),
                            _c("td", [_vm._v(_vm._s(prevision.nombre))]),
                            _vm._v(" "),
                            _c("td", [_vm._v(_vm._s(prevision.created_at))]),
                            _vm._v(" "),
                            _c("td", [_vm._v(_vm._s(prevision.updated_at))]),
                            _vm._v(" "),
                            _c("td", [
                              _c("div", { staticClass: "dropdown" }, [
                                _vm._m(0, true),
                                _vm._v(" "),
                                _c(
                                  "div",
                                  {
                                    staticClass:
                                      "dropdown-menu dropdown-menu-right dropdown-menu-arrow"
                                  },
                                  [
                                    _c(
                                      "a",
                                      {
                                        staticClass: "dropdown-item",
                                        attrs: {
                                          href: "#",
                                          "data-toggle": "modal",
                                          "data-target": "#modal-notification"
                                        },
                                        on: {
                                          click: function($event) {
                                            return _vm.openmodal(prevision)
                                          }
                                        }
                                      },
                                      [
                                        _c("i", { staticClass: "fas fa-eye" }),
                                        _vm._v(
                                          "Ver\n                            "
                                        )
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "router-link",
                                      {
                                        staticClass: "dropdown-item",
                                        attrs: {
                                          to: {
                                            name: "prevision-editar",
                                            params: { id: prevision.id }
                                          }
                                        }
                                      },
                                      [
                                        _c("i", { staticClass: "fas fa-pen" }),
                                        _vm._v(
                                          "Editar\n                            "
                                        )
                                      ]
                                    )
                                  ],
                                  1
                                )
                              ])
                            ])
                          ]
                        )
                      }),
                      0
                    )
                  : _c("tbody", [
                      _c("tr", [
                        _c(
                          "td",
                          {
                            staticClass: "text-center",
                            attrs: { colspan: _vm.numberCols }
                          },
                          [_vm._v("No se ha entoncontrado informacion")]
                        )
                      ])
                    ])
              ],
              1
            )
          ]),
          _vm._v(" "),
          _c(
            "base-pagination",
            _vm._b(
              {
                attrs: { align: "center" },
                on: { "pagination-change-page": _vm.search }
              },
              "base-pagination",
              _vm.pagination,
              false
            )
          ),
          _vm._v(" "),
          _c(
            "base-modal",
            { attrs: { gradient: "teal" } },
            [
              _c(
                "h5",
                {
                  staticClass: "modal-title text-white",
                  attrs: { slot: "header", id: "modal-title-notification" },
                  slot: "header"
                },
                [_vm._v("\n            Informacion de la prevision\n        ")]
              ),
              _vm._v(" "),
              _c(
                "p",
                {
                  staticClass: "text-white",
                  attrs: { slot: "body" },
                  slot: "body"
                },
                [
                  _vm._v(
                    "\n            Nombre de prevision: " +
                      _vm._s(_vm.prevision.nombre)
                  ),
                  _c("br"),
                  _vm._v(
                    "\n            Fecha de creacion: " +
                      _vm._s(_vm.prevision.created_at) +
                      "\n        "
                  )
                ]
              ),
              _vm._v(" "),
              _c("template", { slot: "footer" }, [
                _c(
                  "button",
                  {
                    staticClass: "btn btn-link ml-auto text-white",
                    attrs: { type: "button", "data-dismiss": "modal" }
                  },
                  [_vm._v("Cerrar")]
                )
              ])
            ],
            2
          )
        ],
        1
      )
    : _vm._e()
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "a",
      {
        staticClass: "btn btn-sm btn-icon-only text-light",
        attrs: {
          href: "#",
          role: "button",
          "data-toggle": "dropdown",
          "aria-haspopup": "true",
          "aria-expanded": "false"
        }
      },
      [_c("i", { staticClass: "fas fa-ellipsis-v" })]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/mantenedores/procedimientos/CrearProcedimiento.vue?vue&type=template&id=35e40e68&scoped=true&":
/*!*************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/mantenedores/procedimientos/CrearProcedimiento.vue?vue&type=template&id=35e40e68&scoped=true& ***!
  \*************************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm.$can("procedimientos-create")
    ? _c("div", { staticClass: "card col-6 center" }, [
        _c("div", { staticClass: "card-header bg-white border-0" }, [
          _c("div", { staticClass: "row align-items-center" }, [
            _vm._m(0),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "col-4 text-right" },
              [
                _c(
                  "router-link",
                  {
                    staticClass: "btn btn-dark",
                    attrs: { to: { name: "procedimiento-list" } }
                  },
                  [_vm._v("Volver a listado")]
                )
              ],
              1
            )
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "card-body" }, [
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-12" }, [
              _c("div", { staticClass: "form-group" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.nombre,
                      expression: "nombre"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: {
                    type: "text",
                    placeholder: "Nombre de Procedimiento"
                  },
                  domProps: { value: _vm.nombre },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.nombre = $event.target.value
                    }
                  }
                })
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "form-group" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.codigo,
                      expression: "codigo"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: {
                    type: "text",
                    placeholder: "Codigo del Procedimiento",
                    maxlength: "12"
                  },
                  domProps: { value: _vm.codigo },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.codigo = $event.target.value
                    }
                  }
                })
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "form-group" }, [
                _c(
                  "button",
                  {
                    staticClass: "btn btn-success",
                    attrs: { disabled: _vm.disabled },
                    on: { click: _vm.crearDiagnostico }
                  },
                  [_vm._v("Guardar\n                    ")]
                )
              ])
            ])
          ])
        ])
      ])
    : _c("div", [_vm._v(" No tienes permisos para accerder a a esta zona")])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-8" }, [
      _c("h3", { staticClass: "mb-0" }, [
        _vm._v("Registrar Nuevo Procedimiento")
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/mantenedores/procedimientos/EditarProcedimientos.vue?vue&type=template&id=fcfd5d8e&scoped=true&":
/*!***************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/mantenedores/procedimientos/EditarProcedimientos.vue?vue&type=template&id=fcfd5d8e&scoped=true& ***!
  \***************************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm.$can("procedimientos-edit")
    ? _c("div", { staticClass: "card col-6 center" }, [
        _c("div", { staticClass: "card-header bg-white border-0" }, [
          _c("div", { staticClass: "row align-items-center" }, [
            _vm._m(0),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "col-4 text-right" },
              [
                _c(
                  "router-link",
                  {
                    staticClass: "btn btn-dark",
                    attrs: { to: { name: "procedimiento-list" } }
                  },
                  [_vm._v("Volver a listado\n                ")]
                )
              ],
              1
            )
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "card-body" }, [
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-12" }, [
              _c("div", { staticClass: "form-group" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.nombre,
                      expression: "nombre"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: {
                    type: "text",
                    placeholder: "Nombre del Procedimiento"
                  },
                  domProps: { value: _vm.nombre },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.nombre = $event.target.value
                    }
                  }
                })
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "form-group" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.codigo,
                      expression: "codigo"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: {
                    type: "text",
                    placeholder: "Codigo del Procedimiento",
                    maxlength: "12"
                  },
                  domProps: { value: _vm.codigo },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.codigo = $event.target.value
                    }
                  }
                })
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "form-group" }, [
                _c(
                  "button",
                  {
                    staticClass: "btn btn-success",
                    attrs: { disabled: _vm.disabled },
                    on: { click: _vm.updateProcedimiento }
                  },
                  [_vm._v("Guardar\n                    ")]
                )
              ])
            ])
          ])
        ])
      ])
    : _c("div", [_vm._v(" No tienes permisos para accerder a a esta zona")])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-8" }, [
      _c("h3", { staticClass: "mb-0" }, [_vm._v("Actualizar Procedimiento")])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/mantenedores/procedimientos/ListaProcedimientos.vue?vue&type=template&id=4b11245a&scoped=true&":
/*!**************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/mantenedores/procedimientos/ListaProcedimientos.vue?vue&type=template&id=4b11245a&scoped=true& ***!
  \**************************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm.$can("procedimientos-list")
    ? _c(
        "div",
        { staticClass: "card" },
        [
          _c("div", { staticClass: "card-header" }, [
            _c(
              "div",
              { staticClass: "row" },
              [
                _c("div", { staticClass: "col" }, [
                  _c(
                    "select",
                    {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.length,
                          expression: "length"
                        }
                      ],
                      staticClass: "form-control ",
                      staticStyle: { "max-width": "10rem" },
                      on: {
                        change: function($event) {
                          var $$selectedVal = Array.prototype.filter
                            .call($event.target.options, function(o) {
                              return o.selected
                            })
                            .map(function(o) {
                              var val = "_value" in o ? o._value : o.value
                              return val
                            })
                          _vm.length = $event.target.multiple
                            ? $$selectedVal
                            : $$selectedVal[0]
                        }
                      }
                    },
                    [
                      _c("option", { attrs: { value: "10" } }, [_vm._v("10")]),
                      _vm._v(" "),
                      _c("option", { attrs: { value: "25" } }, [_vm._v("25")]),
                      _vm._v(" "),
                      _c("option", { attrs: { value: "50" } }, [_vm._v("50")]),
                      _vm._v(" "),
                      _c("option", { attrs: { value: "100" } }, [_vm._v("100")])
                    ]
                  )
                ]),
                _vm._v(" "),
                _c(
                  "router-link",
                  {
                    staticClass: " rigth btn btn-success",
                    attrs: { to: { name: "procedimiento-crear" } }
                  },
                  [_vm._v("Nuevo Procedimiento")]
                )
              ],
              1
            )
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "card-body" }, [
            _c(
              "table",
              {
                staticClass:
                  "table table-condensed align-items-center table-bordered"
              },
              [
                _c("base-table-head", {
                  staticClass: "thead-light",
                  attrs: { columns: _vm.columns }
                }),
                _vm._v(" "),
                _vm.procedimientos.length > 0
                  ? _c(
                      "tbody",
                      _vm._l(_vm.procedimientos, function(procedimiento) {
                        return _c(
                          "tr",
                          { key: procedimiento.id, staticClass: "text-center" },
                          [
                            _c("td", [_vm._v(_vm._s(procedimiento.id))]),
                            _vm._v(" "),
                            _c("td", [_vm._v(_vm._s(procedimiento.nombre))]),
                            _vm._v(" "),
                            _c("td", [_vm._v(_vm._s(procedimiento.codigo))]),
                            _vm._v(" "),
                            _c("td", [
                              _vm._v(_vm._s(procedimiento.created_at))
                            ]),
                            _vm._v(" "),
                            _c("td", [
                              _vm._v(_vm._s(procedimiento.updated_at))
                            ]),
                            _vm._v(" "),
                            _c("td", [
                              _c("div", { staticClass: "dropdown" }, [
                                _vm._m(0, true),
                                _vm._v(" "),
                                _c(
                                  "div",
                                  {
                                    staticClass:
                                      "dropdown-menu dropdown-menu-right dropdown-menu-arrow"
                                  },
                                  [
                                    _c(
                                      "a",
                                      {
                                        staticClass: "dropdown-item",
                                        attrs: {
                                          href: "#",
                                          "data-toggle": "modal",
                                          "data-target": "#modal-notification"
                                        },
                                        on: {
                                          click: function($event) {
                                            return _vm.openmodal(procedimiento)
                                          }
                                        }
                                      },
                                      [
                                        _c("i", { staticClass: "fas fa-eye" }),
                                        _vm._v(
                                          "Ver\n                            "
                                        )
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "router-link",
                                      {
                                        staticClass: "dropdown-item",
                                        attrs: {
                                          to: {
                                            name: "procedimiento-editar",
                                            params: { id: procedimiento.id }
                                          }
                                        }
                                      },
                                      [
                                        _c("i", { staticClass: "fas fa-pen" }),
                                        _vm._v(
                                          "Editar\n                            "
                                        )
                                      ]
                                    )
                                  ],
                                  1
                                )
                              ])
                            ])
                          ]
                        )
                      }),
                      0
                    )
                  : _c("tbody", [
                      _c("tr", [
                        _c(
                          "td",
                          {
                            staticClass: "text-center",
                            attrs: { colspan: _vm.numberCols }
                          },
                          [_vm._v("No se ha entoncontrado informacion")]
                        )
                      ])
                    ])
              ],
              1
            )
          ]),
          _vm._v(" "),
          _c(
            "base-pagination",
            _vm._b(
              {
                attrs: { align: "center" },
                on: { "pagination-change-page": _vm.search }
              },
              "base-pagination",
              _vm.pagination,
              false
            )
          ),
          _vm._v(" "),
          _c(
            "base-modal",
            { attrs: { gradient: "teal" } },
            [
              _c(
                "h5",
                {
                  staticClass: "modal-title text-white",
                  attrs: { slot: "header", id: "modal-title-notification" },
                  slot: "header"
                },
                [
                  _vm._v(
                    "\n            Informacion del procedimiento\n        "
                  )
                ]
              ),
              _vm._v(" "),
              _c(
                "p",
                {
                  staticClass: "text-white",
                  attrs: { slot: "body" },
                  slot: "body"
                },
                [
                  _vm._v(
                    "\n            Nombre de procedimiento: " +
                      _vm._s(_vm.procedimiento.nombre)
                  ),
                  _c("br"),
                  _vm._v(
                    "\n            Codigo de procedimiento: " +
                      _vm._s(_vm.procedimiento.codigo)
                  ),
                  _c("br"),
                  _vm._v(
                    "\n            Fecha de creacion: " +
                      _vm._s(_vm.procedimiento.created_at) +
                      "\n        "
                  )
                ]
              ),
              _vm._v(" "),
              _c("template", { slot: "footer" }, [
                _c(
                  "button",
                  {
                    staticClass: "btn btn-link ml-auto text-white",
                    attrs: { type: "button", "data-dismiss": "modal" }
                  },
                  [_vm._v("Cerrar")]
                )
              ])
            ],
            2
          )
        ],
        1
      )
    : _c("div", [_vm._v(" No tienes permisos para accerder a a esta zona")])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "a",
      {
        staticClass: "btn btn-sm btn-icon-only text-light",
        attrs: {
          href: "#",
          role: "button",
          "data-toggle": "dropdown",
          "aria-haspopup": "true",
          "aria-expanded": "false"
        }
      },
      [_c("i", { staticClass: "fas fa-ellipsis-v" })]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/mantenedores/regiones/CrearRegion.vue?vue&type=template&id=29ec9fef&scoped=true&":
/*!************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/mantenedores/regiones/CrearRegion.vue?vue&type=template&id=29ec9fef&scoped=true& ***!
  \************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm.$can("regiones-create")
    ? _c("div", { staticClass: "card col-6 center" }, [
        _c("div", { staticClass: "card-header bg-white border-0" }, [
          _c("div", { staticClass: "row align-items-center" }, [
            _vm._m(0),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "col-4 text-right" },
              [
                _c(
                  "router-link",
                  {
                    staticClass: "btn btn-dark",
                    attrs: { to: { name: "region-list" } }
                  },
                  [_vm._v("Volver a listado")]
                )
              ],
              1
            )
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "card-body" }, [
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-12" }, [
              _c("div", { staticClass: "form-group" }, [
                _c("label", [_vm._v("Region: ")]),
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.nombre,
                      expression: "nombre"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: { type: "text", placeholder: "Nombre de Region" },
                  domProps: { value: _vm.nombre },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.nombre = $event.target.value
                    }
                  }
                })
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "form-group" }, [
                _c("label", { staticClass: "label" }, [_vm._v("Posee Staff:")]),
                _vm._v(" "),
                _c("span", { staticClass: "clearfix" }),
                _vm._v(" "),
                _c("label", [_vm._v("NO/SI")]),
                _vm._v(" "),
                _c("span", { staticClass: "clearfix" }),
                _vm._v(" "),
                _c("label", { staticClass: "custom-toggle " }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.staff,
                        expression: "staff"
                      }
                    ],
                    attrs: { type: "checkbox" },
                    domProps: {
                      checked: Array.isArray(_vm.staff)
                        ? _vm._i(_vm.staff, null) > -1
                        : _vm.staff
                    },
                    on: {
                      change: function($event) {
                        var $$a = _vm.staff,
                          $$el = $event.target,
                          $$c = $$el.checked ? true : false
                        if (Array.isArray($$a)) {
                          var $$v = null,
                            $$i = _vm._i($$a, $$v)
                          if ($$el.checked) {
                            $$i < 0 && (_vm.staff = $$a.concat([$$v]))
                          } else {
                            $$i > -1 &&
                              (_vm.staff = $$a
                                .slice(0, $$i)
                                .concat($$a.slice($$i + 1)))
                          }
                        } else {
                          _vm.staff = $$c
                        }
                      }
                    }
                  }),
                  _vm._v(" "),
                  _c("span", {
                    staticClass: "custom-toggle-slider rounded-circle",
                    attrs: { "data-label-off": "No", "data-label-on": "Yes" }
                  })
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "form-group" }, [
                _c(
                  "button",
                  {
                    staticClass: "btn btn-success",
                    attrs: { disabled: _vm.disabled },
                    on: { click: _vm.crearDiagnostico }
                  },
                  [_vm._v("Guardar\n                    ")]
                )
              ])
            ])
          ])
        ])
      ])
    : _vm._e()
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-8" }, [
      _c("h3", { staticClass: "mb-0" }, [_vm._v("Registrar Nueva Region")])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/mantenedores/regiones/EditarRegion.vue?vue&type=template&id=208766aa&scoped=true&":
/*!*************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/mantenedores/regiones/EditarRegion.vue?vue&type=template&id=208766aa&scoped=true& ***!
  \*************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm.$can("regiones-edit")
    ? _c("div", { staticClass: "card col-6 center" }, [
        _c("div", { staticClass: "card-header bg-white border-0" }, [
          _c("div", { staticClass: "row align-items-center" }, [
            _vm._m(0),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "col-4 text-right" },
              [
                _c(
                  "router-link",
                  {
                    staticClass: "btn btn-dark",
                    attrs: { to: { name: "region-list" } }
                  },
                  [_vm._v("Volver a listado\n                ")]
                )
              ],
              1
            )
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "card-body" }, [
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-12" }, [
              _c("div", { staticClass: "form-group" }, [
                _c("label", [_vm._v("Region: ")]),
                _vm._v(" "),
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.nombre,
                      expression: "nombre"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: { type: "text", placeholder: "Nombre de Region" },
                  domProps: { value: _vm.nombre },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.nombre = $event.target.value
                    }
                  }
                })
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "form-group" }, [
                _c("label", { staticClass: "label" }, [_vm._v("Posee Staff:")]),
                _vm._v(" "),
                _c("span", { staticClass: "clearfix" }),
                _vm._v(" "),
                _c("label", [_vm._v("NO/SI")]),
                _vm._v(" "),
                _c("span", { staticClass: "clearfix" }),
                _vm._v(" "),
                _c("label", { staticClass: "custom-toggle " }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.staff,
                        expression: "staff"
                      }
                    ],
                    attrs: { type: "checkbox" },
                    domProps: {
                      checked: Array.isArray(_vm.staff)
                        ? _vm._i(_vm.staff, null) > -1
                        : _vm.staff
                    },
                    on: {
                      change: function($event) {
                        var $$a = _vm.staff,
                          $$el = $event.target,
                          $$c = $$el.checked ? true : false
                        if (Array.isArray($$a)) {
                          var $$v = null,
                            $$i = _vm._i($$a, $$v)
                          if ($$el.checked) {
                            $$i < 0 && (_vm.staff = $$a.concat([$$v]))
                          } else {
                            $$i > -1 &&
                              (_vm.staff = $$a
                                .slice(0, $$i)
                                .concat($$a.slice($$i + 1)))
                          }
                        } else {
                          _vm.staff = $$c
                        }
                      }
                    }
                  }),
                  _vm._v(" "),
                  _c("span", {
                    staticClass: "custom-toggle-slider rounded-circle"
                  })
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "form-group" }, [
                _c(
                  "button",
                  {
                    staticClass: "btn btn-success",
                    attrs: { disabled: _vm.disabled },
                    on: { click: _vm.updateregion }
                  },
                  [_vm._v("Guardar\n                    ")]
                )
              ])
            ])
          ])
        ])
      ])
    : _vm._e()
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-8" }, [
      _c("h3", { staticClass: "mb-0" }, [_vm._v("Actualizar region")])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/mantenedores/regiones/ListaRegiones.vue?vue&type=template&id=7d6f04d9&scoped=true&":
/*!**************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/mantenedores/regiones/ListaRegiones.vue?vue&type=template&id=7d6f04d9&scoped=true& ***!
  \**************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm.$can("regiones-list")
    ? _c(
        "div",
        { staticClass: "card" },
        [
          _c("div", { staticClass: "card-header" }, [
            _c("div", { staticClass: "row" }, [
              _c("div", { staticClass: "col" }, [
                _c(
                  "select",
                  {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.length,
                        expression: "length"
                      }
                    ],
                    staticClass: "form-control ",
                    staticStyle: { "max-width": "10rem" },
                    on: {
                      change: function($event) {
                        var $$selectedVal = Array.prototype.filter
                          .call($event.target.options, function(o) {
                            return o.selected
                          })
                          .map(function(o) {
                            var val = "_value" in o ? o._value : o.value
                            return val
                          })
                        _vm.length = $event.target.multiple
                          ? $$selectedVal
                          : $$selectedVal[0]
                      }
                    }
                  },
                  [
                    _c("option", { attrs: { value: "10" } }, [_vm._v("10")]),
                    _vm._v(" "),
                    _c("option", { attrs: { value: "25" } }, [_vm._v("25")]),
                    _vm._v(" "),
                    _c("option", { attrs: { value: "50" } }, [_vm._v("50")]),
                    _vm._v(" "),
                    _c("option", { attrs: { value: "100" } }, [_vm._v("100")])
                  ]
                )
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "col align-content-center" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.buscar,
                      expression: "buscar"
                    }
                  ],
                  staticClass: "form-control  ",
                  attrs: { type: "text", placeholder: "Buscar" },
                  domProps: { value: _vm.buscar },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.buscar = $event.target.value
                    }
                  }
                })
              ]),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "col" },
                [
                  _c(
                    "router-link",
                    {
                      staticClass: "btn btn-success float-right",
                      attrs: { to: { name: "region-crear" } }
                    },
                    [_vm._v("Nueva Region\n                ")]
                  )
                ],
                1
              )
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "card-body" }, [
            _c(
              "table",
              {
                staticClass:
                  "table table-condensed align-items-center table-bordered"
              },
              [
                _c("base-table-head", {
                  staticClass: "thead-light",
                  attrs: {
                    columns: _vm.columns,
                    sortOrders: _vm.sortOrders,
                    sortKey: _vm.sortKey
                  },
                  on: { sortBy: _vm.sortBy }
                }),
                _vm._v(" "),
                _vm.regions
                  ? _c(
                      "tbody",
                      _vm._l(_vm.regions, function(region) {
                        return _c(
                          "tr",
                          { key: region.id, staticClass: "text-center" },
                          [
                            _c("td", [_vm._v(_vm._s(region.id))]),
                            _vm._v(" "),
                            _c("td", [_vm._v(_vm._s(region.nombre))]),
                            _vm._v(" "),
                            _c("td", [
                              _vm._v(_vm._s(region.staff == true ? "SI" : "NO"))
                            ]),
                            _vm._v(" "),
                            _c("td", [_vm._v(_vm._s(region.created_at))]),
                            _vm._v(" "),
                            _c("td", [_vm._v(_vm._s(region.updated_at))]),
                            _vm._v(" "),
                            _c("td", [
                              _c("div", { staticClass: "dropdown" }, [
                                _vm._m(0, true),
                                _vm._v(" "),
                                _c(
                                  "div",
                                  {
                                    staticClass:
                                      "dropdown-menu dropdown-menu-right dropdown-menu-arrow"
                                  },
                                  [
                                    _c(
                                      "a",
                                      {
                                        staticClass: "dropdown-item",
                                        attrs: {
                                          href: "#",
                                          "data-toggle": "modal",
                                          "data-target": "#modal-notification"
                                        },
                                        on: {
                                          click: function($event) {
                                            return _vm.openmodal(region)
                                          }
                                        }
                                      },
                                      [
                                        _c("i", { staticClass: "fas fa-eye" }),
                                        _vm._v(
                                          "Ver\n                            "
                                        )
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "router-link",
                                      {
                                        staticClass: "dropdown-item",
                                        attrs: {
                                          to: {
                                            name: "region-editar",
                                            params: { id: region.id }
                                          }
                                        }
                                      },
                                      [
                                        _c("i", { staticClass: "fas fa-pen" }),
                                        _vm._v(
                                          "Editar\n                            "
                                        )
                                      ]
                                    )
                                  ],
                                  1
                                )
                              ])
                            ])
                          ]
                        )
                      }),
                      0
                    )
                  : _c("tbody", [
                      _c("tr", [
                        _c(
                          "td",
                          {
                            staticClass: "text-center",
                            attrs: { colspan: _vm.numberCols }
                          },
                          [_vm._v("No se ha entoncontrado informacion")]
                        )
                      ])
                    ])
              ],
              1
            )
          ]),
          _vm._v(" "),
          _c(
            "base-pagination",
            _vm._b(
              {
                attrs: { align: "center" },
                on: { "pagination-change-page": _vm.search }
              },
              "base-pagination",
              _vm.pagination,
              false
            )
          ),
          _vm._v(" "),
          _c(
            "base-modal",
            { attrs: { gradient: "teal" } },
            [
              _c(
                "h5",
                {
                  staticClass: "modal-title text-white",
                  attrs: { slot: "header", id: "modal-title-notification" },
                  slot: "header"
                },
                [_vm._v("\n            Informacion de la region\n        ")]
              ),
              _vm._v(" "),
              _c(
                "p",
                {
                  staticClass: "text-white",
                  attrs: { slot: "body" },
                  slot: "body"
                },
                [
                  _vm._v(
                    "\n            Nombre de region: " +
                      _vm._s(_vm.region.nombre)
                  ),
                  _c("br"),
                  _vm._v(
                    "\n            Posee staf: " +
                      _vm._s(_vm.region.staff === true ? "SI" : "NO")
                  ),
                  _c("br"),
                  _vm._v(
                    "\n            Fecha de creacion: " +
                      _vm._s(_vm.region.created_at) +
                      "\n        "
                  )
                ]
              ),
              _vm._v(" "),
              _c("template", { slot: "footer" }, [
                _c(
                  "button",
                  {
                    staticClass: "btn btn-link ml-auto text-white",
                    attrs: { type: "button", "data-dismiss": "modal" }
                  },
                  [_vm._v("Cerrar")]
                )
              ])
            ],
            2
          )
        ],
        1
      )
    : _vm._e()
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "a",
      {
        staticClass: "btn btn-sm btn-icon-only text-light",
        attrs: {
          href: "#",
          role: "button",
          "data-toggle": "dropdown",
          "aria-haspopup": "true",
          "aria-expanded": "false"
        }
      },
      [_c("i", { staticClass: "fas fa-ellipsis-v" })]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/mantenedores/MantenedoresMain.vue":
/*!*******************************************************************!*\
  !*** ./resources/js/components/mantenedores/MantenedoresMain.vue ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _MantenedoresMain_vue_vue_type_template_id_40be0e66_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./MantenedoresMain.vue?vue&type=template&id=40be0e66&scoped=true& */ "./resources/js/components/mantenedores/MantenedoresMain.vue?vue&type=template&id=40be0e66&scoped=true&");
/* harmony import */ var _MantenedoresMain_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./MantenedoresMain.vue?vue&type=script&lang=js& */ "./resources/js/components/mantenedores/MantenedoresMain.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _MantenedoresMain_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _MantenedoresMain_vue_vue_type_template_id_40be0e66_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _MantenedoresMain_vue_vue_type_template_id_40be0e66_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "40be0e66",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/mantenedores/MantenedoresMain.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/mantenedores/MantenedoresMain.vue?vue&type=script&lang=js&":
/*!********************************************************************************************!*\
  !*** ./resources/js/components/mantenedores/MantenedoresMain.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_MantenedoresMain_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./MantenedoresMain.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/mantenedores/MantenedoresMain.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_MantenedoresMain_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/mantenedores/MantenedoresMain.vue?vue&type=template&id=40be0e66&scoped=true&":
/*!**************************************************************************************************************!*\
  !*** ./resources/js/components/mantenedores/MantenedoresMain.vue?vue&type=template&id=40be0e66&scoped=true& ***!
  \**************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MantenedoresMain_vue_vue_type_template_id_40be0e66_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./MantenedoresMain.vue?vue&type=template&id=40be0e66&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/mantenedores/MantenedoresMain.vue?vue&type=template&id=40be0e66&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MantenedoresMain_vue_vue_type_template_id_40be0e66_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MantenedoresMain_vue_vue_type_template_id_40be0e66_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/mantenedores/bonos/CrearBonos.vue":
/*!*******************************************************************!*\
  !*** ./resources/js/components/mantenedores/bonos/CrearBonos.vue ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _CrearBonos_vue_vue_type_template_id_2d35cd49_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./CrearBonos.vue?vue&type=template&id=2d35cd49&scoped=true& */ "./resources/js/components/mantenedores/bonos/CrearBonos.vue?vue&type=template&id=2d35cd49&scoped=true&");
/* harmony import */ var _CrearBonos_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./CrearBonos.vue?vue&type=script&lang=js& */ "./resources/js/components/mantenedores/bonos/CrearBonos.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _CrearBonos_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _CrearBonos_vue_vue_type_template_id_2d35cd49_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _CrearBonos_vue_vue_type_template_id_2d35cd49_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "2d35cd49",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/mantenedores/bonos/CrearBonos.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/mantenedores/bonos/CrearBonos.vue?vue&type=script&lang=js&":
/*!********************************************************************************************!*\
  !*** ./resources/js/components/mantenedores/bonos/CrearBonos.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CrearBonos_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./CrearBonos.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/mantenedores/bonos/CrearBonos.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CrearBonos_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/mantenedores/bonos/CrearBonos.vue?vue&type=template&id=2d35cd49&scoped=true&":
/*!**************************************************************************************************************!*\
  !*** ./resources/js/components/mantenedores/bonos/CrearBonos.vue?vue&type=template&id=2d35cd49&scoped=true& ***!
  \**************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CrearBonos_vue_vue_type_template_id_2d35cd49_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./CrearBonos.vue?vue&type=template&id=2d35cd49&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/mantenedores/bonos/CrearBonos.vue?vue&type=template&id=2d35cd49&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CrearBonos_vue_vue_type_template_id_2d35cd49_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CrearBonos_vue_vue_type_template_id_2d35cd49_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/mantenedores/bonos/EditarBonos.vue":
/*!********************************************************************!*\
  !*** ./resources/js/components/mantenedores/bonos/EditarBonos.vue ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _EditarBonos_vue_vue_type_template_id_29b60dcf_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./EditarBonos.vue?vue&type=template&id=29b60dcf&scoped=true& */ "./resources/js/components/mantenedores/bonos/EditarBonos.vue?vue&type=template&id=29b60dcf&scoped=true&");
/* harmony import */ var _EditarBonos_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./EditarBonos.vue?vue&type=script&lang=js& */ "./resources/js/components/mantenedores/bonos/EditarBonos.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _EditarBonos_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _EditarBonos_vue_vue_type_template_id_29b60dcf_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _EditarBonos_vue_vue_type_template_id_29b60dcf_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "29b60dcf",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/mantenedores/bonos/EditarBonos.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/mantenedores/bonos/EditarBonos.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************!*\
  !*** ./resources/js/components/mantenedores/bonos/EditarBonos.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_EditarBonos_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./EditarBonos.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/mantenedores/bonos/EditarBonos.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_EditarBonos_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/mantenedores/bonos/EditarBonos.vue?vue&type=template&id=29b60dcf&scoped=true&":
/*!***************************************************************************************************************!*\
  !*** ./resources/js/components/mantenedores/bonos/EditarBonos.vue?vue&type=template&id=29b60dcf&scoped=true& ***!
  \***************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EditarBonos_vue_vue_type_template_id_29b60dcf_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./EditarBonos.vue?vue&type=template&id=29b60dcf&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/mantenedores/bonos/EditarBonos.vue?vue&type=template&id=29b60dcf&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EditarBonos_vue_vue_type_template_id_29b60dcf_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EditarBonos_vue_vue_type_template_id_29b60dcf_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/mantenedores/bonos/ListaBonos.vue":
/*!*******************************************************************!*\
  !*** ./resources/js/components/mantenedores/bonos/ListaBonos.vue ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ListaBonos_vue_vue_type_template_id_09931466_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ListaBonos.vue?vue&type=template&id=09931466&scoped=true& */ "./resources/js/components/mantenedores/bonos/ListaBonos.vue?vue&type=template&id=09931466&scoped=true&");
/* harmony import */ var _ListaBonos_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ListaBonos.vue?vue&type=script&lang=js& */ "./resources/js/components/mantenedores/bonos/ListaBonos.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ListaBonos_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ListaBonos_vue_vue_type_template_id_09931466_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ListaBonos_vue_vue_type_template_id_09931466_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "09931466",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/mantenedores/bonos/ListaBonos.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/mantenedores/bonos/ListaBonos.vue?vue&type=script&lang=js&":
/*!********************************************************************************************!*\
  !*** ./resources/js/components/mantenedores/bonos/ListaBonos.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ListaBonos_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ListaBonos.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/mantenedores/bonos/ListaBonos.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ListaBonos_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/mantenedores/bonos/ListaBonos.vue?vue&type=template&id=09931466&scoped=true&":
/*!**************************************************************************************************************!*\
  !*** ./resources/js/components/mantenedores/bonos/ListaBonos.vue?vue&type=template&id=09931466&scoped=true& ***!
  \**************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ListaBonos_vue_vue_type_template_id_09931466_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ListaBonos.vue?vue&type=template&id=09931466&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/mantenedores/bonos/ListaBonos.vue?vue&type=template&id=09931466&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ListaBonos_vue_vue_type_template_id_09931466_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ListaBonos_vue_vue_type_template_id_09931466_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/mantenedores/diagnosticos/CrearDiagnostico.vue":
/*!********************************************************************************!*\
  !*** ./resources/js/components/mantenedores/diagnosticos/CrearDiagnostico.vue ***!
  \********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _CrearDiagnostico_vue_vue_type_template_id_6287a8b4_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./CrearDiagnostico.vue?vue&type=template&id=6287a8b4&scoped=true& */ "./resources/js/components/mantenedores/diagnosticos/CrearDiagnostico.vue?vue&type=template&id=6287a8b4&scoped=true&");
/* harmony import */ var _CrearDiagnostico_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./CrearDiagnostico.vue?vue&type=script&lang=js& */ "./resources/js/components/mantenedores/diagnosticos/CrearDiagnostico.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _CrearDiagnostico_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _CrearDiagnostico_vue_vue_type_template_id_6287a8b4_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _CrearDiagnostico_vue_vue_type_template_id_6287a8b4_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "6287a8b4",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/mantenedores/diagnosticos/CrearDiagnostico.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/mantenedores/diagnosticos/CrearDiagnostico.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************!*\
  !*** ./resources/js/components/mantenedores/diagnosticos/CrearDiagnostico.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CrearDiagnostico_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./CrearDiagnostico.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/mantenedores/diagnosticos/CrearDiagnostico.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CrearDiagnostico_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/mantenedores/diagnosticos/CrearDiagnostico.vue?vue&type=template&id=6287a8b4&scoped=true&":
/*!***************************************************************************************************************************!*\
  !*** ./resources/js/components/mantenedores/diagnosticos/CrearDiagnostico.vue?vue&type=template&id=6287a8b4&scoped=true& ***!
  \***************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CrearDiagnostico_vue_vue_type_template_id_6287a8b4_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./CrearDiagnostico.vue?vue&type=template&id=6287a8b4&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/mantenedores/diagnosticos/CrearDiagnostico.vue?vue&type=template&id=6287a8b4&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CrearDiagnostico_vue_vue_type_template_id_6287a8b4_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CrearDiagnostico_vue_vue_type_template_id_6287a8b4_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/mantenedores/diagnosticos/EditarDiagnostico.vue":
/*!*********************************************************************************!*\
  !*** ./resources/js/components/mantenedores/diagnosticos/EditarDiagnostico.vue ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _EditarDiagnostico_vue_vue_type_template_id_4537da6a_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./EditarDiagnostico.vue?vue&type=template&id=4537da6a&scoped=true& */ "./resources/js/components/mantenedores/diagnosticos/EditarDiagnostico.vue?vue&type=template&id=4537da6a&scoped=true&");
/* harmony import */ var _EditarDiagnostico_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./EditarDiagnostico.vue?vue&type=script&lang=js& */ "./resources/js/components/mantenedores/diagnosticos/EditarDiagnostico.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _EditarDiagnostico_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _EditarDiagnostico_vue_vue_type_template_id_4537da6a_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _EditarDiagnostico_vue_vue_type_template_id_4537da6a_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "4537da6a",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/mantenedores/diagnosticos/EditarDiagnostico.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/mantenedores/diagnosticos/EditarDiagnostico.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************!*\
  !*** ./resources/js/components/mantenedores/diagnosticos/EditarDiagnostico.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_EditarDiagnostico_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./EditarDiagnostico.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/mantenedores/diagnosticos/EditarDiagnostico.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_EditarDiagnostico_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/mantenedores/diagnosticos/EditarDiagnostico.vue?vue&type=template&id=4537da6a&scoped=true&":
/*!****************************************************************************************************************************!*\
  !*** ./resources/js/components/mantenedores/diagnosticos/EditarDiagnostico.vue?vue&type=template&id=4537da6a&scoped=true& ***!
  \****************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EditarDiagnostico_vue_vue_type_template_id_4537da6a_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./EditarDiagnostico.vue?vue&type=template&id=4537da6a&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/mantenedores/diagnosticos/EditarDiagnostico.vue?vue&type=template&id=4537da6a&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EditarDiagnostico_vue_vue_type_template_id_4537da6a_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EditarDiagnostico_vue_vue_type_template_id_4537da6a_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/mantenedores/diagnosticos/ListaDiagnosticos.vue":
/*!*********************************************************************************!*\
  !*** ./resources/js/components/mantenedores/diagnosticos/ListaDiagnosticos.vue ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ListaDiagnosticos_vue_vue_type_template_id_8cb2ec2a_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ListaDiagnosticos.vue?vue&type=template&id=8cb2ec2a&scoped=true& */ "./resources/js/components/mantenedores/diagnosticos/ListaDiagnosticos.vue?vue&type=template&id=8cb2ec2a&scoped=true&");
/* harmony import */ var _ListaDiagnosticos_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ListaDiagnosticos.vue?vue&type=script&lang=js& */ "./resources/js/components/mantenedores/diagnosticos/ListaDiagnosticos.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ListaDiagnosticos_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ListaDiagnosticos_vue_vue_type_template_id_8cb2ec2a_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ListaDiagnosticos_vue_vue_type_template_id_8cb2ec2a_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "8cb2ec2a",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/mantenedores/diagnosticos/ListaDiagnosticos.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/mantenedores/diagnosticos/ListaDiagnosticos.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************!*\
  !*** ./resources/js/components/mantenedores/diagnosticos/ListaDiagnosticos.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ListaDiagnosticos_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ListaDiagnosticos.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/mantenedores/diagnosticos/ListaDiagnosticos.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ListaDiagnosticos_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/mantenedores/diagnosticos/ListaDiagnosticos.vue?vue&type=template&id=8cb2ec2a&scoped=true&":
/*!****************************************************************************************************************************!*\
  !*** ./resources/js/components/mantenedores/diagnosticos/ListaDiagnosticos.vue?vue&type=template&id=8cb2ec2a&scoped=true& ***!
  \****************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ListaDiagnosticos_vue_vue_type_template_id_8cb2ec2a_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ListaDiagnosticos.vue?vue&type=template&id=8cb2ec2a&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/mantenedores/diagnosticos/ListaDiagnosticos.vue?vue&type=template&id=8cb2ec2a&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ListaDiagnosticos_vue_vue_type_template_id_8cb2ec2a_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ListaDiagnosticos_vue_vue_type_template_id_8cb2ec2a_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/mantenedores/previsiones/CrearPrevisiones.vue":
/*!*******************************************************************************!*\
  !*** ./resources/js/components/mantenedores/previsiones/CrearPrevisiones.vue ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _CrearPrevisiones_vue_vue_type_template_id_f0a567ee_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./CrearPrevisiones.vue?vue&type=template&id=f0a567ee&scoped=true& */ "./resources/js/components/mantenedores/previsiones/CrearPrevisiones.vue?vue&type=template&id=f0a567ee&scoped=true&");
/* harmony import */ var _CrearPrevisiones_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./CrearPrevisiones.vue?vue&type=script&lang=js& */ "./resources/js/components/mantenedores/previsiones/CrearPrevisiones.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _CrearPrevisiones_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _CrearPrevisiones_vue_vue_type_template_id_f0a567ee_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _CrearPrevisiones_vue_vue_type_template_id_f0a567ee_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "f0a567ee",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/mantenedores/previsiones/CrearPrevisiones.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/mantenedores/previsiones/CrearPrevisiones.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************!*\
  !*** ./resources/js/components/mantenedores/previsiones/CrearPrevisiones.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CrearPrevisiones_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./CrearPrevisiones.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/mantenedores/previsiones/CrearPrevisiones.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CrearPrevisiones_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/mantenedores/previsiones/CrearPrevisiones.vue?vue&type=template&id=f0a567ee&scoped=true&":
/*!**************************************************************************************************************************!*\
  !*** ./resources/js/components/mantenedores/previsiones/CrearPrevisiones.vue?vue&type=template&id=f0a567ee&scoped=true& ***!
  \**************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CrearPrevisiones_vue_vue_type_template_id_f0a567ee_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./CrearPrevisiones.vue?vue&type=template&id=f0a567ee&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/mantenedores/previsiones/CrearPrevisiones.vue?vue&type=template&id=f0a567ee&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CrearPrevisiones_vue_vue_type_template_id_f0a567ee_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CrearPrevisiones_vue_vue_type_template_id_f0a567ee_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/mantenedores/previsiones/EditarPrevisiones.vue":
/*!********************************************************************************!*\
  !*** ./resources/js/components/mantenedores/previsiones/EditarPrevisiones.vue ***!
  \********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _EditarPrevisiones_vue_vue_type_template_id_6b1f3092_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./EditarPrevisiones.vue?vue&type=template&id=6b1f3092&scoped=true& */ "./resources/js/components/mantenedores/previsiones/EditarPrevisiones.vue?vue&type=template&id=6b1f3092&scoped=true&");
/* harmony import */ var _EditarPrevisiones_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./EditarPrevisiones.vue?vue&type=script&lang=js& */ "./resources/js/components/mantenedores/previsiones/EditarPrevisiones.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _EditarPrevisiones_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _EditarPrevisiones_vue_vue_type_template_id_6b1f3092_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _EditarPrevisiones_vue_vue_type_template_id_6b1f3092_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "6b1f3092",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/mantenedores/previsiones/EditarPrevisiones.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/mantenedores/previsiones/EditarPrevisiones.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************!*\
  !*** ./resources/js/components/mantenedores/previsiones/EditarPrevisiones.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_EditarPrevisiones_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./EditarPrevisiones.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/mantenedores/previsiones/EditarPrevisiones.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_EditarPrevisiones_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/mantenedores/previsiones/EditarPrevisiones.vue?vue&type=template&id=6b1f3092&scoped=true&":
/*!***************************************************************************************************************************!*\
  !*** ./resources/js/components/mantenedores/previsiones/EditarPrevisiones.vue?vue&type=template&id=6b1f3092&scoped=true& ***!
  \***************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EditarPrevisiones_vue_vue_type_template_id_6b1f3092_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./EditarPrevisiones.vue?vue&type=template&id=6b1f3092&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/mantenedores/previsiones/EditarPrevisiones.vue?vue&type=template&id=6b1f3092&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EditarPrevisiones_vue_vue_type_template_id_6b1f3092_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EditarPrevisiones_vue_vue_type_template_id_6b1f3092_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/mantenedores/previsiones/ListaPrevisiones.vue":
/*!*******************************************************************************!*\
  !*** ./resources/js/components/mantenedores/previsiones/ListaPrevisiones.vue ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ListaPrevisiones_vue_vue_type_template_id_3346818d_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ListaPrevisiones.vue?vue&type=template&id=3346818d&scoped=true& */ "./resources/js/components/mantenedores/previsiones/ListaPrevisiones.vue?vue&type=template&id=3346818d&scoped=true&");
/* harmony import */ var _ListaPrevisiones_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ListaPrevisiones.vue?vue&type=script&lang=js& */ "./resources/js/components/mantenedores/previsiones/ListaPrevisiones.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ListaPrevisiones_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ListaPrevisiones_vue_vue_type_template_id_3346818d_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ListaPrevisiones_vue_vue_type_template_id_3346818d_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "3346818d",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/mantenedores/previsiones/ListaPrevisiones.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/mantenedores/previsiones/ListaPrevisiones.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************!*\
  !*** ./resources/js/components/mantenedores/previsiones/ListaPrevisiones.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ListaPrevisiones_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ListaPrevisiones.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/mantenedores/previsiones/ListaPrevisiones.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ListaPrevisiones_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/mantenedores/previsiones/ListaPrevisiones.vue?vue&type=template&id=3346818d&scoped=true&":
/*!**************************************************************************************************************************!*\
  !*** ./resources/js/components/mantenedores/previsiones/ListaPrevisiones.vue?vue&type=template&id=3346818d&scoped=true& ***!
  \**************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ListaPrevisiones_vue_vue_type_template_id_3346818d_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ListaPrevisiones.vue?vue&type=template&id=3346818d&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/mantenedores/previsiones/ListaPrevisiones.vue?vue&type=template&id=3346818d&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ListaPrevisiones_vue_vue_type_template_id_3346818d_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ListaPrevisiones_vue_vue_type_template_id_3346818d_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/mantenedores/procedimientos/CrearProcedimiento.vue":
/*!************************************************************************************!*\
  !*** ./resources/js/components/mantenedores/procedimientos/CrearProcedimiento.vue ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _CrearProcedimiento_vue_vue_type_template_id_35e40e68_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./CrearProcedimiento.vue?vue&type=template&id=35e40e68&scoped=true& */ "./resources/js/components/mantenedores/procedimientos/CrearProcedimiento.vue?vue&type=template&id=35e40e68&scoped=true&");
/* harmony import */ var _CrearProcedimiento_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./CrearProcedimiento.vue?vue&type=script&lang=js& */ "./resources/js/components/mantenedores/procedimientos/CrearProcedimiento.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _CrearProcedimiento_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _CrearProcedimiento_vue_vue_type_template_id_35e40e68_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _CrearProcedimiento_vue_vue_type_template_id_35e40e68_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "35e40e68",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/mantenedores/procedimientos/CrearProcedimiento.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/mantenedores/procedimientos/CrearProcedimiento.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************!*\
  !*** ./resources/js/components/mantenedores/procedimientos/CrearProcedimiento.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CrearProcedimiento_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./CrearProcedimiento.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/mantenedores/procedimientos/CrearProcedimiento.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CrearProcedimiento_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/mantenedores/procedimientos/CrearProcedimiento.vue?vue&type=template&id=35e40e68&scoped=true&":
/*!*******************************************************************************************************************************!*\
  !*** ./resources/js/components/mantenedores/procedimientos/CrearProcedimiento.vue?vue&type=template&id=35e40e68&scoped=true& ***!
  \*******************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CrearProcedimiento_vue_vue_type_template_id_35e40e68_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./CrearProcedimiento.vue?vue&type=template&id=35e40e68&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/mantenedores/procedimientos/CrearProcedimiento.vue?vue&type=template&id=35e40e68&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CrearProcedimiento_vue_vue_type_template_id_35e40e68_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CrearProcedimiento_vue_vue_type_template_id_35e40e68_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/mantenedores/procedimientos/EditarProcedimientos.vue":
/*!**************************************************************************************!*\
  !*** ./resources/js/components/mantenedores/procedimientos/EditarProcedimientos.vue ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _EditarProcedimientos_vue_vue_type_template_id_fcfd5d8e_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./EditarProcedimientos.vue?vue&type=template&id=fcfd5d8e&scoped=true& */ "./resources/js/components/mantenedores/procedimientos/EditarProcedimientos.vue?vue&type=template&id=fcfd5d8e&scoped=true&");
/* harmony import */ var _EditarProcedimientos_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./EditarProcedimientos.vue?vue&type=script&lang=js& */ "./resources/js/components/mantenedores/procedimientos/EditarProcedimientos.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _EditarProcedimientos_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _EditarProcedimientos_vue_vue_type_template_id_fcfd5d8e_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _EditarProcedimientos_vue_vue_type_template_id_fcfd5d8e_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "fcfd5d8e",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/mantenedores/procedimientos/EditarProcedimientos.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/mantenedores/procedimientos/EditarProcedimientos.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************!*\
  !*** ./resources/js/components/mantenedores/procedimientos/EditarProcedimientos.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_EditarProcedimientos_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./EditarProcedimientos.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/mantenedores/procedimientos/EditarProcedimientos.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_EditarProcedimientos_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/mantenedores/procedimientos/EditarProcedimientos.vue?vue&type=template&id=fcfd5d8e&scoped=true&":
/*!*********************************************************************************************************************************!*\
  !*** ./resources/js/components/mantenedores/procedimientos/EditarProcedimientos.vue?vue&type=template&id=fcfd5d8e&scoped=true& ***!
  \*********************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EditarProcedimientos_vue_vue_type_template_id_fcfd5d8e_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./EditarProcedimientos.vue?vue&type=template&id=fcfd5d8e&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/mantenedores/procedimientos/EditarProcedimientos.vue?vue&type=template&id=fcfd5d8e&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EditarProcedimientos_vue_vue_type_template_id_fcfd5d8e_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EditarProcedimientos_vue_vue_type_template_id_fcfd5d8e_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/mantenedores/procedimientos/ListaProcedimientos.vue":
/*!*************************************************************************************!*\
  !*** ./resources/js/components/mantenedores/procedimientos/ListaProcedimientos.vue ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ListaProcedimientos_vue_vue_type_template_id_4b11245a_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ListaProcedimientos.vue?vue&type=template&id=4b11245a&scoped=true& */ "./resources/js/components/mantenedores/procedimientos/ListaProcedimientos.vue?vue&type=template&id=4b11245a&scoped=true&");
/* harmony import */ var _ListaProcedimientos_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ListaProcedimientos.vue?vue&type=script&lang=js& */ "./resources/js/components/mantenedores/procedimientos/ListaProcedimientos.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ListaProcedimientos_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ListaProcedimientos_vue_vue_type_template_id_4b11245a_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ListaProcedimientos_vue_vue_type_template_id_4b11245a_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "4b11245a",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/mantenedores/procedimientos/ListaProcedimientos.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/mantenedores/procedimientos/ListaProcedimientos.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************!*\
  !*** ./resources/js/components/mantenedores/procedimientos/ListaProcedimientos.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ListaProcedimientos_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ListaProcedimientos.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/mantenedores/procedimientos/ListaProcedimientos.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ListaProcedimientos_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/mantenedores/procedimientos/ListaProcedimientos.vue?vue&type=template&id=4b11245a&scoped=true&":
/*!********************************************************************************************************************************!*\
  !*** ./resources/js/components/mantenedores/procedimientos/ListaProcedimientos.vue?vue&type=template&id=4b11245a&scoped=true& ***!
  \********************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ListaProcedimientos_vue_vue_type_template_id_4b11245a_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ListaProcedimientos.vue?vue&type=template&id=4b11245a&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/mantenedores/procedimientos/ListaProcedimientos.vue?vue&type=template&id=4b11245a&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ListaProcedimientos_vue_vue_type_template_id_4b11245a_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ListaProcedimientos_vue_vue_type_template_id_4b11245a_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/mantenedores/regiones/CrearRegion.vue":
/*!***********************************************************************!*\
  !*** ./resources/js/components/mantenedores/regiones/CrearRegion.vue ***!
  \***********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _CrearRegion_vue_vue_type_template_id_29ec9fef_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./CrearRegion.vue?vue&type=template&id=29ec9fef&scoped=true& */ "./resources/js/components/mantenedores/regiones/CrearRegion.vue?vue&type=template&id=29ec9fef&scoped=true&");
/* harmony import */ var _CrearRegion_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./CrearRegion.vue?vue&type=script&lang=js& */ "./resources/js/components/mantenedores/regiones/CrearRegion.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _CrearRegion_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _CrearRegion_vue_vue_type_template_id_29ec9fef_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _CrearRegion_vue_vue_type_template_id_29ec9fef_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "29ec9fef",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/mantenedores/regiones/CrearRegion.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/mantenedores/regiones/CrearRegion.vue?vue&type=script&lang=js&":
/*!************************************************************************************************!*\
  !*** ./resources/js/components/mantenedores/regiones/CrearRegion.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CrearRegion_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./CrearRegion.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/mantenedores/regiones/CrearRegion.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CrearRegion_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/mantenedores/regiones/CrearRegion.vue?vue&type=template&id=29ec9fef&scoped=true&":
/*!******************************************************************************************************************!*\
  !*** ./resources/js/components/mantenedores/regiones/CrearRegion.vue?vue&type=template&id=29ec9fef&scoped=true& ***!
  \******************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CrearRegion_vue_vue_type_template_id_29ec9fef_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./CrearRegion.vue?vue&type=template&id=29ec9fef&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/mantenedores/regiones/CrearRegion.vue?vue&type=template&id=29ec9fef&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CrearRegion_vue_vue_type_template_id_29ec9fef_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CrearRegion_vue_vue_type_template_id_29ec9fef_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/mantenedores/regiones/EditarRegion.vue":
/*!************************************************************************!*\
  !*** ./resources/js/components/mantenedores/regiones/EditarRegion.vue ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _EditarRegion_vue_vue_type_template_id_208766aa_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./EditarRegion.vue?vue&type=template&id=208766aa&scoped=true& */ "./resources/js/components/mantenedores/regiones/EditarRegion.vue?vue&type=template&id=208766aa&scoped=true&");
/* harmony import */ var _EditarRegion_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./EditarRegion.vue?vue&type=script&lang=js& */ "./resources/js/components/mantenedores/regiones/EditarRegion.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _EditarRegion_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _EditarRegion_vue_vue_type_template_id_208766aa_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _EditarRegion_vue_vue_type_template_id_208766aa_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "208766aa",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/mantenedores/regiones/EditarRegion.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/mantenedores/regiones/EditarRegion.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************!*\
  !*** ./resources/js/components/mantenedores/regiones/EditarRegion.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_EditarRegion_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./EditarRegion.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/mantenedores/regiones/EditarRegion.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_EditarRegion_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/mantenedores/regiones/EditarRegion.vue?vue&type=template&id=208766aa&scoped=true&":
/*!*******************************************************************************************************************!*\
  !*** ./resources/js/components/mantenedores/regiones/EditarRegion.vue?vue&type=template&id=208766aa&scoped=true& ***!
  \*******************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EditarRegion_vue_vue_type_template_id_208766aa_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./EditarRegion.vue?vue&type=template&id=208766aa&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/mantenedores/regiones/EditarRegion.vue?vue&type=template&id=208766aa&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EditarRegion_vue_vue_type_template_id_208766aa_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EditarRegion_vue_vue_type_template_id_208766aa_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/mantenedores/regiones/ListaRegiones.vue":
/*!*************************************************************************!*\
  !*** ./resources/js/components/mantenedores/regiones/ListaRegiones.vue ***!
  \*************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ListaRegiones_vue_vue_type_template_id_7d6f04d9_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ListaRegiones.vue?vue&type=template&id=7d6f04d9&scoped=true& */ "./resources/js/components/mantenedores/regiones/ListaRegiones.vue?vue&type=template&id=7d6f04d9&scoped=true&");
/* harmony import */ var _ListaRegiones_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ListaRegiones.vue?vue&type=script&lang=js& */ "./resources/js/components/mantenedores/regiones/ListaRegiones.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ListaRegiones_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ListaRegiones_vue_vue_type_template_id_7d6f04d9_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ListaRegiones_vue_vue_type_template_id_7d6f04d9_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "7d6f04d9",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/mantenedores/regiones/ListaRegiones.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/mantenedores/regiones/ListaRegiones.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************!*\
  !*** ./resources/js/components/mantenedores/regiones/ListaRegiones.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ListaRegiones_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ListaRegiones.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/mantenedores/regiones/ListaRegiones.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ListaRegiones_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/mantenedores/regiones/ListaRegiones.vue?vue&type=template&id=7d6f04d9&scoped=true&":
/*!********************************************************************************************************************!*\
  !*** ./resources/js/components/mantenedores/regiones/ListaRegiones.vue?vue&type=template&id=7d6f04d9&scoped=true& ***!
  \********************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ListaRegiones_vue_vue_type_template_id_7d6f04d9_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ListaRegiones.vue?vue&type=template&id=7d6f04d9&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/mantenedores/regiones/ListaRegiones.vue?vue&type=template&id=7d6f04d9&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ListaRegiones_vue_vue_type_template_id_7d6f04d9_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ListaRegiones_vue_vue_type_template_id_7d6f04d9_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);
//# sourceMappingURL=mantenedores-component.js.map