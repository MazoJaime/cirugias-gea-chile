(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["registro-cirugia-component"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/asistencias/BitacoraAsistencia.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/asistencias/BitacoraAsistencia.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    asistencia: Object
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/asistencias/CirugiaWrapper.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/asistencias/CirugiaWrapper.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _SigaAsistencia__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./SigaAsistencia */ "./resources/js/components/asistencias/SigaAsistencia.vue");
/* harmony import */ var _BitacoraAsistencia__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./BitacoraAsistencia */ "./resources/js/components/asistencias/BitacoraAsistencia.vue");
/* harmony import */ var _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ckeditor/ckeditor5-build-classic */ "./node_modules/@ckeditor/ckeditor5-build-classic/build/ckeditor.js");
/* harmony import */ var _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_2__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


 // Import this component

/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    BitacoraAsistencia: _BitacoraAsistencia__WEBPACK_IMPORTED_MODULE_1__["default"],
    SigaAsistencia: _SigaAsistencia__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  data: function data() {
    return {
      editor: _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_2___default.a,
      editorData: '<p>Observaciones del caso aquí</p>',
      asistencia: {},
      observacion: '',
      costoTipo: '',
      editor2: '',
      user: this.$authUser,
      cobertura: false,
      topemanual: false,
      clasificacion: '',
      topeuf: 0,
      fechaacidente: '',
      fechaautoriza: '',
      cambiouf: 0,
      autoriza: '',
      nacimiento: '',
      bonos: [],
      selecBonos: '',
      costos: [{}],
      regiones: [],
      previsiones: []
    };
  },
  created: function created() {
    var _this = this;

    axios.get('/api/asistencia/' + this.$route.params.asistencia).then(function (response) {
      if (response.data.asistencia) {
        _this.asistencia = response.data.asistencia;

        if (_this.asistencia.costos) {
          _this.costoTipo = _this.asistencia.costos.tipo_cuenta;
          _this.bono.costo = _this.asistencia.costos.costo;
          _this.bono.insumos = _this.asistencia.costos.insumos;
          _this.bono.complementario = _this.asistencia.costos.complementos;
          _this.bono.costo_insumos = _this.asistencia.costos.costo_insumos;
          _this.bono.costo_complementario = _this.asistencia.costos.costo_complementos;
          _this.hospitalizacion.costo = _this.asistencia.costos.costo;
          _this.hospitalizacion.costo_insumos = _this.asistencia.costos.costo_insumos;
          _this.hospitalizacion.programa = _this.asistencia.costos.programa;
          _this.hospitalizacion.honorarios = _this.asistencia.costos.honorarios;
        }

        _this.previsiones = response.data.previsiones;
        _this.regiones = response.data.regiones;
      }
    });

    if (this.asistencia.region != null) {
      axios.get('/api/doctores/').then(function (response) {});
    }
  },
  methods: {
    pushselect: function pushselect(id) {
      var _this2 = this;

      //recibe array
      console.log(id);
      this.costos = [];

      var _loop = function _loop(i) {
        if (!_this2.costos.find(function (x) {
          return x.id == id[i];
        })) {
          _this2.costos.unshift(_this2.bonos.find(function (bono) {
            return bono.id == id[i];
          }));
        }
      };

      for (var i = 0; i < id.length; i++) {
        _loop(i);
      }
    },
    agregaObservacion: function agregaObservacion() {
      var _this3 = this;

      axios.post('/api/observacion/asistencia', {
        asistencia: this.asistencia.asistencia,
        texto: this.editorData
      }).then(function (response) {
        if (response.data.status === 'success') {
          _this3.asistencia.observaciones.unshift(response.data.observacion);

          _this3.observacion = null;
        }
      });
      this.editorData = '';
    },
    getBonos: function getBonos(id) {
      var _this4 = this;

      var url = '/api/ObtenerBonos/?bono=' + id;
      axios.get(url).then(function (response) {
        _this4.bonos = response.data;
      });
    },
    CreaCostos: function CreaCostos() {
      var formdata = this.dataCostos();
      axios.post('/api/costos/asistencia', formdata).then(function (response) {
        if (response.data.status && response.data.status === 'success') {
          console.log('costos creados correctamente');
        }
      });
    },
    actualizaCostos: function actualizaCostos() {
      var formdata = this.dataCostos();
      axios.put('/api/costos/asistencia', formdata).then(function (response) {
        if (response.data.status && response.data.status === 'success') {
          console.log('costos creados correctamente');
        }
      });
    },
    dataCostos: function dataCostos() {
      var formdata = {};
      formdata.asistencia = this.asistencia.asistencia;
      /*               'costo' => $request->costo,
                         'cuenta_tipo' => $request->cuenta_tipo,
                         'insumos' => $request->insumos ?? false,
                         'costo_insumos' => $request->costo_insumos,
                         'complementos' => $request->complementos ?? false,
                         'costo_complementos' => $request->costo_complementos,
                         'honorarios' => $request->honorarios,
                         'programa' => $request->programa,*/

      if (this.costoTipo == '1') {
        formdata.tipo_cuenta = this.costoTipo;
        formdata.insumos = this.bono.insumos;
        formdata.costo = this.bono.costo;
        formdata.costo_insumos = this.bono.costo_insumos;
        formdata.complementos = this.bono.complementario;
        formdata.costo_complementos = this.bono.costo_complementario;
      } else if (this.costoTipo == '2') {
        formdata.tipo_cuenta = this.costoTipo;
        formdata.costo = this.hospitalizacion.costo;
        formdata.costo_insumos = this.hospitalizacion.costo_insumos;
        formdata.costo_complementos = this.hospitalizacion.costo_complementario;
        formdata.honorarios = this.hospitalizacion.honorarios;
        formdata.programa = this.hospitalizacion.programa;
      }

      return formdata;
    },
    getValorUf: function getValorUf() {
      var _this5 = this;

      var fecha = this.fechaacidente.split('-');
      var url = '/api/uf/valor/' + fecha[0] + '/' + fecha[1] + '/' + fecha[2];
      axios.get(url).then(function (response) {
        _this5.cambiouf = response.data.precio;
      })["catch"](console.error);
    }
  },
  computed: {
    costoPad: function costoPad() {
      var val;

      for (var i = 0; i < costos.length; i++) {
        val += costos[i].valor;
      }

      return val - this.bono.costo_insumos - this.bono.costo_complementario;
    },
    cuentaAbierta: function cuentaAbierta() {
      return this.hospitalizacion.costo - this.hospitalizacion.honorarios - this.hospitalizacion.costo_insumos - this.hospitalizacion.programa;
    },
    topeenpesos: function topeenpesos() {
      return this.cambiouf * this.topeuf;
    },
    edad: function edad() {
      var nac = new Date(this.nacimiento);
      var diff_ms = Date.now() - nac.getTime();
      var age_dt = new Date(diff_ms);
      var nan = Math.abs(age_dt.getUTCFullYear() - 1970);

      if (isNaN(nan)) {
        return '';
      } else {
        return nan + ' años';
      }
    },
    tiempodesdeautorizacion: function tiempodesdeautorizacion() {
      if (this.fechaautoriza.length > 1) {
        var fec = new Date();
        var fec2 = new Date(this.fechaautoriza);
        var diffTime = Math.abs(fec2 - fec);
        var diffDays = Math.floor(diffTime / (1000 * 60 * 60 * 24));
        var dias = diffDays > 1 ? 'días' : 'día';
        return diffDays + ' ' + dias;
      } else {
        return '';
      }
    }
  },
  watch: {
    clasificacion: function clasificacion() {
      if (this.clasificacion == "1") {
        this.topeuf = 100;
        this.topemanual = true;
      } else if (this.clasificacion == "2") {
        this.topeuf = 0;
        this.topemanual = false;
      } else {
        this.topeuf = 120;
        this.topemanual = true;
      }
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/asistencias/RegistoCirugia.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/asistencias/RegistoCirugia.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      asistencia: '',
      expediente: '',
      cuenta: '',
      asistencia_apertura: '',
      expediente_apertura: '',
      usuario_creacion: '',
      fecha_creado: '',
      loading: false,
      badasistencia: false,
      informacioncreacion: {},
      prevision: '',
      nombre_afiliado: '',
      costo_asistencia: 0,
      rut_afiliado: ''
    };
  },
  computed: {
    creador: function creador() {
      return this.usuario_creacion || this.$authUser.name;
    },
    fechacreacion: function fechacreacion() {
      var d = new Date();
      return d.getFullYear() + '-' + d.getMonth() + '-' + d.getDate();
    }
  },
  methods: {
    contedorBusqueda: function contedorBusqueda() {
      if (this.asistencia.length > 1 && !isNaN(this.asistencia)) {
        this.asistencia = this.asistencia.trim();
        this.cleanData();
        this.loading = true;
        this.buscaSiga();
      }
    },
    buscaSiga: _.debounce(function () {
      var _this = this;

      axios.get('/api/asistencia/' + this.asistencia).then(function (response) {
        if (response.data.asistencia) {
          _this.cleanData();

          _this.$swal({
            type: 'warning',
            text: 'El numero de asistencia ya existe, intenta nuevamente'
          }).then(function () {
            _this.badasistencia = true;
          });
        }
      })["catch"](function (error) {
        var uri = 'http://siga.geasa.cl/servicios/swciru_servicios.php?type=unique';
        axios.defaults.headers.common = {};
        axios.get(uri, {
          params: {
            numasis: _this.asistencia
          }
        }).then(function (response) {
          if (response.data.status === 'success') {
            var res = response.data;
            _this.expediente = res.CVEEXPED;
            _this.cuenta = res.CUENTA;
            _this.asistencia_apertura = res.APERTURA_ASIS;
            _this.expediente_apertura = res.APERTURA_EXP;
            _this.nombre_afiliado = res.NOMAFILIADO;
            _this.prevision = parseInt(res.PREVISION);
            _this.costo_asistencia = parseFloat(res.COSTO_ASISTENCIA);
            _this.rut_afiliado = res.CVEAFILIADO;
            _this.badasistencia = false;
          } else {
            _this.$swal({
              type: 'error',
              text: 'El numero de asistencia entregado no existe en el siga como cirugia, verifica e intenta de nuevo'
            });

            _this.badasistencia = true;
          }

          _this.loading = false;
        })["catch"](function (error) {
          console.error(error);
          _this.loading = false;
        });
        console.log('asistencia NO existe internamente, se puede crear aqui');
      });
    }, 700),
    cleanData: function cleanData() {
      this.expediente = '';
      this.asistencia_apertura = '';
      this.expediente_apertura = '';
      this.cuenta = '';
    },
    guardar: function guardar() {
      var _this2 = this;

      var url = '/api/asistencias';
      var formdata = {
        'asistencia': this.asistencia,
        'expediente': this.expediente,
        'cuenta': this.cuenta,
        'nombre_afiliado': this.nombre_afiliado,
        'rut_afiliado': this.rut_afiliado,
        'apertura_expediente': this.expediente_apertura,
        'apertura_asistencia': this.asistencia_apertura,
        'prevision_id': this.prevision,
        'costo_asistencia': this.costo_asistencia
      };
      axios.post(url, formdata).then(function (response) {
        if (response.data.asistencia) {
          _this2.$swal({
            type: 'success',
            text: 'Asistencia ' + response.data.asistencia + ' se registro correctamente',
            showCancelButton: false
          }).then(function () {});

          _this2.$router.push({
            name: 'asistencia-trabajar',
            params: {
              asistencia: response.data.asistencia
            }
          });
        } else {
          _this2.$swal({
            type: 'error',
            text: 'Un problema ha impedido registrar la cirugia, por favor intente nuevamente, si el problema persiste informar al area de sistemas.'
          });
        }
      })["catch"](function (error) {
        _this2.errors = error.response.data.errors;
        var message = '';

        for (var _i = 0, _Object$keys = Object.keys(_this2.errors); _i < _Object$keys.length; _i++) {
          var field = _Object$keys[_i];
          console.log(_this2.errors[field][0]);
          message = _this2.errors[field][0];
          /*  message += ' ' +(this.errors[field][0], 'error');*/
        }

        _this2.$swal({
          type: 'error',
          text: 'La Asistencia no ha podido ser creada debido a que :' + message
        });

        _this2.errors = [];
      });
    }
  },
  created: function created() {
    var _this3 = this;

    axios.get('/api/asistencias/data').then(function (response) {
      _this3.informacioncreacion = response.data;
    });
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/asistencias/SigaAsistencia.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/asistencias/SigaAsistencia.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    asistencia: {
      "default": ''
    },
    expediente: {
      "default": ''
    },
    cuenta: {
      "default": ''
    },
    nombre_afiliado: {
      "default": ''
    },
    rut_afiliado: {
      "default": ''
    },
    apertura_expediente: {
      "default": ''
    },
    apertura_asistencia: {
      "default": ''
    },
    prevision: {
      "default": ''
    },
    costo_asistencia: {
      "default": ''
    },
    usuario_creacion: {
      "default": ''
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/asistencias/CirugiaWrapper.vue?vue&type=style&index=0&lang=css&":
/*!********************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/asistencias/CirugiaWrapper.vue?vue&type=style&index=0&lang=css& ***!
  \********************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\nblockquote {\n\n    margin: 3rem auto;\n    font-style: italic;\n    color: #555555;\n    padding: 2rem;\n    line-height: 1.6;\n    position: relative;\n    background: #EDEDED;\n}\nblockquote::after {\n    content: '';\n}\nfigure.table > table, tbody, th, td {\n    border: 1px solid #e9ecef;\n}\n\n\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/asistencias/CirugiaWrapper.vue?vue&type=style&index=0&lang=css&":
/*!************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/asistencias/CirugiaWrapper.vue?vue&type=style&index=0&lang=css& ***!
  \************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./CirugiaWrapper.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/asistencias/CirugiaWrapper.vue?vue&type=style&index=0&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/asistencias/BitacoraAsistencia.vue?vue&type=template&id=50f57a6a&scoped=true&":
/*!*********************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/asistencias/BitacoraAsistencia.vue?vue&type=template&id=50f57a6a&scoped=true& ***!
  \*********************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "base-modal",
    { attrs: { modalSize: "modal-lg" } },
    [
      _c(
        "h5",
        {
          staticClass: "modal-title text-white",
          attrs: { slot: "header", id: "modal-title-notification" },
          slot: "header"
        },
        [_vm._v("\n        Bitacora de acciones\n    ")]
      ),
      _vm._v(" "),
      _c(
        "div",
        { attrs: { slot: "body" }, slot: "body" },
        _vm._l(_vm.asistencia.bitacora, function(bitacora) {
          return _c(
            "card",
            { key: bitacora.id, staticClass: "mt-1" },
            [
              _vm._t(
                "default",
                [
                  _vm._v(
                    " " +
                      _vm._s(bitacora.creador.name) +
                      " el " +
                      _vm._s(bitacora.created_at.split(" ")[0]) +
                      " a las\n                " +
                      _vm._s(bitacora.created_at.split(" ")[1]) +
                      "\n            "
                  )
                ],
                { slot: "header" }
              ),
              _vm._v(
                "\n            " + _vm._s(bitacora.observacion) + "\n        "
              )
            ],
            2
          )
        }),
        1
      ),
      _vm._v(" "),
      _c("template", { slot: "footer" }, [
        _c("div", { staticClass: "text-center" }, [
          _c(
            "button",
            {
              staticClass: "btn btn-primary ml-auto",
              attrs: { type: "button", "data-dismiss": "modal" }
            },
            [_vm._v("Cerrar")]
          )
        ])
      ])
    ],
    2
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/asistencias/CirugiaWrapper.vue?vue&type=template&id=71020f5e&":
/*!*****************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/asistencias/CirugiaWrapper.vue?vue&type=template&id=71020f5e& ***!
  \*****************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("div", { staticClass: "row mt-3" }, [
        _c(
          "div",
          { staticClass: "col" },
          [
            _c(
              "router-link",
              {
                staticClass: "btn btn-default float-right mx-1",
                attrs: { to: { name: "asistencias-list" } }
              },
              [_vm._v(" Listado\n            ")]
            ),
            _vm._v(" "),
            _vm._m(0)
          ],
          1
        )
      ]),
      _vm._v(" "),
      _c(
        "tabs",
        { staticClass: "flex-column flex-md-row", attrs: { fill: "" } },
        [
          _c(
            "card",
            { attrs: { shadow: "" } },
            [
              _c(
                "tab-pane",
                { attrs: { title: "asistencia" } },
                [
                  _c(
                    "span",
                    { attrs: { slot: "asistencias" }, slot: "asistencias" },
                    [
                      _vm._v(
                        "\n                    Asistencia\n                "
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c("siga-asistencia", {
                    attrs: {
                      asistencia: _vm.asistencia.asistencia,
                      expediente: _vm.asistencia.expediente,
                      cuenta: _vm.asistencia.cuenta,
                      nombre_afiliado: _vm.asistencia.nombre_afiliado,
                      rut_afiliado: _vm.asistencia.rut_afiliado,
                      apertura_expediente: _vm.asistencia.apertura_expediente,
                      apertura_asistencia: _vm.asistencia.apertura_asistencia,
                      prevision: _vm.asistencia.prevision,
                      costo_asistencia: _vm.asistencia.costo_asistencia,
                      usuario_creacion: _vm.asistencia.creador
                    }
                  }),
                  _vm._v(" "),
                  _c("div", { staticClass: "row" }, [
                    _vm.regiones.length > 0
                      ? _c("div", { staticClass: "form-group" }, [
                          _c(
                            "select",
                            {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.asistencia.region,
                                  expression: "asistencia.region"
                                }
                              ],
                              staticClass: "custom-select",
                              on: {
                                change: function($event) {
                                  var $$selectedVal = Array.prototype.filter
                                    .call($event.target.options, function(o) {
                                      return o.selected
                                    })
                                    .map(function(o) {
                                      var val =
                                        "_value" in o ? o._value : o.value
                                      return val
                                    })
                                  _vm.$set(
                                    _vm.asistencia,
                                    "region",
                                    $event.target.multiple
                                      ? $$selectedVal
                                      : $$selectedVal[0]
                                  )
                                }
                              }
                            },
                            [
                              _c("option", [_vm._v(" Escoja")]),
                              _vm._v(" "),
                              _vm._l(_vm.regiones, function(region) {
                                return _c(
                                  "option",
                                  {
                                    key: region.id,
                                    domProps: { value: region.id }
                                  },
                                  [
                                    _vm._v(
                                      _vm._s(region.nombre) +
                                        "\n                            "
                                    )
                                  ]
                                )
                              })
                            ],
                            2
                          )
                        ])
                      : _vm._e()
                  ])
                ],
                1
              ),
              _vm._v(" "),
              _c("tab-pane", { attrs: { tittle: "coberturas" } }, [
                _c("span", { attrs: { slot: "title" }, slot: "title" }, [
                  _vm._v("\n                    Coberturas\n                ")
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "row" }, [
                  _c("div", { staticClass: "col-6" }, [
                    _c("label", [_vm._v("Fecha del accidente")]),
                    _vm._v(" "),
                    _c("div", { staticClass: "input-group " }, [
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.fechaacidente,
                            expression: "fechaacidente"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: { type: "date" },
                        domProps: { value: _vm.fechaacidente },
                        on: {
                          change: _vm.getValorUf,
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.fechaacidente = $event.target.value
                          }
                        }
                      })
                    ])
                  ]),
                  _vm._v(" "),
                  _vm.fechaacidente.length > 1
                    ? _c("div", { staticClass: "col-6" }, [
                        _c("label", [_vm._v("Cambio UF")]),
                        _vm._v(" "),
                        _c("div", { staticClass: "input-group " }, [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.cambiouf,
                                expression: "cambiouf"
                              }
                            ],
                            staticClass: "form-control",
                            attrs: { type: "number" },
                            domProps: { value: _vm.cambiouf },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.cambiouf = $event.target.value
                              }
                            }
                          })
                        ])
                      ])
                    : _vm._e(),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-3" }, [
                    _c("label", { staticClass: "label" }, [
                      _vm._v("CObertua especial:")
                    ]),
                    _vm._v(" "),
                    _c("span", { staticClass: "clearfix" }),
                    _vm._v(" "),
                    _c("label", [_vm._v("NO/SI")]),
                    _vm._v(" "),
                    _c("span", { staticClass: "clearfix" }),
                    _vm._v(" "),
                    _c("label", { staticClass: "custom-toggle" }, [
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.cobertura,
                            expression: "cobertura"
                          }
                        ],
                        attrs: { type: "checkbox" },
                        domProps: {
                          checked: Array.isArray(_vm.cobertura)
                            ? _vm._i(_vm.cobertura, null) > -1
                            : _vm.cobertura
                        },
                        on: {
                          change: function($event) {
                            var $$a = _vm.cobertura,
                              $$el = $event.target,
                              $$c = $$el.checked ? true : false
                            if (Array.isArray($$a)) {
                              var $$v = null,
                                $$i = _vm._i($$a, $$v)
                              if ($$el.checked) {
                                $$i < 0 && (_vm.cobertura = $$a.concat([$$v]))
                              } else {
                                $$i > -1 &&
                                  (_vm.cobertura = $$a
                                    .slice(0, $$i)
                                    .concat($$a.slice($$i + 1)))
                              }
                            } else {
                              _vm.cobertura = $$c
                            }
                          }
                        }
                      }),
                      _vm._v(" "),
                      _c("span", {
                        staticClass: "custom-toggle-slider rounded-circle"
                      })
                    ])
                  ])
                ]),
                _vm._v(" "),
                _vm.cobertura
                  ? _c("div", { staticClass: "row" }, [
                      _c("div", { staticClass: "col-6" }, [
                        _c("label", [_vm._v("Clasificacion Cliente")]),
                        _vm._v(" "),
                        _c(
                          "select",
                          {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.clasificacion,
                                expression: "clasificacion"
                              }
                            ],
                            staticClass: "custom-select",
                            on: {
                              change: function($event) {
                                var $$selectedVal = Array.prototype.filter
                                  .call($event.target.options, function(o) {
                                    return o.selected
                                  })
                                  .map(function(o) {
                                    var val = "_value" in o ? o._value : o.value
                                    return val
                                  })
                                _vm.clasificacion = $event.target.multiple
                                  ? $$selectedVal
                                  : $$selectedVal[0]
                              }
                            }
                          },
                          [
                            _c("option", { attrs: { value: "1" } }, [
                              _vm._v("deportista")
                            ]),
                            _vm._v(" "),
                            _c("option", { attrs: { value: "2" } }, [
                              _vm._v("Excepcion")
                            ]),
                            _vm._v(" "),
                            _c("option", { attrs: { value: "3" } }, [
                              _vm._v("Fondo solidario")
                            ])
                          ]
                        )
                      ])
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _c("div", { staticClass: "row" }, [
                  _c("div", { staticClass: "col-6" }, [
                    _c("label", [_vm._v("Tope Cobertura UF")]),
                    _vm._v(" "),
                    _c("div", { staticClass: "input-group " }, [
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.topeuf,
                            expression: "topeuf"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: { type: "number", readonly: _vm.topemanual },
                        domProps: { value: _vm.topeuf },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.topeuf = $event.target.value
                          }
                        }
                      })
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-6" }, [
                    _c("label", [_vm._v("Tope CObertura $")]),
                    _vm._v(" "),
                    _c("div", { staticClass: "input-group " }, [
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.topeenpesos,
                            expression: "topeenpesos"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: { type: "number", readonly: "" },
                        domProps: { value: _vm.topeenpesos },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.topeenpesos = $event.target.value
                          }
                        }
                      })
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-6" }, [
                    _c("label", [_vm._v("NOmbre quien autoriza")]),
                    _vm._v(" "),
                    _c("div", { staticClass: "input-group " }, [
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.autoriza,
                            expression: "autoriza"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: { type: "text" },
                        domProps: { value: _vm.autoriza },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.autoriza = $event.target.value
                          }
                        }
                      })
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-6" }, [
                    _c("label", [_vm._v("Fecha autoriza")]),
                    _vm._v(" "),
                    _c("div", { staticClass: "input-group " }, [
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.fechaautoriza,
                            expression: "fechaautoriza"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: { type: "datetime-local" },
                        domProps: { value: _vm.fechaautoriza },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.fechaautoriza = $event.target.value
                          }
                        }
                      })
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-6" }, [
                    _c("label", [_vm._v("Prevision Final")]),
                    _vm._v(" "),
                    _c("select", { staticClass: "custom-select" }, [
                      _c("option", { attrs: { value: "1" } }, [
                        _vm._v("FONASA A")
                      ]),
                      _vm._v(" "),
                      _c("option", { attrs: { value: "2" } }, [
                        _vm._v("FONASA B")
                      ]),
                      _vm._v(" "),
                      _c("option", { attrs: { value: "3" } }, [
                        _vm._v("FONASA D")
                      ])
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-6" }, [
                    _c("label", [_vm._v("Fecha Nacimiento")]),
                    _vm._v(" "),
                    _c("div", { staticClass: "input-group " }, [
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.nacimiento,
                            expression: "nacimiento"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: { type: "date" },
                        domProps: { value: _vm.nacimiento },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.nacimiento = $event.target.value
                          }
                        }
                      })
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-3" }, [
                    _c("label", [_vm._v("Edad")]),
                    _vm._v(" "),
                    _c("div", { staticClass: "input-group " }, [
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.edad,
                            expression: "edad"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: { type: "text" },
                        domProps: { value: _vm.edad },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.edad = $event.target.value
                          }
                        }
                      })
                    ])
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "row" }, [
                  _c("div", { staticClass: "col-12 " }, [
                    _c("label", [_vm._v("Que sucedio")]),
                    _vm._v(" "),
                    _c("textarea", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.editor2,
                          expression: "editor2"
                        }
                      ],
                      staticClass: "form-control",
                      domProps: { value: _vm.editor2 },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.editor2 = $event.target.value
                        }
                      }
                    })
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-6" }, [
                    _c("label", [_vm._v("diagnostico")]),
                    _vm._v(" "),
                    _c("select", { staticClass: "custom-select" }, [
                      _c("option", { attrs: { value: "1" } }, [
                        _vm._v("Enfermedad 1")
                      ]),
                      _vm._v(" "),
                      _c("option", { attrs: { value: "2" } }, [
                        _vm._v("Enfermedad 2")
                      ]),
                      _vm._v(" "),
                      _c("option", { attrs: { value: "3" } }, [
                        _vm._v("Enfermedad 3")
                      ])
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-6" }, [
                    _c("label", [_vm._v("Tiempo desde autorizacion ")]),
                    _vm._v(" "),
                    _c("div", { staticClass: "input-group " }, [
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.tiempodesdeautorizacion,
                            expression: "tiempodesdeautorizacion"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: { type: "text" },
                        domProps: { value: _vm.tiempodesdeautorizacion },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.tiempodesdeautorizacion = $event.target.value
                          }
                        }
                      })
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-6" }, [
                    _c("label", [_vm._v(" Medico staff control")]),
                    _vm._v(" "),
                    _c("select")
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-6" }, [
                    _c("label", [_vm._v("Fecha 1er control")]),
                    _vm._v(" "),
                    _c("div", { staticClass: "input-group " }, [
                      _c("input", {
                        staticClass: "form-control",
                        attrs: { type: "date" }
                      })
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-6" }, [
                    _c("label", [_vm._v(" Procedimientos")]),
                    _vm._v(" "),
                    _c("select")
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-6" }, [
                    _c("label", [_vm._v("Fecha autorizacion Cirugia")]),
                    _vm._v(" "),
                    _c("div", { staticClass: "input-group " }, [
                      _c("input", {
                        staticClass: "form-control",
                        attrs: { type: "date" }
                      })
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-6" }, [
                    _c("label", [_vm._v("fecha de cirguia")]),
                    _vm._v(" "),
                    _c("div", { staticClass: "input-group " }, [
                      _c("input", {
                        staticClass: "form-control",
                        attrs: { type: "date" }
                      })
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-6" }, [
                    _c("label", [_vm._v(" status de la cirugia")]),
                    _vm._v(" "),
                    _c("select")
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-6" }, [
                    _c("label", [
                      _vm._v("Tiempo de cirugia desde autorizacion ")
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "input-group " }, [
                      _c("input", {
                        staticClass: "form-control",
                        attrs: { type: "text" }
                      })
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-6" }, [
                    _c("label", [_vm._v("Tiempo de cirugia desde accidente ")]),
                    _vm._v(" "),
                    _c("div", { staticClass: "input-group " }, [
                      _c("input", {
                        staticClass: "form-control",
                        attrs: { type: "text" }
                      })
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-6" }, [
                    _c("label", [_vm._v("Tiempo de gestion de cirugia ")]),
                    _vm._v(" "),
                    _c("div", { staticClass: "input-group " }, [
                      _c("input", {
                        staticClass: "form-control",
                        attrs: { type: "text" }
                      })
                    ])
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("tab-pane", { attrs: { title: "costos" } }, [
                _c("span", { attrs: { slot: "title" }, slot: "title" }, [
                  _vm._v("\n\n                    Costos\n                ")
                ]),
                _vm._v(" "),
                _c(
                  "select",
                  {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.costoTipo,
                        expression: "costoTipo"
                      }
                    ],
                    staticClass: "custom-select",
                    attrs: { id: "select-tipo-costo" },
                    on: {
                      change: [
                        function($event) {
                          var $$selectedVal = Array.prototype.filter
                            .call($event.target.options, function(o) {
                              return o.selected
                            })
                            .map(function(o) {
                              var val = "_value" in o ? o._value : o.value
                              return val
                            })
                          _vm.costoTipo = $event.target.multiple
                            ? $$selectedVal
                            : $$selectedVal[0]
                        },
                        function($event) {
                          return _vm.getBonos(_vm.costoTipo)
                        }
                      ]
                    }
                  },
                  [
                    _c("option", { attrs: { value: "" } }, [_vm._v("escoja")]),
                    _vm._v(" "),
                    _c("option", { attrs: { value: "1" } }, [
                      _vm._v("Bono Pad")
                    ]),
                    _vm._v(" "),
                    _c("option", { attrs: { value: "2" } }, [_vm._v(" Otro")])
                  ]
                ),
                _vm._v(" "),
                _vm.costoTipo == "1"
                  ? _c(
                      "div",
                      { staticClass: "m-2 row" },
                      [
                        _c(
                          "div",
                          { staticClass: "col-8 mb-3" },
                          [
                            _c("v-select", {
                              attrs: {
                                multiple: "",
                                options: _vm.bonos,
                                reduce: function(bono) {
                                  return bono.id
                                },
                                label: "descripcion",
                                selectable: function() {
                                  return _vm.selecBonos.length < 3
                                }
                              },
                              on: { input: _vm.pushselect },
                              model: {
                                value: _vm.selecBonos,
                                callback: function($$v) {
                                  _vm.selecBonos = $$v
                                },
                                expression: "selecBonos"
                              }
                            })
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _vm._l(_vm.costos, function(costo) {
                          return _c("div", { staticClass: "col-6 mb-2" }, [
                            _c("label", [_vm._v("Costo Bono")]),
                            _vm._v(" "),
                            _c("div", { staticClass: "input-group " }, [
                              _c(
                                "div",
                                { staticClass: "input-group-prepend" },
                                [
                                  _c(
                                    "span",
                                    { staticClass: "input-group-text " },
                                    [
                                      _c("i", {
                                        staticClass: "fa fa-dollar-sign"
                                      })
                                    ]
                                  )
                                ]
                              ),
                              _vm._v(" "),
                              _c("input", {
                                staticClass: "form-control",
                                attrs: { type: "number" },
                                domProps: { value: costo.valor }
                              })
                            ])
                          ])
                        }),
                        _vm._v(" "),
                        _c("div", { staticClass: "row" }, [
                          _c("div", { staticClass: "col-6" }, [
                            _c("label", [_vm._v("Total Bono Pad")]),
                            _vm._v(" "),
                            _c("div", { staticClass: "input-group " }, [
                              _c(
                                "div",
                                { staticClass: "input-group-prepend" },
                                [
                                  _c(
                                    "span",
                                    {
                                      staticClass: "input-group-text readonly"
                                    },
                                    [
                                      _c("i", {
                                        staticClass: "fa fa-dollar-sign"
                                      })
                                    ]
                                  )
                                ]
                              ),
                              _vm._v(" "),
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.costoPad,
                                    expression: "costoPad"
                                  }
                                ],
                                staticClass: "form-control",
                                attrs: { type: "number", readonly: "" },
                                domProps: { value: _vm.costoPad },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.costoPad = $event.target.value
                                  }
                                }
                              })
                            ])
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "col-3" }, [
                            _c("label", { staticClass: "label" }, [
                              _vm._v("Requiere Insumos:")
                            ]),
                            _vm._v(" "),
                            _c("span", { staticClass: "clearfix" }),
                            _vm._v(" "),
                            _c("label", [_vm._v("NO/SI")]),
                            _vm._v(" "),
                            _c("span", { staticClass: "clearfix" }),
                            _vm._v(" "),
                            _c("label", { staticClass: "custom-toggle" }, [
                              _c("input", { attrs: { type: "checkbox" } }),
                              _vm._v(" "),
                              _c("span", {
                                staticClass:
                                  "custom-toggle-slider rounded-circle"
                              })
                            ])
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "col-3" }, [
                            _c("label", [_vm._v("Costo Insumos")]),
                            _vm._v(" "),
                            _c("div", { staticClass: "input-group " }, [
                              _c(
                                "div",
                                { staticClass: "input-group-prepend" },
                                [
                                  _c(
                                    "span",
                                    { staticClass: "input-group-text " },
                                    [
                                      _c("i", {
                                        staticClass: "fa fa-dollar-sign"
                                      })
                                    ]
                                  )
                                ]
                              ),
                              _vm._v(" "),
                              _c("input", {
                                staticClass: "form-control",
                                attrs: { type: "number" }
                              })
                            ])
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "col-3" }, [
                            _c("label", { staticClass: "label" }, [
                              _vm._v("Requiere Complementario:")
                            ]),
                            _vm._v(" "),
                            _c("span", { staticClass: "clearfix" }),
                            _vm._v(" "),
                            _c("label", [_vm._v("NO/SI")]),
                            _vm._v(" "),
                            _c("span", { staticClass: "clearfix" }),
                            _vm._v(" "),
                            _c("label", { staticClass: "custom-toggle" }, [
                              _c("input", { attrs: { type: "checkbox" } }),
                              _vm._v(" "),
                              _c("span", {
                                staticClass:
                                  "custom-toggle-slider rounded-circle"
                              })
                            ])
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "col-3" }, [
                            _c("label", [_vm._v("Costo Insumos")]),
                            _vm._v(" "),
                            _c("div", { staticClass: "input-group " }, [
                              _c(
                                "div",
                                { staticClass: "input-group-prepend" },
                                [
                                  _c(
                                    "span",
                                    { staticClass: "input-group-text " },
                                    [
                                      _c("i", {
                                        staticClass: "fa fa-dollar-sign"
                                      })
                                    ]
                                  )
                                ]
                              ),
                              _vm._v(" "),
                              _c("input", {
                                staticClass: "form-control",
                                attrs: { type: "number" }
                              })
                            ])
                          ])
                        ])
                      ],
                      2
                    )
                  : _vm._e()
              ]),
              _vm._v(" "),
              _c(
                "tab-pane",
                { staticClass: "row", attrs: { title: "observaciones" } },
                [
                  _c("span", { attrs: { slot: "title" }, slot: "title" }, [
                    _vm._v(
                      "\n\n                Observaciones\n\n                "
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-lg-12 col-md-12 col-sm-12" }, [
                    _c(
                      "button",
                      {
                        staticClass: "btn btn-success float-right mx-1",
                        attrs: {
                          "data-toggle": "modal",
                          "data-target": "#modal-form-observacion"
                        }
                      },
                      [
                        _c(
                          "span",
                          {
                            staticClass: "btn-inner--icon",
                            attrs: { "data-toggle": "modal" }
                          },
                          [_c("i", { staticClass: "fa fa-book-open" })]
                        ),
                        _vm._v(" "),
                        _c("span", { staticClass: "btn-inner--text" }, [
                          _vm._v("Agregar Observación")
                        ])
                      ]
                    )
                  ]),
                  _vm._v(" "),
                  _vm._l(_vm.asistencia.observaciones, function(observacion) {
                    return _c(
                      "card",
                      {
                        key: observacion.id,
                        staticClass: " col-md-5 col-lg-5 my-1 mx-5"
                      },
                      [
                        _vm._t(
                          "default",
                          [
                            _vm._v(
                              " " +
                                _vm._s(observacion.creador.name) +
                                " el\n                        " +
                                _vm._s(observacion.created_at) +
                                "\n                    "
                            )
                          ],
                          { slot: "header" }
                        ),
                        _vm._v(" "),
                        _c("div", {
                          domProps: { innerHTML: _vm._s(observacion.texto) }
                        })
                      ],
                      2
                    )
                  })
                ],
                2
              ),
              _vm._v(" "),
              _c("tab-pane", { attrs: { tittle: "atencion" } }, [
                _c("span", { attrs: { slot: "title" }, slot: "title" }, [
                  _vm._v("\n                Atención\n                ")
                ]),
                _vm._v(" "),
                _c("div", [
                  _c("div", { staticClass: "form-group" }, [
                    _c("label", { staticClass: "clearfix" }, [
                      _vm._v(" Acudio a control con staf")
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group" }, [
                      _c("input", {
                        staticClass: "custom-radio",
                        attrs: {
                          type: "radio",
                          value: "1",
                          name: "staff-acudio",
                          id: "staff-si"
                        }
                      }),
                      _c("label", { attrs: { for: "staff-si" } }, [
                        _vm._v("Si")
                      ]),
                      _vm._v(" "),
                      _c("input", {
                        staticClass: "custom-radio",
                        attrs: {
                          type: "radio",
                          value: "0",
                          name: "staff-acudio",
                          id: "staff-no"
                        }
                      }),
                      _vm._v(" "),
                      _c("label", { attrs: { for: "staff-no" } }, [
                        _vm._v("No")
                      ])
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "form-group" }, [
                    _c("select", { attrs: { name: "modelo-atencion" } }, [
                      _c("option", [_vm._v("Escoja")]),
                      _vm._v(" "),
                      _c("option", { attrs: { value: "1" } }, [
                        _vm._v("Escoja1")
                      ])
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "form-group" }, [
                    _c("select", { attrs: { name: "modelo-atencion" } }, [
                      _c("option", [_vm._v("Escoja")]),
                      _vm._v(" "),
                      _c("option", { attrs: { value: "1" } }, [
                        _vm._v("Escoja1")
                      ])
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "form-group" }, [
                    _c("select", { attrs: { name: "modelo-atencion" } }, [
                      _c("option", [_vm._v("Escoja")]),
                      _vm._v(" "),
                      _c("option", { attrs: { value: "1" } }, [
                        _vm._v("Escoja1")
                      ])
                    ])
                  ])
                ])
              ])
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c("bitacora-asistencia", { attrs: { asistencia: _vm.asistencia } }),
      _vm._v(" "),
      _c(
        "base-modal",
        {
          attrs: {
            modalIdentification: "modal-form-observacion",
            modalSize: "modal-lg"
          }
        },
        [
          _c(
            "h5",
            {
              staticClass: "modal-title",
              attrs: { slot: "header", id: "modal-title-observacion" },
              slot: "header"
            },
            [_vm._v("\n            Registrar Observación del caso\n        ")]
          ),
          _vm._v(" "),
          _c(
            "div",
            { attrs: { slot: "body" }, slot: "body" },
            [
              _c("ckeditor", {
                attrs: { editor: _vm.editor },
                model: {
                  value: _vm.editorData,
                  callback: function($$v) {
                    _vm.editorData = $$v
                  },
                  expression: "editorData"
                }
              })
            ],
            1
          ),
          _vm._v(" "),
          _c("template", { slot: "footer" }, [
            _c(
              "button",
              {
                staticClass: "btn btn-primary ",
                attrs: { type: "button", "data-dismiss": "modal" }
              },
              [_vm._v("Cerrar\n            ")]
            ),
            _vm._v(" "),
            _c(
              "button",
              {
                staticClass: "btn btn-success ml-auto",
                attrs: { type: "button" },
                on: { click: _vm.agregaObservacion }
              },
              [_vm._v("Agregar")]
            )
          ])
        ],
        2
      )
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "button",
      {
        staticClass: "btn btn-default float-right mx-1",
        attrs: { "data-toggle": "modal", "data-target": "#modal-notification" }
      },
      [
        _c("span", { staticClass: "btn-inner--icon" }, [
          _c("i", { staticClass: "fa fa-book-open" })
        ]),
        _vm._v(" "),
        _c("span", { staticClass: "btn-inner--text" }, [_vm._v("Bitacora")])
      ]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/asistencias/RegistoCirugia.vue?vue&type=template&id=433db4c4&scoped=true&":
/*!*****************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/asistencias/RegistoCirugia.vue?vue&type=template&id=433db4c4&scoped=true& ***!
  \*****************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "card" }, [
    _vm._m(0),
    _vm._v(" "),
    _c("div", { staticClass: "card-body" }, [
      _c("table", { staticClass: "table table-borderless " }, [
        _c("tr", [
          _c("td", [_vm._v("Fecha de registro:")]),
          _vm._v(" "),
          _c("td", [
            _vm._v(
              "\n                    " +
                _vm._s(_vm.fechacreacion) +
                "\n                "
            )
          ]),
          _vm._v(" "),
          _c("td", [_vm._v("Usuario Apertura:")]),
          _vm._v(" "),
          _c("td", [_vm._v(_vm._s(_vm.creador))])
        ]),
        _vm._v(" "),
        _c("tr", [
          _c("td", [_vm._v("Asistencia:")]),
          _vm._v(" "),
          _c("td", [
            _c(
              "div",
              {
                staticClass: "form-group",
                class: _vm.badasistencia ? "has-danger" : ""
              },
              [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.asistencia,
                      expression: "asistencia"
                    }
                  ],
                  staticClass: "form-control",
                  class: _vm.badasistencia ? "is-invalid" : "",
                  attrs: { type: "text", name: "asistencia" },
                  domProps: { value: _vm.asistencia },
                  on: {
                    input: [
                      function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.asistencia = $event.target.value
                      },
                      _vm.contedorBusqueda
                    ]
                  }
                }),
                _vm._v(" "),
                _vm.loading ? _c("p", [_vm._v("⟳ Buscando")]) : _vm._e()
              ]
            )
          ]),
          _vm._v(" "),
          _c("td", [_vm._v("Apertura Asistencia:")]),
          _vm._v(" "),
          _c("td", [
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.asistencia_apertura,
                  expression: "asistencia_apertura"
                }
              ],
              staticClass: "form-control",
              attrs: { type: "date", readonly: "" },
              domProps: { value: _vm.asistencia_apertura },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.asistencia_apertura = $event.target.value
                }
              }
            })
          ])
        ]),
        _vm._v(" "),
        _c("tr", [
          _c("td", [_vm._v("Expediente:")]),
          _vm._v(" "),
          _c("td", [
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.expediente,
                  expression: "expediente"
                }
              ],
              staticClass: "form-control readonly",
              attrs: { type: "text", readonly: "" },
              domProps: { value: _vm.expediente },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.expediente = $event.target.value
                }
              }
            })
          ]),
          _vm._v(" "),
          _c("td", [_vm._v("Apertura Expediente:")]),
          _vm._v(" "),
          _c("td", [
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.expediente_apertura,
                  expression: "expediente_apertura"
                }
              ],
              staticClass: "form-control",
              attrs: { type: "date", readonly: "" },
              domProps: { value: _vm.expediente_apertura },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.expediente_apertura = $event.target.value
                }
              }
            })
          ])
        ]),
        _vm._v(" "),
        _c("tr", [
          _c("td", [_vm._v("Cuenta:")]),
          _vm._v(" "),
          _c("td", [
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.cuenta,
                  expression: "cuenta"
                }
              ],
              staticClass: "form-control",
              attrs: { type: "text", readonly: "" },
              domProps: { value: _vm.cuenta },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.cuenta = $event.target.value
                }
              }
            })
          ]),
          _vm._v(" "),
          _c("td", [_vm._v("Prevision Inicial:")]),
          _vm._v(" "),
          _c(
            "td",
            [
              _c("v-select", {
                attrs: {
                  label: "nombre",
                  options: _vm.informacioncreacion.previsiones,
                  disabled: "",
                  reduce: function(option) {
                    return option.id
                  }
                },
                model: {
                  value: _vm.prevision,
                  callback: function($$v) {
                    _vm.prevision = $$v
                  },
                  expression: "prevision"
                }
              })
            ],
            1
          )
        ]),
        _vm._v(" "),
        _c("tr", [
          _c("td", [_vm._v("Nombre del Afliado:")]),
          _vm._v(" "),
          _c("td", [
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.nombre_afiliado,
                  expression: "nombre_afiliado"
                }
              ],
              staticClass: "form-control",
              attrs: { type: "text", readonly: "" },
              domProps: { value: _vm.nombre_afiliado },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.nombre_afiliado = $event.target.value
                }
              }
            })
          ]),
          _vm._v(" "),
          _c("td", [_vm._v("Costo Actual Siga:")]),
          _vm._v(" "),
          _c("td", [
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.costo_asistencia,
                  expression: "costo_asistencia"
                }
              ],
              staticClass: "form-control",
              attrs: { type: "text", readonly: "" },
              domProps: { value: _vm.costo_asistencia },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.costo_asistencia = $event.target.value
                }
              }
            })
          ])
        ]),
        _vm._v(" "),
        _c("tr", [
          _c("td", [_vm._v("RUT del Afliado:")]),
          _vm._v(" "),
          _c("td", [
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.rut_afiliado,
                  expression: "rut_afiliado"
                }
              ],
              staticClass: "form-control",
              attrs: { type: "text", readonly: "" },
              domProps: { value: _vm.rut_afiliado },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.rut_afiliado = $event.target.value
                }
              }
            })
          ])
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "row justify-content-md-center" }, [
        _c("div", { staticClass: "col-md-auto" }, [
          _c(
            "button",
            { staticClass: "btn btn-success", on: { click: _vm.guardar } },
            [_vm._v(" Continuar")]
          )
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "card-header" }, [
      _c("h1", { staticClass: "card-title" }, [_vm._v("Ficha desde el SIGA")])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/asistencias/SigaAsistencia.vue?vue&type=template&id=148f7c4b&scoped=true&":
/*!*****************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/asistencias/SigaAsistencia.vue?vue&type=template&id=148f7c4b&scoped=true& ***!
  \*****************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "row pt-md-3 pt-lg-3" }, [
    _c("div", { staticClass: "col col-md-2" }, [
      _c("div", { staticClass: "form-group" }, [
        _c("label", [_vm._v("Expediente")]),
        _vm._v(" "),
        _c("input", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.expediente,
              expression: "expediente"
            }
          ],
          staticClass: "form-control",
          attrs: { type: "text", readonly: "" },
          domProps: { value: _vm.expediente },
          on: {
            input: function($event) {
              if ($event.target.composing) {
                return
              }
              _vm.expediente = $event.target.value
            }
          }
        })
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "col col-md-2" }, [
      _c("div", { staticClass: "form-group" }, [
        _c("label", [_vm._v("Asistencia")]),
        _vm._v(" "),
        _c("input", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.asistencia,
              expression: "asistencia"
            }
          ],
          staticClass: "form-control",
          attrs: { type: "text", readonly: "" },
          domProps: { value: _vm.asistencia },
          on: {
            input: function($event) {
              if ($event.target.composing) {
                return
              }
              _vm.asistencia = $event.target.value
            }
          }
        })
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "col col-md-2" }, [
      _c("div", { staticClass: "form-group" }, [
        _c("label", [_vm._v("Cuenta")]),
        _vm._v(" "),
        _c("input", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.cuenta,
              expression: "cuenta"
            }
          ],
          staticClass: "form-control",
          attrs: { type: "text", readonly: "" },
          domProps: { value: _vm.cuenta },
          on: {
            input: function($event) {
              if ($event.target.composing) {
                return
              }
              _vm.cuenta = $event.target.value
            }
          }
        })
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "col col-md-2" }, [
      _c("div", { staticClass: "form-group" }, [
        _c("label", [_vm._v("Rut del Afiliado")]),
        _vm._v(" "),
        _c("input", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.rut_afiliado,
              expression: "rut_afiliado"
            }
          ],
          staticClass: "form-control",
          attrs: { type: "text", readonly: "" },
          domProps: { value: _vm.rut_afiliado },
          on: {
            input: function($event) {
              if ($event.target.composing) {
                return
              }
              _vm.rut_afiliado = $event.target.value
            }
          }
        })
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "col col-md-4" }, [
      _c("div", { staticClass: "form-group" }, [
        _c("label", [_vm._v("Nombre del Afiliado")]),
        _vm._v(" "),
        _c("input", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.nombre_afiliado,
              expression: "nombre_afiliado"
            }
          ],
          staticClass: "form-control",
          attrs: { type: "text", readonly: "" },
          domProps: { value: _vm.nombre_afiliado },
          on: {
            input: function($event) {
              if ($event.target.composing) {
                return
              }
              _vm.nombre_afiliado = $event.target.value
            }
          }
        })
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "col col-md-2" }, [
      _c("div", { staticClass: "form-group" }, [
        _c("label", [_vm._v("Apertura Expediente")]),
        _vm._v(" "),
        _c("div", { staticClass: "input-group" }, [
          _vm._m(0),
          _vm._v(" "),
          _c("input", {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.apertura_expediente,
                expression: "apertura_expediente"
              }
            ],
            staticClass: "form-control",
            attrs: { type: "text", readonly: "" },
            domProps: { value: _vm.apertura_expediente },
            on: {
              input: function($event) {
                if ($event.target.composing) {
                  return
                }
                _vm.apertura_expediente = $event.target.value
              }
            }
          })
        ])
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "col col-md-2" }, [
      _c("div", { staticClass: "form-group" }, [
        _c("label", [_vm._v("Apretura Asistencia")]),
        _vm._v(" "),
        _c("div", { staticClass: "input-group " }, [
          _vm._m(1),
          _vm._v(" "),
          _c("input", {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.apertura_asistencia,
                expression: "apertura_asistencia"
              }
            ],
            staticClass: "form-control",
            attrs: { type: "text", readonly: "" },
            domProps: { value: _vm.apertura_asistencia },
            on: {
              input: function($event) {
                if ($event.target.composing) {
                  return
                }
                _vm.apertura_asistencia = $event.target.value
              }
            }
          })
        ])
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "col col-md-2" }, [
      _c("div", { staticClass: "form-group" }, [
        _c("label", [_vm._v("Prevision Inicial")]),
        _vm._v(" "),
        _c("input", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.prevision.nombre,
              expression: "prevision.nombre"
            }
          ],
          staticClass: "form-control",
          attrs: { type: "text", readonly: "" },
          domProps: { value: _vm.prevision.nombre },
          on: {
            input: function($event) {
              if ($event.target.composing) {
                return
              }
              _vm.$set(_vm.prevision, "nombre", $event.target.value)
            }
          }
        })
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "col col-md-2" }, [
      _c("div", { staticClass: "form-group" }, [
        _c("label", [_vm._v("Costo SIGA")]),
        _vm._v(" "),
        _c("div", { staticClass: "input-group " }, [
          _vm._m(2),
          _vm._v(" "),
          _c("input", {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.costo_asistencia,
                expression: "costo_asistencia"
              }
            ],
            staticClass: "form-control",
            attrs: { type: "text", readonly: "" },
            domProps: { value: _vm.costo_asistencia },
            on: {
              input: function($event) {
                if ($event.target.composing) {
                  return
                }
                _vm.costo_asistencia = $event.target.value
              }
            }
          })
        ])
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "col col-md-4" }, [
      _c("div", { staticClass: "form-group" }, [
        _c("label", [_vm._v("Creador")]),
        _vm._v(" "),
        _c("input", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.usuario_creacion.name,
              expression: "usuario_creacion.name"
            }
          ],
          staticClass: "form-control",
          attrs: { type: "text", readonly: "" },
          domProps: { value: _vm.usuario_creacion.name },
          on: {
            input: function($event) {
              if ($event.target.composing) {
                return
              }
              _vm.$set(_vm.usuario_creacion, "name", $event.target.value)
            }
          }
        })
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-prepend" }, [
      _c("span", { staticClass: "input-group-text readonly" }, [
        _c("i", { staticClass: "ni ni-calendar-grid-58" })
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-prepend" }, [
      _c("span", { staticClass: "input-group-text readonly" }, [
        _c("i", { staticClass: "ni ni-calendar-grid-58" })
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-prepend" }, [
      _c("span", { staticClass: "input-group-text readonly" }, [
        _c("i", { staticClass: "fa fa-dollar-sign" })
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/asistencias/BitacoraAsistencia.vue":
/*!********************************************************************!*\
  !*** ./resources/js/components/asistencias/BitacoraAsistencia.vue ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _BitacoraAsistencia_vue_vue_type_template_id_50f57a6a_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./BitacoraAsistencia.vue?vue&type=template&id=50f57a6a&scoped=true& */ "./resources/js/components/asistencias/BitacoraAsistencia.vue?vue&type=template&id=50f57a6a&scoped=true&");
/* harmony import */ var _BitacoraAsistencia_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./BitacoraAsistencia.vue?vue&type=script&lang=js& */ "./resources/js/components/asistencias/BitacoraAsistencia.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _BitacoraAsistencia_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _BitacoraAsistencia_vue_vue_type_template_id_50f57a6a_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _BitacoraAsistencia_vue_vue_type_template_id_50f57a6a_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "50f57a6a",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/asistencias/BitacoraAsistencia.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/asistencias/BitacoraAsistencia.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************!*\
  !*** ./resources/js/components/asistencias/BitacoraAsistencia.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_BitacoraAsistencia_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./BitacoraAsistencia.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/asistencias/BitacoraAsistencia.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_BitacoraAsistencia_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/asistencias/BitacoraAsistencia.vue?vue&type=template&id=50f57a6a&scoped=true&":
/*!***************************************************************************************************************!*\
  !*** ./resources/js/components/asistencias/BitacoraAsistencia.vue?vue&type=template&id=50f57a6a&scoped=true& ***!
  \***************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BitacoraAsistencia_vue_vue_type_template_id_50f57a6a_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./BitacoraAsistencia.vue?vue&type=template&id=50f57a6a&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/asistencias/BitacoraAsistencia.vue?vue&type=template&id=50f57a6a&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BitacoraAsistencia_vue_vue_type_template_id_50f57a6a_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BitacoraAsistencia_vue_vue_type_template_id_50f57a6a_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/asistencias/CirugiaWrapper.vue":
/*!****************************************************************!*\
  !*** ./resources/js/components/asistencias/CirugiaWrapper.vue ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _CirugiaWrapper_vue_vue_type_template_id_71020f5e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./CirugiaWrapper.vue?vue&type=template&id=71020f5e& */ "./resources/js/components/asistencias/CirugiaWrapper.vue?vue&type=template&id=71020f5e&");
/* harmony import */ var _CirugiaWrapper_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./CirugiaWrapper.vue?vue&type=script&lang=js& */ "./resources/js/components/asistencias/CirugiaWrapper.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _CirugiaWrapper_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./CirugiaWrapper.vue?vue&type=style&index=0&lang=css& */ "./resources/js/components/asistencias/CirugiaWrapper.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _CirugiaWrapper_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _CirugiaWrapper_vue_vue_type_template_id_71020f5e___WEBPACK_IMPORTED_MODULE_0__["render"],
  _CirugiaWrapper_vue_vue_type_template_id_71020f5e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/asistencias/CirugiaWrapper.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/asistencias/CirugiaWrapper.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************!*\
  !*** ./resources/js/components/asistencias/CirugiaWrapper.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CirugiaWrapper_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./CirugiaWrapper.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/asistencias/CirugiaWrapper.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CirugiaWrapper_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/asistencias/CirugiaWrapper.vue?vue&type=style&index=0&lang=css&":
/*!*************************************************************************************************!*\
  !*** ./resources/js/components/asistencias/CirugiaWrapper.vue?vue&type=style&index=0&lang=css& ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_CirugiaWrapper_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./CirugiaWrapper.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/asistencias/CirugiaWrapper.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_CirugiaWrapper_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_CirugiaWrapper_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_CirugiaWrapper_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_CirugiaWrapper_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_CirugiaWrapper_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/asistencias/CirugiaWrapper.vue?vue&type=template&id=71020f5e&":
/*!***********************************************************************************************!*\
  !*** ./resources/js/components/asistencias/CirugiaWrapper.vue?vue&type=template&id=71020f5e& ***!
  \***********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CirugiaWrapper_vue_vue_type_template_id_71020f5e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./CirugiaWrapper.vue?vue&type=template&id=71020f5e& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/asistencias/CirugiaWrapper.vue?vue&type=template&id=71020f5e&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CirugiaWrapper_vue_vue_type_template_id_71020f5e___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CirugiaWrapper_vue_vue_type_template_id_71020f5e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/asistencias/RegistoCirugia.vue":
/*!****************************************************************!*\
  !*** ./resources/js/components/asistencias/RegistoCirugia.vue ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _RegistoCirugia_vue_vue_type_template_id_433db4c4_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./RegistoCirugia.vue?vue&type=template&id=433db4c4&scoped=true& */ "./resources/js/components/asistencias/RegistoCirugia.vue?vue&type=template&id=433db4c4&scoped=true&");
/* harmony import */ var _RegistoCirugia_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./RegistoCirugia.vue?vue&type=script&lang=js& */ "./resources/js/components/asistencias/RegistoCirugia.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _RegistoCirugia_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _RegistoCirugia_vue_vue_type_template_id_433db4c4_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _RegistoCirugia_vue_vue_type_template_id_433db4c4_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "433db4c4",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/asistencias/RegistoCirugia.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/asistencias/RegistoCirugia.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************!*\
  !*** ./resources/js/components/asistencias/RegistoCirugia.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_RegistoCirugia_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./RegistoCirugia.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/asistencias/RegistoCirugia.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_RegistoCirugia_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/asistencias/RegistoCirugia.vue?vue&type=template&id=433db4c4&scoped=true&":
/*!***********************************************************************************************************!*\
  !*** ./resources/js/components/asistencias/RegistoCirugia.vue?vue&type=template&id=433db4c4&scoped=true& ***!
  \***********************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_RegistoCirugia_vue_vue_type_template_id_433db4c4_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./RegistoCirugia.vue?vue&type=template&id=433db4c4&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/asistencias/RegistoCirugia.vue?vue&type=template&id=433db4c4&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_RegistoCirugia_vue_vue_type_template_id_433db4c4_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_RegistoCirugia_vue_vue_type_template_id_433db4c4_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/asistencias/SigaAsistencia.vue":
/*!****************************************************************!*\
  !*** ./resources/js/components/asistencias/SigaAsistencia.vue ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _SigaAsistencia_vue_vue_type_template_id_148f7c4b_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./SigaAsistencia.vue?vue&type=template&id=148f7c4b&scoped=true& */ "./resources/js/components/asistencias/SigaAsistencia.vue?vue&type=template&id=148f7c4b&scoped=true&");
/* harmony import */ var _SigaAsistencia_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./SigaAsistencia.vue?vue&type=script&lang=js& */ "./resources/js/components/asistencias/SigaAsistencia.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _SigaAsistencia_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _SigaAsistencia_vue_vue_type_template_id_148f7c4b_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _SigaAsistencia_vue_vue_type_template_id_148f7c4b_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "148f7c4b",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/asistencias/SigaAsistencia.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/asistencias/SigaAsistencia.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************!*\
  !*** ./resources/js/components/asistencias/SigaAsistencia.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SigaAsistencia_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./SigaAsistencia.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/asistencias/SigaAsistencia.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SigaAsistencia_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/asistencias/SigaAsistencia.vue?vue&type=template&id=148f7c4b&scoped=true&":
/*!***********************************************************************************************************!*\
  !*** ./resources/js/components/asistencias/SigaAsistencia.vue?vue&type=template&id=148f7c4b&scoped=true& ***!
  \***********************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SigaAsistencia_vue_vue_type_template_id_148f7c4b_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./SigaAsistencia.vue?vue&type=template&id=148f7c4b&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/asistencias/SigaAsistencia.vue?vue&type=template&id=148f7c4b&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SigaAsistencia_vue_vue_type_template_id_148f7c4b_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SigaAsistencia_vue_vue_type_template_id_148f7c4b_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);
//# sourceMappingURL=registro-cirugia-component.js.map