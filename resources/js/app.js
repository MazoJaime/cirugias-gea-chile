import './bootstrap'

import router from "./routes";

/** Mixin para control de permisos e informacion basica de usuario en vue solo si usuario esta autenticado
 * Aunque no es posible siquiera que accesa a la instancias de vue dado que solo se carga despues del login
 * por este motivo no se tienen validaciones para definicion de valores por default
 **/
import Permissions from './mixins/Permissions';

Vue.prototype.$authUser = window.authUser;
Vue.mixin(Permissions);


/**Componentes **/
Vue.component('notifications-dropdown', function (resolve) {
    require([/* webpackChunkName: "notifications-component" */'./components/notificaciones/NotificationsDropdown.vue'], resolve)
});
Vue.component('search-input', function (resolve) {
    require([/* webpackChunkName: "search-component" */'./components/SearchBar.vue'], resolve)
});
import vSelect from 'vue-select';

Vue.component('v-select', vSelect);
import Tabs from "./components/Tabs/Tabs";
import TabPane from "./components/Tabs/TabPane";
import Card from "./components/Card";

Vue.component('tabs', Tabs);
Vue.component('tab-pane', TabPane);
Vue.component('card', Card);

/** Componentes base definidos de forma dinamica**/
const files = require.context('./components/base', true, /\.vue$/i);
files.keys().map(key => Vue.component('' + key.split('/').pop().split('.')[0], files(key).default));

import CKEditor from '@ckeditor/ckeditor5-vue';

Vue.use( CKEditor );





new Vue({
    el: '#app',
    router,
});
