window._ = require('lodash');
import Vue from 'vue'

/**
 * Componente usado para indicar tiempo transcurrido desde evento de una notificacion
 **/
import VueTimeago from 'vue-timeago'

Vue.use(VueTimeago, {
    locale: 'es',
    locales: {
        es: require('date-fns/locale/es')
    }
});

window.Vue = Vue;
// Configure Laravel Echo
window.axios = require('axios');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}
/** Sweetalert 2 en un wrapper**/
import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';

Vue.use(VueSweetalert2);

/**
 * Interceptor de errores 403, en caso de que por algun motivo algun componente que requiera permisos
 * Se pasa por alto el los permisos si el backend retorna este error redidira al index de la app
 * **/
axios.interceptors.response.use(
    function (response) {
        return response;
    },
    function (error) {

        if (error.response.status === 403) {
            Vue.swal('Error', error.response.data.message, 'error');
        }
        if (error.response.status === 419) {
            Vue.swal('Error', error.response.data.message, 'error')
                .then((result) => {
                    window.location.href = "/login";
                });

        }
        return Promise.reject(error);

    });
