import Router from "vue-router";


/**
 * Separacion en multiples modulos para reducir tamaño de archivo principal app.js
 */
const AsistenciasList = () => import(/* webpackChunkName: "asistencias-list-component" */ './components/asistencias/AsistenciasList.vue');
const NotFound = () => import(/* webpackChunkName: "not-found-component" */ './components/NotFound.vue');
const RegistoCirugia = () => import(/* webpackChunkName: "registro-cirugia-component" */ './components/asistencias/RegistoCirugia.vue');
const CirugiaInfo = () => import(/* webpackChunkName: "registro-cirugia-component" */  './components/asistencias/CirugiaWrapper.vue');

// Mdodulos de mantenedores
const MantenedoresMain = () => import(/* webpackChunkName: "mantenedores-component" */ './components/mantenedores/MantenedoresMain.vue');

const ListaPrevisiones = () => import(/* webpackChunkName: "mantenedores-component" */ './components/mantenedores/previsiones/ListaPrevisiones.vue');
const CrearPrevisiones = () => import(/* webpackChunkName: "mantenedores-component" */ './components/mantenedores/previsiones/CrearPrevisiones.vue');
const EditarPrevisiones = () => import(/* webpackChunkName: "mantenedores-component" */ './components/mantenedores/previsiones/EditarPrevisiones.vue');

const ListaDiagnosticos = () => import(/* webpackChunkName: "mantenedores-component" */ './components/mantenedores/diagnosticos/ListaDiagnosticos.vue');
const CrearDiagnosticos = () => import(/* webpackChunkName: "mantenedores-component" */ './components/mantenedores/diagnosticos/CrearDiagnostico.vue');
const EditarDiagnosticos = () => import(/* webpackChunkName: "mantenedores-component" */ './components/mantenedores/diagnosticos/EditarDiagnostico.vue');

const ListaProcedimientos = () => import(/* webpackChunkName: "mantenedores-component" */ './components/mantenedores/procedimientos/ListaProcedimientos.vue');
const CrearProcedimientos = () => import(/* webpackChunkName: "mantenedores-component" */ './components/mantenedores/procedimientos/CrearProcedimiento.vue');
const EditarProcedimientos = () => import(/* webpackChunkName: "mantenedores-component" */ './components/mantenedores/procedimientos/EditarProcedimientos.vue');

const ListaRegiones = () => import(/* webpackChunkName: "mantenedores-component" */ './components/mantenedores/regiones/ListaRegiones.vue');
const CrearRegiones = () => import(/* webpackChunkName: "mantenedores-component" */ './components/mantenedores/regiones/CrearRegion.vue');
const EditarRegion = () => import(/* webpackChunkName: "mantenedores-component" */ './components/mantenedores/regiones/EditarRegion.vue');

const ListaBonos = () => import(/* webpackChunkName: "mantenedores-component" */ './components/mantenedores/bonos/ListaBonos.vue');
const CrearBonos = () => import(/* webpackChunkName: "mantenedores-component" */ './components/mantenedores/bonos/CrearBonos.vue');
const EditarBonos = () => import(/* webpackChunkName: "mantenedores-component" */ './components/mantenedores/bonos/EditarBonos.vue');

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: '/asistencias',
            component: MantenedoresMain,
            children: [

                {
                    path: '',
                    name: 'asistencias-list',
                    component: AsistenciasList,
                },
                {
                    path: 'create',
                    name: 'cirugia-crear',
                    component: RegistoCirugia,
                },
                {
                    path: ':asistencia',
                    name: 'asistencia-trabajar',
                    component: CirugiaInfo,
                },
                {
                    path: '*',
                    redirect: {name: 'asistencias-list'}
                }
            ]
        },
        {
            path: '/mantenedores/previsiones',
            component: MantenedoresMain,
            children: [
                {
                    path: '',
                    name: 'previsiones-list',
                    component: ListaPrevisiones
                },
                {
                    path: 'create',
                    name: 'prevision-crear',
                    component: CrearPrevisiones
                },
                {
                    path: ':id/edit',
                    name: 'prevision-editar',
                    component: EditarPrevisiones,
                },
                {
                    path: '*',
                    redirect: {name: 'previsiones-list'}
                }
            ]
        },
        {
            path: '/mantenedores/diagnosticos',
            component: MantenedoresMain,
            children: [
                {
                    path: '',
                    name: 'diagnostico-list',
                    component: ListaDiagnosticos
                },
                {
                    path: 'create',
                    name: 'diagnostico-crear',
                    component: CrearDiagnosticos
                },
                {
                    path: ':id/edit',
                    name: 'diagnostico-editar',
                    component: EditarDiagnosticos,
                },
                {
                    path: '*',
                    redirect: {name: 'diagnostico-list'}
                }
            ]
        },
        {
            path: '/mantenedores/procedimientos',
            component: MantenedoresMain,
            children: [
                {
                    path: '',
                    name: 'procedimiento-list',
                    component: ListaProcedimientos
                },
                {
                    path: 'create',
                    name: 'procedimiento-crear',
                    component: CrearProcedimientos
                },
                {
                    path: ':id/edit',
                    name: 'procedimiento-editar',
                    component: EditarProcedimientos,
                },
                {
                    path: '*',
                    redirect: {name: 'procedimiento-list'}
                }
            ]
        },
        {
            path: '/mantenedores/regiones',
            component: MantenedoresMain,
            children: [
                {
                    path: '',
                    name: 'region-list',
                    component: ListaRegiones
                },
                {
                    path: 'create',
                    name: 'region-crear',
                    component: CrearRegiones
                },
                {
                    path: ':id/edit',
                    name: 'region-editar',
                    component: EditarRegion
                }
            ]
        }, {
            path: '/mantenedores/bonos',
            component: MantenedoresMain,
            children: [
                {
                    path: '',
                    name: 'bonos-list',
                    component: ListaBonos
                },
                {
                    path: 'create',
                    name: 'bonos-crear',
                    component: CrearBonos
                },
                {
                    path: ':id/edit',
                    name: 'bonos-editar',
                    component: EditarBonos
                }
            ]
        },
        {
            path: '/404',
            name: 'not-found',
            component: NotFound
        },
    ],
    mode: 'history'
})


