<?php

return [

    'failed' => 'Usuario o contraseña incorrectos',
    'throttle' => 'Demaciados intentos. Intente nuevamente tras :seconds segundos.',
];
