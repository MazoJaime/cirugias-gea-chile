<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Argon Dashboard') }}</title>
    <!-- Favicon -->
    <link href="{{ asset('argon/img/brand/favicon.png') }}" rel="icon" type="image/png">
    <!-- Fonts -->
{{--<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">--}}
<!-- Icons -->

    <link href="{{ asset('argon/vendor/nucleo/css/nucleo.css') }}" rel="stylesheet">
    <link href="{{ asset('argon/vendor/@fortawesome/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
    <link href="{{asset('css/app.css')}}" rel="stylesheet">
    <!-- Argon CSS -->
    {{--<link type="text/css" href="{{ asset('argon/css/argon.css') }}?v=1.0.0" rel="stylesheet">--}}
    @stack('css')
</head>
<body class="{{ $class ?? '' }}">
@auth
    <script>
        window.authUser = @json(Auth::user());
        window.Permissions = @json(Auth::user()->allPermissions, true);
    </script>
@endauth
<div id="app">

    @auth()
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>

        @include('layouts.navbars.sidebar')

    @endauth

    <div class="main-content">
        @include('layouts.navbars.navbar')
        <div class="header bg-gradient-teal pt-5 pt-md-8">
            <div class="container-fluid">
                <div class="header-body">
                    @yield('header')
                </div>
            </div>
        </div>
        @yield('content')
    </div>
</div>

<script src="{{ asset('argon/vendor/jquery/dist/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('argon/vendor/bootstrap/dist/js/bootstrap.bundle.min.js') }}" type="text/javascript"></script>
<script src="{{asset('js/app.js')}}" type="text/javascript"></script>

<!-- Argon JS -->
<script src="{{ asset('argon/js/argon.js') }}?v={{rand(0,1000)}}" type="text/javascript"></script>
<script>

    $(document).ready(function () {

        $('#hide_side').on('click', function () {
            $('#sidenav-main').visibility = 'hidden'
        });

        $('#hide').click(function () {
            $("#sidenav-main").fadeOut(1600, "linear")
        });

        var state = "expanded";
        //Check if navbar is expanded or minimized and handle
        $('#navbar-toggle').click(function () {
            if (state == "expanded") {
                $('.sidebar').css('width', '0rem');
                $('.sidebar').css('display', 'none');

                $('.main-content').css('margin-left', '0rem');
                $('#brand').css('display', 'none');
                state = "minimized";
            } else {
                if (state == "minimized") {
                    $('.sidebar').css('width', '14rem');
                    $('.sidebar').css('display', 'block');
                    $('.main-content').css('margin-left', '14rem');
                    $('#brand').css('display', 'block');
                    state = "expanded";
                }
            }
        })

    });

</script>

@stack('js')
</body>
</html>
