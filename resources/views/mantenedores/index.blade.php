@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <router-view :key="$route.fullPath"></router-view>
    </div>


@endsection

