@extends('layouts.app', ['title' => __('User Management')])

@section('content')

    <div class="container-fluid mt--5">
        <div class="row">
            <div class=" mx-auto col-xl-6 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Creación de usuarios') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('user.index') }}"
                                   class="btn btn-dark">{{ __('Volver a usuarios') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('user.store') }}" autocomplete="off">
                            @csrf


                            <h6 class="heading-small text-muted mb-4">{{ __('Información del usuario') }}</h6>
                            <div class="pl-lg-4">
                                <div class="form-group{{ $errors->has('rut') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="rut">{{ __('Rut') }}</label>
                                    <input type="text" name="rut" id="rut"
                                           class="form-control form-control-alternative{{ $errors->has('rut') ? ' is-invalid' : '' }}"
                                           placeholder="{{ __('Rut') }}" value="{{ old('rut') }}" required autofocus
                                           onchange="validaactivo(this)">

                                    @if ($errors->has('rut'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('rut') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-name">{{ __('Nombre') }}</label>
                                    <input type="text" name="name" id="input-name"
                                           class="form-control form-control-alternative{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                           placeholder="{{ __('Nombre') }}" value="{{ old('name') }}" required>

                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('user') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-user">{{ __('Usuario') }}</label>
                                    <input type="text" name="user" id="input-user" readonly
                                           class="form-control form-control-alternative{{ $errors->has('user') ? ' is-invalid' : '' }}"
                                           placeholder="{{ __('Usuario') }}" value="{{ old('user') }}" required>

                                    @if ($errors->has('user'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('user') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <div class=" custom-control custom-radio">
                                        <div class="custom-control custom-radio mb-3">
                                            <input name="active" class="custom-control-input" id="active1" type="radio"
                                                   value="0">
                                            <label class="custom-control-label" for="active1">Inactivo</label>
                                        </div>
                                        <div class="custom-control custom-radio mb-3">
                                            <input name="active" class="custom-control-input" id="active2" checked=""
                                                   type="radio" value="1">
                                            <label class="custom-control-label" for="active2">Activo</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="form-control-label" for="roles">{{ __('Roles') }}</label>
                                    {!! Form::select('roles[]', $roles,[], array('class' => 'form-control' ,'multiple')) !!}
                                </div>
                                <div class="text-center">
                                    <button type="submit" class="btn btn-success mt-4">{{ __('Save') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        @include('layouts.footers.auth')
    </div>

@endsection

@push('js')
    <script>
        function validaactivo(input) {
            rut = document.getElementById('rut').value;
            if (rut.length >= 6) {

                const init2 = {
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                        'Content-Type': 'application/json',
                    },
                    method: 'POST',
                    body: '{ "rut": "' + rut + '" }',
                };

                fetch('/api/register/validacion', init2).then((res) => {
                    if (res.ok) {
                        return res.json();
                    }
                }).then((validacion) => {
                    if (validacion.status === 'success') {
                        /*   18624073-1 */
                        document.getElementById('input-name').value = validacion.name;
                        document.getElementById('input-user').value = validacion.user;
                        rutok = rut == validacion.rut ? true : false;

                    } else {
                        alert(validacion.message);
                        input.value = '';
                    }
                }).catch(function () {
                    // ERROR
                });

            }
        }
    </script>

@endpush
