@extends('layouts.app', ['title' => __('User Management')])

@section('content')

    <div class="container-fluid mt--5">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Users') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('user.create') }}"
                                   class="btn btn-success">{{ __('Crear Usuario') }}</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        @if (session('status'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {{ session('status') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                    </div>

                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col">{{ __('Nombre') }}</th>
                                <th scope="col">{{ __('Usuario') }}</th>
                                <th scope="col">Rut</th>
                                <th scope="col">Estado</th>
                                <th scope="col">Creacion</th>
                                <th scope="col">Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($users as $user)
                                <tr>
                                    <td>{{ $user->name }}</td>
                                    <td> {{ $user->user }}</td>
                                    <td> {{ $user->rut }}</td>
                                    <td class="{{$user->active == true ? 'text-success':'text-danger' }}"> {{ $user->active == true ? 'Activo': 'Inactivo' }}</td>
                                    <td>{{ $user->created_at->format('Y-m-d') }}</td>
                                    <td >
                                        <div class="dropdown">
                                            <a class="btn btn-sm btn-icon-only text-light" href="#" role="button"
                                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fas fa-ellipsis-v"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                @if ($user->id != auth()->id())
                                                    <form action="{{ route('user.destroy', $user) }}"
                                                          id="form-delete-user-{{$user->id}}" method="post">
                                                        @csrf
                                                        @method('delete')

                                                        <a class="dropdown-item"
                                                           href="{{ route('user.edit', $user) }}">{{ __('Edit') }}</a>
                                                        <button type="button" class="dropdown-item"
                                                                onclick="deleteuser({{$user->id}})">
                                                            {{ __('Delete') }}
                                                        </button>
                                                    </form>
                                                @else
                                                    <a class="dropdown-item"
                                                       href="{{ route('profile.edit') }}">{{ __('Edit') }}</a>
                                                @endif
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div class="card-footer py-4">
                        <nav class="d-flex justify-content-end" aria-label="...">
                            {{ $users->links() }}
                        </nav>
                    </div>

                </div>
            </div>
        </div>

        @include('layouts.footers.auth')
    </div>
@endsection
@push('js')

    <script>


        function deleteuser(id) {
            Swal.fire({

                text: "Seguro que deseas eliminar este registro?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonText: 'No, No Eliminar',
                confirmButtonText: 'Si! Eliminar',

            }).then((result) => {
                if (result.value) {
                    let form = $('#form-delete-user-' + id)
                    form.trigger('submit');
                }
            })
        }


    </script>
@endpush
