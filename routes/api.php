<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// modificamos el authServiceProvider para que solo se pueda usar la api si y solo si se esta logueado
//de forma web
Route::group(['middleware' => ['auth']], function () {
    Route::get('previsiones', 'Api\PrevisionesController@index');
    Route::get('busqueda/asistencia', 'Api\SearchAsistenciasController@search');
    Route::get('asistencias/list', 'Api\SearchAsistenciasController@list');
    Route::get('asistencia/{asistencia}', 'Api\AsistenciaController@show')->where('asistencia', '[0-9]+');
    Route::put('prevision/asistencia', 'Api\AsistenciaController@updateprevision');
    Route::get('asistencias/data', 'Api\AsistenciaController@data');
    Route::post('asistencias', 'Api\AsistenciaController@store');
    Route::post('costos/asistencia', 'Api\AsistenciaController@costos');
    Route::put('costos/asistencia', 'Api\AsistenciaController@updatecosto');
    Route::post('observacion/asistencia', 'Api\AsistenciaController@observacion');
    Route::get('ObtenerBonos', 'Api\AsistenciaController@bonosData');//obtener listado de bonos por tipo

    Route::get('uf/valor/{day}/{month}/{year}','Api\UfController@show');
    Route::post('register/validacion', 'Api\Auth\RegisterController@index');


    Route::group(['prefix' => 'mantenedores'], function () {
        Route::resource('previsiones', 'Api\Mantenedores\PrevisionController')->only(['index', 'store', 'update', 'show']);
        Route::resource('diagnosticos', 'Api\Mantenedores\DiagnosticoController')->only(['index', 'store', 'update', 'show']);
        Route::resource('procedimientos', 'Api\Mantenedores\ProcedimientosController')->only(['index', 'store', 'update', 'show']);
        Route::resource('regiones', 'Api\Mantenedores\RegionesController')->only(['index', 'store', 'update', 'show']);
        Route::resource('bonos', 'Api\Mantenedores\BonosController')->only(['index', 'store', 'update', 'show']);

    });
});



