<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/', 'HomeController@index')->name('home');

    /**
     * Rutas Para notificaciones
     */
    Route::post('notifications', 'NotificationController@store');
    Route::get('notifications', 'NotificationController@index');
    Route::patch('notifications/{id}/read', 'NotificationController@markAsRead');
    Route::post('notifications/mark-all-read', 'NotificationController@markAllRead');

    /**
     * Rutas de perfil
     */
    Route::get('profile', 'ProfileController@edit')->name('profile.edit');
    Route::put('profile', 'ProfileController@update')->name('profile.update');
    Route::put('profile/password', 'ProfileController@password')->name('profile.password');

    //Todo con prefino asistencias va al controller de asistencias donde todo termina renderizado mediante vue
    Route::get('/asistencias/{any?}', 'Asistencia\AsistenciaController@index')->where('any', '.*');

    /* Route::get('asistencias/{id}','Asistencia\AsistenciaController@index');*/
    /**
     * Mantenedores
     */
    Route::group(['prefix' => 'mantenedores'], function () {
        /**
         * Solo user y roles usan interfaz echa con blade.
         */
        Route::resource('roles', 'RoleController');
        Route::resource('user', 'UserController', ['except' => ['show']]);
        Route::get('/{any?}', 'MantenedoresController@index')->where('any', '.*');
    });

});



/*Route::post('notifications/{id}/dismiss', 'NotificationController@dismiss');*/
