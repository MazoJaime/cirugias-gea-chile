<?php

namespace Tests;

use Faker\Factory;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use App\Models\User;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
    protected $faker;

    public function setUp(): void
    {
        parent::setUp();
        $this->faker = Factory::create();
    }

    /**
     * @return  User
     */
    public function login_as_admin()
    {
        $user = new User([
            'id' => 1, 'name' => 'Jaime Mazo', 'email' => 'mazoj@cl.geainternacional.com',
        ]);

        $this->be($user);
        return $user;
    }
}
