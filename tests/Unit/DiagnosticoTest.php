<?php

namespace Tests\Unit;

use App\Models\Diagnostico;
use Closure;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;

class DiagnosticoTest extends TestCase
{

    /**
     * @test
     */
    public function por_defecto_trae_informacion_de_primera_pagina()
    {
        $this->login_as_admin();

        $reponse = $this->get(route('diagnosticos.index'));
        $reponse->assertStatus(200)
            ->assertJsonFragment(['current_page' => 1]);

    }

    /**
     * @test
     */
    public function trae_cantidad_por_defecto_de_paginacion()
    {
        $this->login_as_admin();

        $reponse = $this->get(route('diagnosticos.index'));
        $reponse->assertStatus(200)
            ->assertJsonFragment(['per_page' => 15]);

    }

    /**
     * si param es custom debe ser tratado como string because fuck php logic!
     * @test
     */
    public function cantidad_en_paginacion_se_modifica()
    {
        $this->login_as_admin();
        $reponse = $this->get(route('diagnosticos.index') . '?length=50&showdata=true');
        $reponse->assertStatus(200)
            ->assertJsonFragment(['per_page' => "50"]);

    }

    /**
     * @test
     */
    public function crear_diagnostico_exito()
    {
        $this->login_as_admin();
        $data = ['nombre' => $this->faker->name];
        $response = $this->post(route('diagnosticos.store'), $data);
        $response->assertStatus(201)
            ->assertJsonFragment($data);
        $this->assertDatabaseHas('diagnosticos', $data);
    }

    /**
     * @test
     */
    public function crear_diagnostico_falla_cuando_nombre_existe()
    {
        $this->login_as_admin();
        $diagnostico = Diagnostico::first();
        $data = ['nombre' => $diagnostico->nombre];
        $response = $this->post(route('diagnosticos.store'), $data);
        $response->assertSessionHasErrors()
            ->assertStatus(302);
    }

    /**
     * @test
     */
    public function recuperar_informacion_de_un_solo_diagnostico()
    {
        $this->login_as_admin();
        $diagnostico = Diagnostico::first();
        $response = $this->get(route('diagnosticos.show', ['diagnostico' => $diagnostico->id]));
        $response->assertSuccessful()->assertJsonFragment($diagnostico->toArray());
    }

    /**
     * @test
     */
    public function recuperar_informacion_de_un_solo_diagnostico_falla_si_encuentra_info()
    {
        $this->login_as_admin();
        $response = $this->get(route('diagnosticos.show', ['diagnostico' => '9999999999as999999']));
        $response->assertStatus(404);
    }

    /**
     * @test
     */
    public function actualizar_informacion_de_diagnostico_exito()
    {
        $this->login_as_admin();
        $diagnostico = Diagnostico::first();
        $data = ['nombre' => $this->faker->name . ' ' . $this->faker->randomNumber()];
        $response = $this->put(route('diagnosticos.update', ['diagnostico' => $diagnostico->id]), $data);
        $response->assertSuccessful() //200
        ->assertJsonFragment($data);
    }
    /**
     * @test
     */
    public function actualizar_informacion_de_diagnostico_falla_si_nombre_existe()
    {
        $this->login_as_admin();
        $diagnostico = Diagnostico::first();
        $data = ['nombre' => $diagnostico->nombre];
        $response = $this->put(route('diagnosticos.update', ['diagnostico' => $diagnostico->id]), $data);
        $response->assertSessionHasErrors();
    }
}
