<?php

namespace Tests\Unit;

use App\Models\Region;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;

class RegionTest extends TestCase
{
    use WithFaker;

    /**
     * @test
     */
    public function por_defecto_trae_informacion_de_primera_pagina()
    {
        $this->login_as_admin();

        $reponse = $this->get(route('regiones.index'));
        $reponse->assertStatus(200)
            ->assertJsonFragment(['current_page' => 1])
            ->assertJsonFragment(['per_page' => 15]);

    }


    /**
     * si param es custom debe ser tratado como string because fuck php logic!
     * @test
     */
    public function cantidad_en_paginacion_se_modifica()
    {
        $this->login_as_admin();
        $reponse = $this->get(route('regiones.index') . '?length=50&showdata=true');
        $reponse->assertStatus(200)
            ->assertJsonFragment(['per_page' => "50"]);

    }

    /**
     * @test
     */
    public function crear_diagnostico_exito()
    {
        $this->login_as_admin();
        $data = ['nombre' => $this->faker->name, 'staff' => true];
        $response = $this->post(route('regiones.store'), $data);
        print_r($data);
        $response->assertStatus(201)
            ->assertJsonFragment($data);
        $this->assertDatabaseHas('regiones', $data);
    }

    /**
     * @test
     */
    public function crear_diagnostico_falla_cuando_nombre_existe()
    {
        $this->login_as_admin();
        $region = Region::all()->random(1)->first();;
        $data = ['nombre' => $region->nombre, 'staff' => $this->faker->boolean];
        $response = $this->post(route('regiones.store'), $data);
        $response->assertSessionHasErrors()
            ->assertStatus(302);
    }

    /**
     * @test
     */
    public function recuperar_informacion_de_un_solo_diagnostico()
    {
        $this->login_as_admin();
        $diagnostico = Region::all()->random(1)->first();
        $response = $this->get(route('regiones.show', ['diagnostico' => $diagnostico->id]));
        $response->assertSuccessful()->assertJsonFragment($diagnostico->toArray());
    }

    /**
     * @test
     */
    public function recuperar_informacion_de_un_solo_diagnostico_falla_si_encuentra_info()
    {
        $this->login_as_admin();
        $response = $this->get(route('regiones.show', ['regione' => '9999999999as999999']));
        $response->assertStatus(404);
    }

    /**
     * @test
     */
    public function actualizar_informacion_de_diagnostico_exito()
    {
        $this->login_as_admin();
        $region = Region::all()->random(1)->first();

        $data = ['nombre' => $this->faker->name . ' asdsd', 'staff'=> true];
        print_r($data);
        $response = $this->put(route('regiones.update', ['diagnostico' => $region->id]), $data);
        $response->assertSuccessful() //200
        ->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function actualizar_informacion_de_diagnostico_falla_si_nombre_existe()
    {
        $this->login_as_admin();
        $region = Region::all()->random(1)->first();
        $region2 = Region::all()->random(1)->first();
        $data = ['nombre' => $region->nombre, 'staff'=> false];
        $response = $this->put(route('regiones.update', ['diagnostico' => $region2->id]), $data);
        $response->assertSessionHasErrors();
    }
}
