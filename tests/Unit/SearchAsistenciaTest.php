<?php

namespace Tests\Feature;

use Illuminate\Support\Facades\DB;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SearchAsistenciaTest extends TestCase
{
    /**
     * @test
     */
    public function busqueda_de_asistencia_por_numero_asis_expediente()
    {
        $data = DB::table('asistencias')
            ->where('asistencia', 'like', "123%")
            ->orWhere('expediente', 'like', "123%")
            ->limit(5)
            ->get(['asistencia', 'expediente', 'nombre_afiliado']);

        $this->assertCount(0,$data);
    }
}
